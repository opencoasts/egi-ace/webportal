from datetime import datetime, timedelta
from typing import Mapping
from django.utils.translation import gettext as _, override
from functools import lru_cache
from itertools import chain
from pathlib import Path
from pprint import pformat
from urllib.request import urlopen

from django.conf import settings

from wiff.engine.parts import Stage, Layer
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler
from wiff.models.schism.stages.base import SCHISMStage

from front.utils import notify_users as notify_users_by_email
from wizard.utils import get_auxfile_bk as get_deployment_auxfile_text
from .ncwms import ncWMS

from datamodel.models import Deployment, DeploymentRun, ForcingSource, \
                                                     DeploymentRunOutputFilepath
from .models import Configuration


class OPENCoastSMappings:

    from wiff.models.schism.common import SimulationKind, ConfigurationKind, \
                     BoundaryKind, ForcingKind, ForcingSource, ForcingSeriesKind

    simulation_kind_map = {
        Configuration.Target.BAROTROPIC: SimulationKind.BAROTROPIC,
        Configuration.Target.BAROTROPIC_WAVES: SimulationKind.BAROTROPIC_WAVES,
        Configuration.Target.BAROTROPIC_MICROBIOLOGY:
                                         SimulationKind.BAROTROPIC_MICROBIOLOGY,
        Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY:
                                   SimulationKind.BAROTROPIC_WAVES_MICROBIOLOGY,

        Configuration.Target.BAROCLINIC: SimulationKind.BAROCLINIC,
        Configuration.Target.BAROCLINIC_WAVES: SimulationKind.BAROCLINIC_WAVES,
        Configuration.Target.BAROCLINIC_MICROBIOLOGY:
                                         SimulationKind.BAROCLINIC_MICROBIOLOGY,
        Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY:
                                   SimulationKind.BAROCLINIC_WAVES_MICROBIOLOGY,
    }

    file_type_map = {
        'albedo': 'albedo.gr3',
        'diffmin': 'diffmin.gr3',
        'diffmax': 'diffmax.gr3',
        'drag': 'drag.gr3',
        'kkfib_1': 'kkfib_1.gr3',
        'kkfib_2': 'kkfib_2.gr3',
        'fib': 'sinkfib.gr3',
        'hgrid_gr3': 'hgrid.gr3',
        'hgrid_ll': 'hgrid.ll',
        'fib_hvar1': 'FIB_hvar_1.ic',
        'fib_hvar2': 'FIB_hvar_2.ic',
        'manning': 'manning.gr3',
        'salinity': 'salt.ic',
        'sedimentationfib': 'fraction_fib.gr3',
        'temperature': 'temp.ic',
        'vgrid_in': 'vgrid.in',
        'watertype': 'watertype.gr3',
        'windrot': 'windrot_geo2proj.gr3',
    }
    barotropic_common_auxfiles = (
        'manning',
        'windrot',
    )
    baroclinic_common_auxfiles = (
        'albedo',
        'diffmin',
        'diffmax',
        'drag',
        'salinity',
        'temperature',
        'watertype',
        'windrot',
    )
    microbiology_common_auxfiles = (
        'kkfib_1',
        'kkfib_2',
        'fib',
        'sedimentationfib',
        'fib_hvar1',
        'fib_hvar2',
    )
    barotropic_microbiology_common_auxfiles = (
        *barotropic_common_auxfiles,
        *microbiology_common_auxfiles,
    )
    baroclinic_microbiology_common_auxfiles = (
        *baroclinic_common_auxfiles,
        *microbiology_common_auxfiles,
    )

    simulation_kind_auxfiles_map = {
        SimulationKind.BAROTROPIC: barotropic_common_auxfiles,
        SimulationKind.BAROTROPIC_WAVES: barotropic_common_auxfiles,
        SimulationKind.BAROTROPIC_MICROBIOLOGY:
                                        barotropic_microbiology_common_auxfiles,
        SimulationKind.BAROTROPIC_WAVES_MICROBIOLOGY:
                                        barotropic_microbiology_common_auxfiles,

        SimulationKind.BAROCLINIC: baroclinic_common_auxfiles,
        SimulationKind.BAROCLINIC_WAVES: baroclinic_common_auxfiles,
        SimulationKind.BAROCLINIC_MICROBIOLOGY:
                                        baroclinic_microbiology_common_auxfiles,
        SimulationKind.BAROCLINIC_WAVES_MICROBIOLOGY:
                                        baroclinic_microbiology_common_auxfiles,
    }

    _configuration_barotropic_paramin_common = dict(
        dt = 150.,
        hvis_coef0 = .025,
    )
    _configuration_baroclinic_paramin_common = dict(
        dt = 30.,
        indvel = 0,
        ihorcon = 0,
        hvis_coef0 = .02,
        ishapiro = 1,
        shapiro = .5,
        h0 = .01,
        thetai = .6,
        nstep_wwm = 10,
    )
    _configuration_wwminputnml_common = dict(
        PROC_DELTC = 600.,
        ENGS_BRHD = .78,
    )

    configuration_defaults_map = {
        ConfigurationKind.PARAM_IN: {
            SimulationKind.BAROTROPIC:
                                 dict(_configuration_barotropic_paramin_common),
            SimulationKind.BAROTROPIC_WAVES: 
                                 dict(_configuration_barotropic_paramin_common),
            SimulationKind.BAROTROPIC_MICROBIOLOGY:
                                 dict(_configuration_barotropic_paramin_common),
            SimulationKind.BAROTROPIC_WAVES_MICROBIOLOGY:
                                 dict(_configuration_barotropic_paramin_common),

            SimulationKind.BAROCLINIC:
                                 dict(_configuration_baroclinic_paramin_common),
            SimulationKind.BAROCLINIC_WAVES: 
                                 dict(_configuration_baroclinic_paramin_common),
            SimulationKind.BAROCLINIC_MICROBIOLOGY:
                                 dict(_configuration_baroclinic_paramin_common),
            SimulationKind.BAROCLINIC_WAVES_MICROBIOLOGY:
                                 dict(_configuration_baroclinic_paramin_common),
        },

        ConfigurationKind.WWMINPUT_NML: {
            SimulationKind.BAROTROPIC_WAVES:
                                        dict(_configuration_wwminputnml_common),
            SimulationKind.BAROTROPIC_WAVES_MICROBIOLOGY: 
                                        dict(_configuration_wwminputnml_common),

            SimulationKind.BAROCLINIC_WAVES:
                                        dict(_configuration_wwminputnml_common),
            SimulationKind.BAROCLINIC_WAVES_MICROBIOLOGY: 
                                        dict(_configuration_wwminputnml_common),
        },
    }

    boundary_kind_map = dict(
        ocean = BoundaryKind.OCEAN,
        river = BoundaryKind.RIVER,
    )

    forcing_kind_map = dict(
        elevation = ForcingKind.ELEVATION,
        circulation = ForcingKind.ELEVATION,
        temperature = ForcingKind.TEMPERATURE,
        salinity = ForcingKind.SALINITY,
        waves = ForcingKind.WAVES,
        flow = ForcingKind.FLOW,
        ecoli = ForcingKind.E_COLI,
        enterococcus = ForcingKind.ENTEROCOCCUS,
    )

    ocean_elev_forcings_map = {
        'prism2017': ForcingSource.PRISM_2017,
        'fes2014': ForcingSource.FES_2014,
        'cmems-global': ForcingSource.CMEMS_GLOBAL,
        'cmems-ibi': ForcingSource.CMEMS_IBI,
    }
    ocean_salt_temp_forcings_map = {
        'cmems-global': ForcingSource.CMEMS_GLOBAL,
        'cmems-ibi': ForcingSource.CMEMS_IBI,
    }
    atmospheric_forcings_map = {
        'noforcing': ForcingSource.NO_FORCING,

        'maretec-sinergea_regional': ForcingSource.MARETEC_SINERGIA_REGIONAL,
        'maretec-sinergea_albufeira': ForcingSource.MARETEC_SINERGIA_ALBUFEIRA,

        'meteofr-arpege_ea': ForcingSource.METEOFRANCE_ARGPEGE_EUROPE,

        'meteogalicia-wrf_west_europe':
                                     ForcingSource.METEOGALICIA_WRF_WEST_EUROPE,
        'meteogalicia-wrf_iberia_biscay':
                                   ForcingSource.METEOGALICIA_WRF_IBERIA_BISCAY,
        'meteogalicia-wrf_galicia': ForcingSource.METEOGALICIA_WRF_GALICIA,

        'noaa-gfs': ForcingSource.NOAA_GFS_0P25,
        'noaa-nam': ForcingSource.NOAA_NAM,
        'noaa-nam_conus': ForcingSource.NOAA_NAM_CONUS,
    }


class DjangoDeployment:

    deployment_id = Stage.Input()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.deployment = Deployment.objects.get(id=self.deployment_id)
        self.deployment_front = self.deployment.deployment
        self.deployment_wiff = self.deployment.wiff
        self.wiff_config = self.deployment_wiff.config

    def _publish_deployment_run_files(self, date_time, files, kind):
        try:
            deployment_run = self.deployment.deploymentrun_set.get(
                run_datetime = date_time,
            )
        except DeploymentRun.DoesNotExist:
            self.logger.info("DeploymentRun record not found: "
                                                  "file(s) won't be published!")
            return False

        for file in files:
            entry = DeploymentRunOutputFilepath.objects.create(
                deployment_run = deployment_run,
                kind = kind,
                filepath = file.relative_to(settings.DEPLOYMENTS_PATH),
            )
            self.logger.info('%s file published!', entry)

        return True

    files_to_publish = ()

    def _publish_files(self):
        for pattern, kind in self.files_to_publish:
            files = self.path.glob(pattern)

            if files:
                self._publish_deployment_run_files(self.date_time, files, kind)
            else:
                self.logger.info('No file(s) to publish for %s!', pattern)
            

    _email_templates_dirpath = Path('wiff/emails')

    def _notify_by_email(self, addresses, subject, run_date, template_filename):
        # addresses += ('jrogeiro@lnec.pt',) # testing purposes
        context = dict(
            deployment = self.deployment,
            run_date = run_date,
        )
        templates_filepaths_ = lambda filename: (
            self._email_templates_dirpath / f'{filename}.txt',
            self._email_templates_dirpath / f'{filename}.html',
        )

        notify_users_by_email( addresses, subject, context,
                                      *templates_filepaths_(template_filename) )

        self.logger.info('Email sent: addresses=%s subject=%s template=%s',
                                    addresses, repr(subject), template_filename)

    def _notify_author_by_email(self, subject, run_date, template_filename,
                                                          extra_addresses=None):
        extra_addresses = extra_addresses or tuple()
        addresses = (self.deployment_front.user.email, *extra_addresses)

        with override('en'):
            self._notify_by_email(addresses, _(subject), run_date,
                                                              template_filename)


class Announce0(Layer, ProcessHandler, DjangoDeployment):

    path = Layer.Input()
    date_time = Layer.Input()
    publish_info = Layer.Input(default=True)
    notifications = Layer.Input(default=True)
    begin_timestamp = Layer.Input(datetime.now)
    admin_emails = Layer.Input(default=tuple)

    _stages_failed = Layer.Input(optional=True)

    tar_bin = Layer.Input('/usr/bin/tar')
    # extra_publish_map = SCHISMStage.Input(dict)

    def execute(self):
        self._create_or_update_run_record()

    def _create_or_update_run_record(self):
        record, created = DeploymentRun.objects.update_or_create(
            deployment = self.deployment,
            run_datetime = self.date_time,

            defaults = dict(
                state = DeploymentRun.State.STARTED,
                begin = self.begin_timestamp,
                end = None,
            )
        )

        action = 'Created' if created else 'Updated'
        self.logger.info('%s DeploymentRun record #%i.', action, record.id)

    def finalize(self):
        self._finalize_run_record()

        if self.publish_info:
            self._archive_files()
            self._publish_files()

        if self._stages_failed and self.notifications:
            self._notify_admin_by_email('Falha na execução do sistema',
                                    self.date_time.date(), 'deployment_failure')

    _tar_args = '--create --auto-compress --dereference --verbose'.split()
    _archiving_map = {
        'logs.tar.xz': ('fort.11', 'mirror.out', 'outputs/mirror.out',
                                                         'outputs/fatal.error'),
        'confs.tar.xz': ('param.*', 'wwminput.nml', 'bctides.in'),
        'forcings.tar.xz': ('sflux/', 'ww3_spec.nc' '*D.th')
    }

    def _archive_files(self):
        for archive_file, archive_patterns in self._archiving_map.items():

            paths_to_archive = tuple( chain.from_iterable(
                                       map(self.path.glob, archive_patterns) ) )

            if not paths_to_archive:
                self.logger.debug('nothing to archive for %s', archive_patterns)
                continue

            relative_paths_to_archive = tuple( file_path.relative_to(self.path)
                                             for file_path in paths_to_archive )

            self.logger.info('archiving %s into %s', archive_patterns,
                                                                   archive_file)
            self.run_executable(self.tar_bin, *self._tar_args,
                           f'--file={archive_file}', *relative_paths_to_archive)

    files_to_publish = (
        ('logs.tar.xz', DeploymentRunOutputFilepath.Kind.LOGS),
        ('confs.tar.xz', DeploymentRunOutputFilepath.Kind.CONFS),
        ('forcings.tar.xz', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    def _finalize_run_record(self):
        try:
            deployment_run = DeploymentRun.objects.get(
                deployment = self.deployment,
                run_datetime = self.date_time,
            )
        except DeploymentRun.DoesNotExist:
            self.logger.info('DeploymentRun record not found!')
            return

        run_state = DeploymentRun.State.FAILED if self._stages_failed \
                                               else DeploymentRun.State.FINISHED

        deployment_run.state = run_state
        deployment_run.end = datetime.now()
        deployment_run.save()

        self.logger.info('Updated DeploymentRun record #%i.', deployment_run.id)

    def _notify_admin_by_email(self, subject, run_date, template_filename,
                                                          extra_addresses=None):
        extra_addresses_ = extra_addresses or tuple()
        addresses = tuple(self.admin_emails + extra_addresses_)

        self._notify_by_email(addresses, subject, run_date, template_filename)


from shutil import rmtree


class Prepare0(SCHISMStage, OPENCoastSMappings, DjangoDeployment):

    series_root = SCHISMStage.Input(optional=True)
    begin_timestamp = SCHISMStage.Input(datetime.now)

    simulation_kind = SCHISMStage.Output()
    simulation_period = SCHISMStage.Output()
    param_in = SCHISMStage.Output()
    vertical_levels_count = SCHISMStage.Output()
    boundaries = SCHISMStage.Output()
    forcings = SCHISMStage.Output()
    bbox = SCHISMStage.Output()
    is_cartesian = SCHISMStage.Output()
    fib_flag = SCHISMStage.Output()
    fib_sources = SCHISMStage.Output()

    _lru_cache_size = 8

    def execute(self):
        self._resolve_simulation_kind()
        self._resolve_configuration_files()
        self._resolve_vertical_levels()
        self._resolve_boundaries()
        self._resolve_forcings()
        self._resolve_wave_boundaries()
        self._resolve_fib_sources()
        self._resolve_static_files()

    def _resolve_simulation_kind(self):
        run_type = self.wiff_config['run_type']
        self.simulation_kind = self.simulation_kind_map[run_type]
        self.logger.info('simulation kind: %s', self.simulation_kind.upper() )

        self.simulation_period = \
                               timedelta(hours=self.wiff_config['period_hours'])

    def _resolve_configuration_files(self):

        def coarse_values_type(defined, defaults):
            for key, value in defined.items():
                default_value = defaults.get(key)
                default_type = type(default_value)

                yield key, \
                         value if default_value is None else default_type(value)

        def get_configuration_for_(conf_kind):
            kinds_map = self.configuration_defaults_map.get(conf_kind) or dict()
            kind_defaults = kinds_map.get(self.simulation_kind) or dict()

            deploy_config = self.deployment_wiff.config.get(conf_kind) or dict()
            coarsed_deploy_config = coarse_values_type(deploy_config,
                                                                  kind_defaults)

            return dict(kind_defaults, **dict(coarsed_deploy_config) )

        self.param_in = get_configuration_for_('param_in')
        self.wwminput_nml = get_configuration_for_('wwminput_nml')

        self.timestep = timedelta( seconds=int(self.param_in['dt']) )
        self.fib_flag = self.deployment_wiff.config.get('fib_flag')

    def _resolve_vertical_levels(self):
        vertical_levels = self.deployment_wiff.config.get('vertical_levels')
        if vertical_levels:
            s_levels = vertical_levels['s_levels']
            z_levels = vertical_levels['z_levels']

            self.vertical_levels_count = len(s_levels) + len(z_levels) - 1

    def _resolve_boundaries(self):
        config = self.deployment_wiff.config

        boundaries_forcings = {
            name: self._handle_config_forcings(details['forcings'])
                               for name, details in config['boundaries'].items()
        }
        self.logger.debug("config boundary forcings map:\n%s",
                                                  pformat(boundaries_forcings) )

        forcing_kinds_of_ = lambda boundary: self.SimulationKind.FORCINGS_MAP \
                                      [self.simulation_kind][ boundary['kind'] ]

        def handle_forcings(boundary_name, details):
            for forcing_kind in forcing_kinds_of_(details):
                forcing_details_of_ = lambda boundary: \
                                     boundaries_forcings[boundary][forcing_kind]

                forcing_details = forcing_details_of_(boundary_name)

                forcing_ratio = forcing_details.get('ratio') or 1.
                forcing_reference = forcing_details.get('reference')

                if forcing_reference:
                    self.logger.debug(
                        "%s, %s forcing references %s: %s",
                            boundary_name, forcing_kind, forcing_reference,
                            forcing_details_of_(forcing_reference)
                    )
                    forcing_details = forcing_details_of_(forcing_reference)

                self.logger.debug("forcing details of %s, %s: \n%s",
                                   boundary_name, forcing_kind, forcing_details)

                details[forcing_kind] = self._handle_forcing_series_kind(
                                 forcing_details, self.date_time, forcing_ratio)

        def resolve_boundaries():
            for boundary_name in config['boundaries_order']:
                config_boundary = config['boundaries'][boundary_name]

                boundary_details = dict(
                    kind = self.boundary_kind_map[ config_boundary['kind'] ],
                    node_count = config_boundary['node_count'],
                )

                handle_forcings(boundary_name, boundary_details)

                yield boundary_details

        self.boundaries = tuple( resolve_boundaries() )
        self.logger.info("the following boundaries are defined:\n%s",
                                                      pformat(self.boundaries) )

    _DUMMY_series_kind = '__DUMMY__'

    def _handle_config_forcings(self, forcings):
        self._handle_generic_microbiology(forcings)

        forcing_series = lambda kind, **kwargs: dict(kind=kind, **kwargs)

        opposite_series = lambda series: tuple( -value for value in series )

        series_ = lambda kind, value: \
            opposite_series(value) if kind == self.ForcingKind.FLOW else value

        monthly_value_series_handler = lambda value, fkind: \
            forcing_series(self.ForcingSeriesKind.ANNUAL,
                series = series_(fkind, value),
            )

        annual_value_series_handler = lambda value, fkind: \
                          monthly_value_series_handler([float(value)]*12, fkind)

        percent_series_handler = lambda value, _: \
            forcing_series(self.ForcingSeriesKind.RATIO,
                ratio = value['percent'] / 100,
                reference = value['bid'],
            )

        url_series_handler = lambda value, _: \
            forcing_series(self.ForcingSeriesKind.TH_SERVICE_URL,
                url = value.strip(),
            )

        dummy_handler = lambda value, _: \
            forcing_series(self._DUMMY_series_kind,
                dummy = value,
            )

        series_type_handlers_map = dict(
            monthly = monthly_value_series_handler,
            annual = annual_value_series_handler,
            percent = percent_series_handler,
            url = url_series_handler,
            string = dummy_handler,
        )
        series_type_handler = lambda details, kind: \
            series_type_handlers_map[ details['type'] ](details['value'], kind)

        _forcing = self.forcing_kind_map
        return { _forcing[kind]: series_type_handler(details, _forcing[kind])
                                         for kind, details in forcings.items() }

        # for kind, details in forcings.items():

        #     forcing = self.forcing_kind_map[kind]
        #     handler = _series_type_handlers_map[ details['type'] ]

        #     yield forcing, handler(details['value'], forcing)

    def _handle_generic_microbiology(self, forcings):
        generic_forcing = forcings.pop('generic', None)

        if generic_forcing:
            forcings.update(
                ecoli = generic_forcing,
                # TODO: redo! very hacky!!!
                enterococcus = dict(
                    type = 'annual',
                    value = 0,
                ) if isinstance(generic_forcing, Mapping) else 0,
            )

        return forcings

    def _handle_forcing_series_kind(self, details, date_time, ratio):
        series_kind = details['kind']

        if series_kind == self._DUMMY_series_kind:
            return details['dummy']

        series_kind_handlers_map = {
            self.ForcingSeriesKind.ANNUAL: 
                lambda: self._monthly_value(yearly_series=details['series'],
                                                                date=date_time),
            self.ForcingSeriesKind.TH_SERVICE_URL:
                lambda: self._th_service_value(url_service=details['url'],
                                                                date=date_time),
        }
        forcing_value = series_kind_handlers_map[series_kind]

        return forcing_value() * ratio

    # develop further to use at least the simulation period dates, instead
    # of using only the beginning period date
    @staticmethod
    def _monthly_value(yearly_series, date):
        flux_month_index = date.month - 1
        return yearly_series[flux_month_index]

    @lru_cache(maxsize=_lru_cache_size)
    def _th_service_value(self, url_service, date):
        dated_url_service = date.strftime(url_service)

        self.logger.debug('trying to fetch forcing from %r', dated_url_service)
        bytes_data = urlopen(dated_url_service).read()
        series_lines = bytes_data.decode().strip().splitlines()

        step_pair = lambda ts, v: ( int(ts), -float(v) )
        timestamps, values = zip( *( step_pair( *line.strip().split() )
                                                    for line in series_lines ) )

        return sum(values) / len(values)

    def _resolve_forcings(self):
        map_forcing_ = lambda mapping, forcing: \
                                 mapping[ self.deployment_wiff.config[forcing] ]

        forcings = dict(
            ocean_elev = map_forcing_(self.ocean_elev_forcings_map,
                                                          'ocean_elev_forcing'),
            atmospheric = map_forcing_(self.atmospheric_forcings_map,
                                                         'atmospheric_forcing'),
        )

        if self.simulation_kind in self.SimulationKind.BAROCLINIC_KINDS:
            forcings['ocean_salt_temp'] = map_forcing_(
                   self.ocean_salt_temp_forcings_map, 'ocean_salt_temp_forcing')

        self.forcings = forcings

        deployment_bbox = self.deployment_wiff.config.get('bbox')
        if deployment_bbox:
            (x_min, y_min), (x_max, y_max) = deployment_bbox
            self.bbox = dict(
                lon = (x_min, x_max),
                lat = (y_min, y_max),
            )

    def _resolve_wave_boundaries(self):
        if self.simulation_kind not in self.SimulationKind.WAVES_KINDS:
            return

        boundaries = self.deployment_wiff.config['boundaries']
        has_boundary_forcing = lambda details: ForcingSource.objects.filter(
                   name=details['forcings']['waves']['value'].strip() ).exists()
        is_wave_boundary = lambda details: 'waves' in details['forcings'] and \
                                                   has_boundary_forcing(details)

        wave_boundary_names = tuple( name
            for name, details in boundaries.items() if is_wave_boundary(details)
        )
        self.logger.debug("wave boundaries: %s", wave_boundary_names)

        index_from_ = lambda name: int( name.split('-')[1] )
        wave_boundary_indexes = tuple( map(index_from_, wave_boundary_names) )

        # boundaries_features = \
        #            self.deployment_wiff.config['boundaries_geojson']['features']
        # points_from_ = lambda feature: feature['geometry']['coordinates']
        # is_wave_boundary_ = lambda feature: \
        #                     feature['properties']['code'] in wave_boundary_names

        # wave_boundaries_points = tuple( points_from_(feature)
        #       for feature in boundaries_features if is_wave_boundary_(feature) )

        # self.logger.debug("wave boundaries points: %s", wave_boundaries_points)

        wave_forcings = dict(
            wave_boundaries = wave_boundary_indexes,
            # wave_boundary_points =
            #                tuple( chain.from_iterable(wave_boundaries_points) ),
        )
        self.forcings.update(wave_forcings)

    def _resolve_fib_sources(self):
        if self.simulation_kind not in self.SimulationKind.MICROBIOLOGY_KINDS:
            return

        self.fib_sources = tuple(
            dict(
                coordinates = (source['lon'], source['lat']),
                forcings = self._handle_generic_microbiology(
                    { kind: details['value']
                                 for kind, details in source['fmodel'].items() }
                )
            ) for source in self.deployment_wiff.config.get('fib_sources') or ()
                                                           if source['chk'] == 1
        )
        self.logger.debug(self.fib_sources)

    def _resolve_static_files(self):
        if self.series_root:
            series_static_path = self.series_root / 'static'

            if not series_static_path.exists():
                series_static_path.mkdir(parents=True)
                try:
                    self._download_static_files(series_static_path)
                except: # clean remaning static folder
                    rmtree(series_static_path, ignore_errors=True)
                    raise

            for static_file_path in series_static_path.iterdir():
                run_static_file_path = self.path / static_file_path.name
                self.link_file(static_file_path, run_static_file_path)

        else:
            self._download_static_files(self.path)

        hgrid_gr3_path = self.path / 'hgrid.gr3'
        hgrid_ll_path = hgrid_gr3_path.with_suffix('.ll')
        hgrid_cartesian = False
        try:
            self.link_file(hgrid_ll_path, hgrid_gr3_path)
        except FileExistsError:
            hgrid_cartesian = True
            self.logger.info('using CARTESIAN coordinate system!')

        self.is_cartesian = hgrid_cartesian

    def _download_static_files(self, path):
        self._download_deployment_files(path)
        self._download_deployment_auxfiles(path)

    def _download_deployment_files(self, path):

        file_type_entries = lambda name: self.deployment.files.filter(
                                                           file_type__name=name)

        for entry in file_type_entries('hgrid') | file_type_entries('vgrid'):

            entry_type_name = f'{entry.file_type.name}_{entry.file_type.format}'
            entry_path = path / self.file_type_map[entry_type_name]

            self.logger.debug("downloading %r file as %s", entry_type_name,
                                                                     entry_path)
            entry_path.write_bytes(entry.file)
            self.logger.info("downloaded %r file as %s", entry_type_name,
                                                                     entry_path)

    _fib_flag_1_auxfiles = (
        'kkfib_1',
        'kkfib_2',
    )

    def _download_deployment_auxfiles(self, path):

        for auxfile in self.simulation_kind_auxfiles_map[self.simulation_kind]:

            if not self.fib_flag == 1 and auxfile in self._fib_flag_1_auxfiles:
                continue

            auxfile_path = path / self.file_type_map[auxfile]
            self.logger.debug("downloading %r file as %s", auxfile,
                                                                   auxfile_path)

            auxfile_path.write_text(
                      get_deployment_auxfile_text(self.deployment.id, auxfile) )
            self.logger.info("downloaded %r file as %s", auxfile, auxfile_path)


import f90nml

from wiff.models.schism.data.param import ParamNml


class Prepare0v58(Prepare0):

    param_nml_reference = Stage.Input('default')
    wwminput_nml_reference = Stage.Input('default')

    param_nml = Stage.Output()
    wwminput_nml = Stage.Output()

    _param_nml_group_setting_map = dict(
        CORE = dict(
            rnday = dict(type=float),
            dt = dict(type=int),
        ),
        OPT = dict(
            dramp = dict(type=float),
            indvel = dict(type=int),
            ihorcon = dict(type=int),
            hvis_coef0 = dict(type=float),
            ishapiro = dict(type=int),
            shapiro = dict(name='shapiro0', type=float),
            thetai = dict(type=float),
            h0 = dict(type=float),
        ),
        SCHOUT = dict(
        )
    )
    _param_nml_settings_map = { name: (group, details)
        for group, settings_map in _param_nml_group_setting_map.items()
            for name, details in settings_map.items()
    }

    def _resolve_configuration_files(self):
        super()._resolve_configuration_files()

        param_nml_text = Configuration.objects.get(
            reference = self.param_nml_reference,
            target = self.wiff_config['run_type'],
            kind = Configuration.Kind.PARAM_NML,
        ).text
        param_nml = ParamNml(param_nml_text)

        for key, value in self.param_in.items():
            try:
                group, details = self._param_nml_settings_map[key]
            except: # KeyError
                self.logger.debug('param.nml: %s = %s ignored!', key, value)
            else:
                name = details.get('name') or key
                typed_value = details['type'](value)

                param_nml.namelist[group][name] = typed_value
                self.logger.debug('param.nml: %s.%s = %s',
                                                        group, key, typed_value)

        self.param_nml = param_nml

        if self.simulation_kind in self.SimulationKind.WAVES_KINDS:

            wwminput_nml_text = Configuration.objects.get(
                reference = self.wwminput_nml_reference,
                target = self.wiff_config['run_type'],
                kind = Configuration.Kind.WWMINPUT_NML,
            ).text
            wwminput_nml = f90nml.Parser().reads(wwminput_nml_text)

            wwm_timestep = timedelta(
                                seconds=float(self.wwminput_nml['PROC_DELTC']) )
            wwminput_nml['PROC']['DELTC'] = wwm_timestep.total_seconds()
            wwminput_nml['PROC']['UNITC'] = 'SEC'

            wwminput_nml['ENGS']['BRHD'] = float(self.wwminput_nml['ENGS_BRHD'])

            self.wwminput_nml = wwminput_nml

            self.param_nml.wwm_step = wwm_timestep/self.param_nml.timestep


class Update0(Stage, FileHandler, DjangoDeployment):

    date_time = Stage.Input()
    publish_outputs = Stage.Input(default=True)
    series_root = Stage.Input(optional=True)
    hotstart_path = Stage.Input(optional=True) # workaround
    ncwms_name = Stage.Input(optional=True)
    ncwms_deployment_location = Stage.Input(optional=True)
    notifications = Stage.Input(default=True)

    files_to_publish = (
        ('outputs/?_*.6?*', DeploymentRunOutputFilepath.Kind.RESULTS),
        ('outputs/?_all-ugrid.nc', DeploymentRunOutputFilepath.Kind.RESULTS),
        ('outputs/*_hotstart.in*', DeploymentRunOutputFilepath.Kind.INPUTS),
        ('outputs/wwm_hotstart.nc*', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    def execute(self):
        if self.publish_outputs:
            self._publish_files()
            self._update_ncwms_dataset()

        self._update_last_run()

        if self.notifications:
            self._notify_user()

    _ncwms_file_glob = '*_all-ugrid.nc'

    def _update_ncwms_dataset(self):
        if not self.ncwms_name:
            self.logger.info('no ncWMS server defined!')
            return

        ncwms_server = ncWMS.servers.get(self.ncwms_name)
        if not ncwms_server:
            self.logger.warning('invalid ncWMS server name %r!',
                                                                self.ncwms_name)
            return

        self.logger.info('ncWMS server name %r', self.ncwms_name)

        ncwms_path = self._build_ncwms_dataset_folder()

        ncwms_file_paths = tuple( ncwms_path.glob(self._ncwms_file_glob) )
        if not ncwms_file_paths:
            self.logger.info('no netcdf file path(s) exposed!')
            return

        self.logger.info('netcdf file paths: %s', ncwms_file_paths)

        logger_msg = lambda action: ( f'%s {action} @ %s', self.deployment,
                                                               self.ncwms_name )

        # TODO: if location has changed it should be replaced instead of updated
        refresh = ncwms_server.deployment_update(self.deployment)
        if refresh:
            self.logger.info( *logger_msg('refreshed') )

        else:
            if not self.ncwms_deployment_location:
                self.logger.info('no ncWMS deployment location set!')
                return

            add = ncwms_server.deployment_add(self.deployment,
                    f'{self.ncwms_deployment_location}/{self._ncwms_file_glob}')
            if add:
                self.logger.info( *logger_msg('added') )
            else:
                self.logger.info( *logger_msg('not added') )

    _ncwms_folder = '_ncwms'
    _ncwms_previous_file_index = 1
    _ncwms_current_files_indexes = (1, 2)

    def _build_ncwms_dataset_folder(self):
        ncwms_path = self.create_folder(self._ncwms_folder)
        dataset_file_paths = list()

        name_of_ = lambda index: self._ncwms_file_glob.replace('*', str(index) )

        if self.hotstart_path:
            dataset_file_paths.append(self.hotstart_path.parent/
                                     name_of_(self._ncwms_previous_file_index) )

        for current_file_index in self._ncwms_current_files_indexes:
            dataset_file_paths.append(self.path/'outputs'/
                                                  name_of_(current_file_index) )

        for link_prefix, target_path in enumerate(dataset_file_paths):
            link_path = ncwms_path / name_of_(link_prefix)
            if target_path.exists():
                self.link_file(target_path, link_path)

        if self.series_root:
            series_ncwms_link = self.series_root / self._ncwms_folder
            return self.link_file(ncwms_path, series_ncwms_link, force=True)
        
        return ncwms_path

    def _notify_user(self):
        run_date = self.date_time.date()
        end_date = self.deployment.end_date

        is_first_run = not self.deployment.last_run_date
        is_expiring = (end_date - run_date) == timedelta(days=7)
        is_expired = run_date == end_date

        if is_first_run:
            subject = 'Início de novo sistema'
            self._notify_author_by_email(subject, run_date,
                                                           'deployment_1st_run')

        elif is_expiring:
            subject = 'Sistema quase a expirar'
            self._notify_author_by_email(subject, run_date,
                                                          'deployment_expiring')

        elif is_expired:
            subject = 'Sistema expirou'
            self._notify_author_by_email(subject, run_date,
                                                           'deployment_expired')

        else:
            return

        self.logger.info("Deployment's author notified.")

    def _update_last_run(self):
        run_date = self.date_time.date()

        self.deployment.last_run_date = run_date
        self.deployment.save(update_fields=['last_run_date'])
        self.logger.info("%s's last run date updated to: %s",
                                                      self.deployment, run_date)


from wiff.models.schism.data.schout import adapt_to_ncwms


class Update0v58(Update0):

    files_to_publish = (
        ('outputs/schouts_*.nc*', DeploymentRunOutputFilepath.Kind.RESULTS),
        ('outputs/hotstart_it=*.nc*', DeploymentRunOutputFilepath.Kind.INPUTS),
        ('outputs/wwm_hotstart.nc*', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    _ncwms_file_glob = 'schouts_*.nc'

    _fib_kind_factor_map = dict(
        fecal = dict(
            FIB_1 = .0001,
            FIB_2 = .0001,
        )
    )

    def _update_ncwms_dataset(self):
        outputs_path = self.path / 'outputs'

        fib_kind = self.wiff_config.get('fib_kind')
        fib_factors = self._fib_kind_factor_map.get(fib_kind) or dict()

        for netcdf_path in outputs_path.glob(self._ncwms_file_glob):
            adapt_to_ncwms(netcdf_path, self.path/'hgrid.ll', fib_factors)

        return super()._update_ncwms_dataset()


class Finalize0(Stage, DjangoDeployment):

    path = Stage.Input()
    date_time = Stage.Input()
    publish = Stage.Input(default=True)
    notification = Stage.Input(default=True)

    _stages_failed = Stage.Input(optional=True)

    def execute(self):
        self._finalize_run_record()

        if self.publish:
            self._publish_files()

        if self._stages_failed and self.notifications:
            self._notify_admin_by_email('Falha na execução do sistema',
                                    self.date_time.date(), 'deployment_failure')

    files_to_publish = (
        ('logs.tar.xz', DeploymentRunOutputFilepath.Kind.LOGS),
        ('confs.tar.xz', DeploymentRunOutputFilepath.Kind.CONFS),
        ('forcings.tar.xz', DeploymentRunOutputFilepath.Kind.INPUTS),
    )

    _publish_files = Update0._publish_files

    def _finalize_run_record(self):
        try:
            deployment_run = DeploymentRun.objects.get(
                deployment = self.deployment,
                run_datetime = self.date_time,
            )
        except DeploymentRun.DoesNotExist:
            self.logger.info('DeploymentRun record not found!')
            return

        run_state = DeploymentRun.State.FAILED if self._stages_failed \
                                               else DeploymentRun.State.FINISHED

        deployment_run.state = run_state
        deployment_run.end = datetime.now()
        deployment_run.save()

        self.logger.info('Updated DeploymentRun record #%i.', deployment_run.id)
