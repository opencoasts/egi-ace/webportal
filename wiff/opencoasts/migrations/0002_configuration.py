# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-06-26 00:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wiff', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('reference', models.SlugField()),
                ('description', models.TextField(blank=True)),
                ('notes', models.TextField(blank=True)),
                ('target', models.CharField(choices=[('basic2d', 'Barotropic'), ('basic3d', 'Baroclinic'), ('waves2d', 'Barotropic with Waves'), ('waves3d', 'Baroclinic with Waves')], max_length=10)),
                ('kind', models.CharField(choices=[('param_nml', 'param.nml'), ('wwminp_nml', 'wwminput.nml')], max_length=10)),
                ('text', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
