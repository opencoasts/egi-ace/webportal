from django.apps import AppConfig


class WiffConfig(AppConfig):

    name = 'wiff.opencoasts'
    verbose_name = 'WIFF'

    label = 'wiff'

    def ready(self):
        from . import signals
