#!/usr/bin/env bash

# NOTE: the _ (underscore) prefix avoids name collisions
_SELF=$0

_PROJECT_PATH=$( dirname -z $( dirname $( dirname $_SELF ) ) )

# $_SELF.vars.template may be used as the basis for a $_SELF.vars
# defined by its name/path, from $_SELF, joined with '.vars' suffix
_SOURCE_VARS=${VARS:-$_SELF.vars}

# > OC_DJANGO_SETTINGS_MODULE (ProjOpenCoastS.settings), OC_PYTHON (python),
# and OC_PYTHONPATH ($_PROJECT_PATH::PYTHONPATH) are optional and unless
# provided their defaults, enclosed in (), are assumed
[ -f "$_SOURCE_VARS" ] && source $_SOURCE_VARS && echo "$_SOURCE_VARS sourced!"

_DJANGO_SETTINGS_MODULE=${OC_DJANGO_SETTINGS_MODULE:-ProjOpenCoastS.settings}
_PYTHONPATH=$_PROJECT_PATH:$OC_PYTHONPATH:$PYTHONPATH
_PYTHON=${OC_PYTHON:-python}

_MODULE=${1:?"Module not provided!"}
_CMD_ARGS=${*:2}

begin_date=$(date)

echo "
> BEGIN ### OPENCoastS WIFF [started on $begin_date] ### BEGIN <

Settings:

  PYTHON=$_PYTHON
  PYTHONPATH=$_PYTHONPATH
  DJANGO_SETTINGS_MODULE=$_DJANGO_SETTINGS_MODULE
  OPTIONAL_ARGS=$OC_OPTIONAL_ARGS
"

PYTHONPATH=$_PYTHONPATH \
DJANGO_SETTINGS_MODULE=$_DJANGO_SETTINGS_MODULE \
    $_PYTHON -m "wiff.opencoasts.$_MODULE" $OC_OPTIONAL_ARGS $_CMD_ARGS

echo "
> END ### OPENCoastS WIFF [started on $begin_date] ### END <
"