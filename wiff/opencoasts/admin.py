from django.contrib.admin import ModelAdmin, register
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget

from .models import *


@register(Deployment)
class DeploymentAdmin(ModelAdmin):

    formfield_overrides = {
        fields.JSONField: dict(
            widget = JSONEditorWidget
        ),
    }


@register(Configuration)
class ConfigurationAdmin(ModelAdmin):

    list_display = ('name', 'reference', 'target', 'kind')
    ordering = ('target', 'kind', 'reference')
