import logging
import pickle
import sys
from datetime import datetime, date, time, timedelta
from django.conf import settings
from functools import partial
from itertools import takewhile
from pathlib import Path
from time import sleep

if __name__ == '__main__':
    import django
    django.setup()

from datamodel.models import Deployment
from wiff.launcher import LOGGING_FMT, WW3Launcher, SCHISMLauncher


def remaining_steps(all_steps, unsuccessful_step_ids):

    steps_to_rerun = list(all_steps)
    for step in all_steps:
        if step.id in unsuccessful_step_ids:
            break
        else:
            steps_to_rerun.remove(step)

    return steps_to_rerun

def launch_simulation(launcher, deployment_id, run_date,
                                                context=None, interactive=True):

    deployment = Deployment.objects.get(id=deployment_id)
    forecast = launcher.setup_deployment_forecast(deployment, overwrite=True)

    kwargs = dict(
        date_time = datetime.combine( run_date, time(0) ),
        # ws_dirname = '{period_hours}hours-testing',
        # ws_replacing = not context,
        notifications = False,
        interactive = interactive,
    )

    _context = context or dict()
    unsuccessful_step_ids = _context.get( 'steps_failed', tuple() ) + \
                                        _context.get( 'steps_skipped', tuple() )
    if unsuccessful_step_ids:
        kwargs['steps'] = remaining_steps(forecast.simulation.steps,
                                                          unsuccessful_step_ids)

    kwargs.update(_context)
    forecast.run_simulation(**kwargs)

import argparse
from multiprocessing import cpu_count

def setup_argparser():
    cmd_parser = argparse.ArgumentParser(
        description = 'OPENCoastS testing launcher'
    )

    cmd_parser.add_argument('root_path',
        help = "forecasting root path")

    cmd_parser.add_argument('deployment_id',
        help = "deployment ID")

    cmd_parser.add_argument('-o', '--offset', metavar='<days>', type=int,
                                                  default=0, dest='offset_days',
        help = "run date offset, past days")

    cmd_parser.add_argument('-c', '--context', metavar='<pickle file>',
                                                            dest='context_path',
        help = "initial context to use, a .pickle file")

    cmd_parser.add_argument('-i', '--interactive', action='store_true',
                                                             dest='interactive',
        help = "ask for confirmation before execute each step")

    cmd_parser.add_argument('-d', '--debug', action='store_true',
        help='set debug logging level')

    cmd_parser_execution = cmd_parser.add_mutually_exclusive_group()

    cmd_parser_execution.add_argument('-m', '--mpi_procs',
                                metavar='COUNT', type=int, default=0, nargs='?',
        help = "run locally using MPI, process count may be set")

    cmd_parser_execution.add_argument('-s', '--sproxy', action='store_true',
        help = "use sproxy to offload the simulation run")


    return cmd_parser

if __name__ == '__main__':

    cmd_args = setup_argparser().parse_args()
    print(cmd_args)

    print("Starting in 3 seconds (Ctrl + C to stop)!")
    sleep(3)

    log_lvl = 'DEBUG' if cmd_args.debug else 'INFO'
    logging.basicConfig(level=log_lvl, format=LOGGING_FMT)

    _root_path = Path(cmd_args.root_path).resolve()

    launcher_dict = dict(
        root_path = _root_path,
        ncwms_name = getattr(settings, 'NCWMS_NAME', None),
    )
    sproxy_launcher = partial(SCHISMLauncher.with_sproxy_compute,
                                       label_prefix='test_', **launcher_dict)
    local_mpi_launcher = partial(SCHISMLauncher.with_local_mpi_compute,
                       mpirun_args=('-np', cmd_args.mpi_procs), **launcher_dict)

    launcher = sproxy_launcher() if cmd_args.sproxy else local_mpi_launcher()
    run_date = date.today() - timedelta(days=cmd_args.offset_days)
    init_context = cmd_args.context_path and \
                        pickle.loads( Path(cmd_args.context_path).read_bytes() )

    launch_simulation(launcher, cmd_args.deployment_id, run_date, init_context,
                                                           cmd_args.interactive)
