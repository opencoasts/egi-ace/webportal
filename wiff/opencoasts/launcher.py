import logging
import multiprocessing.pool
import sys
from datetime import datetime, date, time, timedelta
from django.conf import settings
from multiprocessing import cpu_count
from pathlib import Path
from time import sleep

if __name__ == '__main__':
    import django
    django.setup()

from datamodel.models import Deployment
from wiff import steps as schism_steps_oc

from core.utils import QueuedExecution
from simulation.core.entities import Series, Simulation
from simulation.schism import steps as schism_steps
from simulation.ww3 import steps as ww3_steps

DAILY_RUN_OFFSET = timedelta(days=1)
DEPLOYMENTS_DIR = 'deployments/opencoasts'
WW3_DEPLOYMENT_DIR = '__waves-na__'

LOGGING_FILE_LEVEL = logging.DEBUG
LOGGING_CONSOLE_LEVEL = logging.DEBUG

LOGGING_FMT = '%(asctime)s %(levelname)s' \
                  ' %(name)s # %(module)s.%(funcName)s:%(lineno)s "%(message)s"'

logger = logging.getLogger(__name__)


class WW3Launcher:

    bin_dir = 'bin/ww3'
    static_dir = 'static/ww3'

    def __init__(self, *, root_path, mpirun_args=() ):

        self.root_path = Path(root_path)
        dir_path = lambda dir: self.root_path / dir

        self.bin_path = dir_path(self.bin_dir)
        self.static_path = dir_path(self.static_dir)
        self.deployment_path = dir_path(DEPLOYMENTS_DIR) / WW3_DEPLOYMENT_DIR

        self.mpirun_args = mpirun_args

    def setup_forecast(self):
        step_init_common = dict(
            bin_path = self.bin_path,
            static_path = self.static_path,

            _registry_strict = False,
        )

        prepare = ww3_steps.Prepare('prepare',
            **step_init_common,
        )

        forcings = ww3_steps.Forcings('forcings',
            dependencies = (prepare, ),
            **step_init_common,
            extent = dict(
                lat=(0., 75.),
                lon=(-98., 13.)
            )
        )

        compute = ww3_steps.Compute('compute',
            dependencies = (forcings, ),
            **step_init_common,
            mpirun_args = self.mpirun_args,
        )

        outputs = ww3_steps.Outputs('outputs',
            dependencies = (compute, ),
            **step_init_common,
        )

        simulation = Simulation('ww3',
            steps = (prepare, forcings, compute, outputs),
            period = timedelta(hours=48),
        )

        return Series('waves-na',
            simulation = simulation,
            interval = timedelta(hours=24),

            path = self.deployment_path,
        )

    def launch_simulation(self, run_date, **kwargs):
        logger.debug(' >> launch_simulation(%s, %s)', run_date, kwargs)

        try:
            forecast = self.setup_forecast()

            logger.info(' ### STARTED SIMULATION: main WW3 @ %s ###', run_date)

            forecast.run_simulation(
               date_time = datetime.combine( run_date, time(0) ),
            )

            logger.info(' ### FINISHED SIMULATION: main WW3 @ %s ###', run_date)

        except:
            logger.exception('something went wrong while running main WW3')


class SCHISMLauncher:

    bin_dir = 'bin'
    static_dir = 'static'
    ww3_bin_dir = 'bin/ww3'
    ww3_static_dir = 'static/ww3'

    def __init__(self, *, root_path, compute_step, mpirun_args=(),
                                                admin_emails=(), ncwms_name=''):

        self.root_path = Path(root_path)
        dir_path = lambda dir: self.root_path / dir

        self.bin_path = dir_path(self.bin_dir)
        self.static_path = dir_path(self.static_dir)
        self.deployment_path = dir_path(DEPLOYMENTS_DIR)

        self.ww3_bin_path = dir_path(self.ww3_bin_dir)
        self.ww3_static_path = dir_path(self.ww3_static_dir)

        self.compute_step = compute_step
        self.mpirun_args = mpirun_args
        self.admin_emails = admin_emails
        self.ncwms_name = ncwms_name

    @classmethod
    def with_local_mpi_compute(cls, **kwargs):
        return cls(
                compute_step = (schism_steps.Compute, dict(
                    mpirun_args = kwargs.get('mpirun_args') or (),
                ) ),

                **kwargs
            )

    @classmethod
    def with_sproxy_compute(cls, label_prefix='', **kwargs):
        return cls(
                compute_step = (schism_steps.SProxyCompute, dict(
                    url = settings.SPROXY_URL,
                    blob_service = settings.SPROXY_BLOB_SERVICE,
                    job_service = settings.SPROXY_JOB_SERVICE,
                    label_prefix = label_prefix,
                ) ),

                **kwargs
            )

    wwm_prepared_dir_template = \
                              f'{WW3_DEPLOYMENT_DIR}/%Y/%m/%d/00/ww3/__latest__'
    ncmws_location_template = '{did}/latest'

    def setup_deployment_forecast(self, deployment, overwrite=False):

        deployment_config = deployment.wiff.config
        deployments_root_path = self.deployment_path

        step_init_common = dict(
            bin_path = self.bin_path,
            static_path = self.static_path,

            _registry_strict = False,
        )

        prepare_oc = schism_steps_oc.Prepare('prepare_oc',
            deployment = deployment,

            **step_init_common,
        )

        prepare_wiff = schism_steps.Prepare('prepare_wiff',
            dependencies = (prepare_oc, ),
            root_path = self.root_path,
            file_overwrite = overwrite,

            **step_init_common,
        )

        prepare_wwm = schism_steps.PrepareWWM('prepare_wwm',
            dependencies = (prepare_wiff, ),
            prepared_path_template =
                    f'{deployments_root_path}/{self.wwm_prepared_dir_template}',

            ww3_bin_path = self.ww3_bin_path,
            ww3_static_path = self.ww3_static_path,
            ww3_mpirun_args = self.mpirun_args,

            _registry_strict = False,
        )

        compute_class, compute_dict = self.compute_step

        label_prefix = compute_dict.pop('label_prefix', None)
        if label_prefix:
            compute_dict['label'] = f'{label_prefix}{deployment.id}'

        compute = compute_class('compute',
            timestep = timedelta(seconds=deployment_config['timestep_seconds']),
            dependencies = (prepare_wwm, ),
            **compute_dict,

            **step_init_common,
        )

        combine = schism_steps.Combine('combine',
            dependencies = (compute, ),

            **step_init_common,
        )

        cleanup = schism_steps.Cleanup('cleanup',
            **step_init_common,
        )

        update_oc = schism_steps_oc.Update('update_oc',
            dependencies = (combine, ),
            deployment = deployment,
            ncwms_name = self.ncwms_name,
            ncwms_deployment_location =
                         self.ncmws_location_template.format(did=deployment.id),

            _registry_strict=False
        )

        finalize_oc = schism_steps_oc.Finalize('finalize_oc',
            dependencies = (prepare_oc, ),
            deployment = deployment,
            admin_emails = self.admin_emails or tuple(),

            _registry_strict=False
        )

        logger.debug(' ### SETTING UP SIMULATION: %s ###', deployment)

        simulation = Simulation('schism',
            steps = (prepare_oc, prepare_wiff, prepare_wwm, compute, combine,
                                               cleanup, update_oc, finalize_oc),
            # steps = (prepare_oc, prepare_wiff, prepare_wwm, compute, combine,
            #                                                            cleanup),
            period = timedelta(hours=deployment_config['period_hours']),
            run_log_settings = dict(
                level = logging.DEBUG,
                formatter = logging.Formatter(fmt=LOGGING_FMT),
                # filters = (),
            ),
        )

        logger.debug(' ### SETTING UP SERIES: %s ###', deployment)

        forecast_path = deployments_root_path / str(deployment.id)
        forecast_path.mkdir(exist_ok=True, parents=True)

        return Series(f'forecast_{deployment.id}',
            simulation = simulation,
            interval = timedelta(hours=24),
            begin = deployment.begin_run_date,
            end = deployment.end_run_date,
            path = forecast_path,
        )

    concurrent_wait = timedelta(minutes=1)

    # include timmings
    def launch_simulations(self, run_date, pool_size=1, **kwargs):
        logger.debug('run date: %s', run_date)

        deployment_set = (
            Deployment.objects
                .filter(deleted = False)
                .filter(active = True)
                .filter(begin_date__lte = run_date)
                .filter(end_date__gte = run_date)
                .order_by('id')
        )
        logger.info('deployment set: %s', deployment_set)

        concurrent = pool_size > 1

        if concurrent:
            from simulation.mixins import ProcessHandler
            ProcessHandler.mpirun_queue.set_concurrency(pool_size)

            # optimization to allow running non queued code on extra workers
            # eg: while heavy computation (via mpirun) are limited by pool_size,
            # extra workers will getting ready (fetching forcings and stuff
            # like that) to start crunching as soon as resources become free
            pool_size += 1

            QueuedExecution.enable()

        with multiprocessing.pool.ThreadPool(processes=pool_size) as pool:

            for deployment in deployment_set:

                args = (deployment, run_date)

                logger.debug(' > apply_async(%s, %s, %s)',
                               self.launch_deployment_simulations, args, kwargs)
                pool.apply_async(self.launch_deployment_simulations, args,
                                                                         kwargs)

                #pool.wait(self.concurrent_wait.total_seconds() )
                if concurrent:
                    sleep(self.concurrent_wait.total_seconds() )

            pool.close()
            pool.join()

    def launch_deployment_simulations(self, deployment, run_date, **kwargs):
        logger.debug(' >> launch_deployment_simulations(%s, %s, %s)',
                                                   deployment, run_date, kwargs)

        last_run_date = deployment.last_run_date
        previous_run_date = run_date - DAILY_RUN_OFFSET

        next_run_date = previous_run_date
        logger.debug('next_run_date: %s', next_run_date)

        if last_run_date:

            if last_run_date == previous_run_date:
                next_run_date = run_date

            elif last_run_date >= run_date:
                logger.info('as of %s, %s is already up to date!', run_date,
                                                                     deployment)
                return

        next_run_date = run_date

        # last_run_date = deployment_base.last_run_date
        # if last_run_date and last_run_date >= run_date:
        #     return

        # previous_run_date = run_date - DAILY_RUN_OFFSET
        # last_run_is_uptodate = last_run_date and \
        #                                       last_run_date >= previous_run_date

        # next_run_date = previous_run_date if last_run_is_outdated else run_date

        try:
            while next_run_date <= run_date:
                forecast = self.setup_deployment_forecast(deployment, **kwargs)

                begin_dt = datetime.now()
                logger.info(' ### STARTED SIMULATION: %s @ %s ###', deployment,
                                                                  next_run_date)

                forecast.run_simulation(
                   date_time = datetime.combine( next_run_date, time(0) ),
                )

                duration = datetime.now() - begin_dt
                logger.info(' ### FINISHED SIMULATION: %s @ %s, took %s ###',
                                            deployment, next_run_date, duration)

                next_run_date += DAILY_RUN_OFFSET

        except:
            logger.exception('something went wrong while running %s',
                                                                     deployment)

def setup_logging(root_path, console_verbose=False, base_logger=logger,
                console_lvl=LOGGING_CONSOLE_LEVEL, file_lvl=LOGGING_FILE_LEVEL):

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.NOTSET)
    logger_formatter = logging.Formatter(fmt=LOGGING_FMT)

    # outputs just launcher log records to console
    console_output = logging.StreamHandler()
    console_output.setLevel(console_lvl)
    console_output.setFormatter(logger_formatter)

    console_logger = root_logger if console_verbose else base_logger
    console_logger.addHandler(console_output)

    # logs everything to weekly rotating file
    file_log_path = root_path / 'log/launcher'
    file_log = logging.handlers.TimedRotatingFileHandler(file_log_path,
                                                      when='W1', backupCount=10)
    file_log.setLevel(file_lvl)
    file_log.setFormatter(logger_formatter)
    root_logger.addHandler(file_log)

import argparse

def setup_argparser():
    cmd_parser = argparse.ArgumentParser(
        description = 'OPENCoastS launcher'
    )

    cmd_parser.add_argument('root_path',
        help = "forecasting root path")

    # cmd_parser.add_argument('deployment_id',
    #     help = "deployment ID")

    cmd_parser.add_argument('-o', '--offset', dest='offset_days',
                                            metavar='DAYS', type=int, default=0,
        help = "run date offset (days: positive=past, negative=future)")

    cmd_parser.add_argument('-c', '--concurrency', dest='threads',
                          metavar='CONCURRENT_DEPLOYMENTS', type=int, default=1,
        help = "launcher thread count (threaded deployment launching)")

    # cmd_parser.add_argument('-r', '--recover', dest='recover_days',
    #                                       metavar='<days>', type=int, default=0,
    #     help = "recover period (days)")

    cmd_parser.add_argument('-v', '--verbose', action='store_true',
        help='print everything to terminal')

    # cmd_parser.add_argument('-d', '--debug', action='store_true',
    #     help='set debug logging level')

    cmd_parser_execution = cmd_parser.add_mutually_exclusive_group()

    # cmd_parser_execution.add_argument('-m', '--mpi', dest='mpirun_args',
    #                                metavar='MPIRUN ARGS', default='', nargs='?',
    #     help = "run locally using MPI, mpirun arguments may defined")

    cmd_parser_execution.add_argument('-s', '--sproxy', action='store_true',
        help = "use sproxy to offload the simulation run")

    cmd_parser_models = cmd_parser.add_mutually_exclusive_group()

    cmd_parser_models.add_argument('--only_ww3', action='store_true',
        help = "run only main WW3 simulation")

    cmd_parser_models.add_argument('--only_schism', action='store_true',
        help = "run only main SCHISM simulations")

    return cmd_parser

if __name__ == '__main__':

    from functools import partial

    cmd_args = setup_argparser().parse_args()
    print(cmd_args)

    print("Starting in 10 seconds (Ctrl + C to stop)!")
    sleep(10)

    root_path = Path(cmd_args.root_path).resolve()

    setup_logging(root_path, console_verbose=cmd_args.verbose)

    logger.debug('cmd_args: %s', cmd_args)

    run_date = date.today() - timedelta(days=cmd_args.offset_days)

    if not cmd_args.only_schism:
        WW3Launcher(
            root_path = root_path,
            mpirun_args = getattr(settings, 'MPIRUN_ARGS_WW3_MAIN', () ),
        ).launch_simulation(run_date)

    if not cmd_args.only_ww3:
        schism_launcher = SCHISMLauncher.with_sproxy_compute if cmd_args.sproxy \
                                      else SCHISMLauncher.with_local_mpi_compute
        schism_launcher(
            root_path = root_path,
            mpirun_args = getattr(settings, 'MPIRUN_ARGS_DEPLOYMENTS', () ),
            ncwms_name = getattr(settings, 'NCWMS_NAME', None),
            admin_emails = settings.ADMINS,
        ).launch_simulations(run_date, pool_size=cmd_args.threads)

    logging.shutdown()
