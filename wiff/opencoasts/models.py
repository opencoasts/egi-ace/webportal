from django.db.models import CASCADE, Model, OneToOneField, CharField, TextField
from django.contrib.postgres.fields import JSONField

from datamodel.models import Name, Info, Deployment


class Deployment(Model):

    parent = OneToOneField(Deployment, related_name='wiff', on_delete=CASCADE,
                                                                 editable=False)
    # datamodel = OneToOneField(Deployment, related_name='wiff',
    #                                           on_delete=CASCADE, editable=False)

    config = JSONField(blank=True, null=True)

    def __str__(self):
        return str(self.parent)
        # return str(self.datamodel)


class Configuration(Name, Info):

    class Target:

        BAROTROPIC = 'basic2d'
        BAROTROPIC_WAVES = 'waves2d'
        BAROTROPIC_MICROBIOLOGY = 'mbio2d'
        BAROTROPIC_WAVES_MICROBIOLOGY = 'wavesmbio2d'

        BAROCLINIC = 'basic3d'
        BAROCLINIC_WAVES = 'waves3d'
        BAROCLINIC_MICROBIOLOGY = 'mbio3d'
        BAROCLINIC_WAVES_MICROBIOLOGY = 'wavesmbio3d'

        _CHOICES = (
            (BAROTROPIC, 'Barotropic'),
            (BAROTROPIC_WAVES, 'Barotropic with Waves'),
            (BAROTROPIC_MICROBIOLOGY, 'Barotropic with Microbiology'),
            (BAROTROPIC_WAVES_MICROBIOLOGY,
                                      'Barotropic with Waves and Microbiology'),

            (BAROCLINIC, 'Baroclinic'),
            (BAROCLINIC_WAVES, 'Baroclinic with Waves'),
            (BAROCLINIC_MICROBIOLOGY, 'Baroclinic with Microbiology'),
            (BAROCLINIC_WAVES_MICROBIOLOGY,
                                      'Baroclinic with Waves and Microbiology'),
        )

    class Kind:

        PARAM_NML = 'param_nml'
        WWMINPUT_NML = 'wwminp_nml'

        _CHOICES = (
            (PARAM_NML, 'param.nml'),
            (WWMINPUT_NML, 'wwminput.nml'),
        )

    target = CharField(max_length=20, choices=Target._CHOICES)
    kind = CharField(max_length=20, choices=Kind._CHOICES)
    text = TextField(blank=True)
