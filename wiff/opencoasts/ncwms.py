import logging
import requests
from pathlib import Path

from wiff.data.ncwms import ncWMS as ncWMSbase


logger = logging.getLogger(__name__)

class ncWMS(ncWMSbase):
    # TODO: add debug and __str__/__repr__

    logger = logger.getChild(__qualname__)

    servers = dict()

    def __init__(self, did_template, root_path='/', name=None, **kwargs):
        super().__init__(**kwargs)

        self.did_template = did_template
        self.root_path = Path(root_path)

        if name:
            type(self).servers[name] = self

            self.logger = type(self).logger.getChild(name)
            self.logger.info("%r ncWMS server added", name)

        self.logger.debug("new ncWMS server created: %r", self)

    def __repr__(self):
        return '\n'.join(
            f'{__qualname__}:',
            f'  did_template = {self.did_template}',
            f'  root_path = {self.root_path}',
            f'super({super().__repr__()})',
        )

    def _reply(self, response):
        if not response.ok:
            self.logger.debug("response.headers=%s, response.request.body=%s",
                                        response.headers, response.request.body)
            self.logger.debug("response not ok, %s: %s",
                                                        response, response.text)
            return

        return response

    def deployment_info(self, deployment):
        dataset_id = self.did_template.format(id=deployment.id)

        try:
            self.logger.debug("getting info for %r dataset", deployment)
            info = self.dataset_info(dataset_id)

        except (IOError, Warning) as error:
            self.logger.warning("error while getting info for %r dataset: %s",
                                                              deployment, error)
            return

        return self._reply(info)

    def deployment_add(self, deployment, netcdf_path, more_info=None):
        location_path = self.root_path / netcdf_path
        new_deployment_config = dict(
            did = self.did_template.format(id=deployment.id),
            title = deployment.name,
            location = str(location_path),
            more_info = more_info or deployment.description,
            copyright = 'OPENCoastS',
            queryable = True,
            downloadable = True,
            auto_refresh_minutes = -1, # off
        )

        try:
            self.logger.debug("adding %r dataset", deployment)
            add = self.dataset_add(**new_deployment_config)

        except (IOError, Warning) as error:
            self.logger.warning("error while adding %r dataset: %s",
                                                              deployment, error)
            return

        return self._reply(add)

    def deployment_update(self, deployment):
        dataset_id = self.did_template.format(id=deployment.id)

        try:
            self.logger.debug("updating %r dataset", deployment)
            update = self.dataset_refresh(dataset_id)

        except (IOError, Warning) as error:
            self.logger.warning("error while updating %r dataset: %s",
                                                              deployment, error)
            return

        return self._reply(update)

    def deployment_remove(self, deployment):
        dataset_id = self.did_template.format(id=deployment.id)

        try:
            self.logger.debug("removing %r dataset", deployment)
            remove = self.dataset_info(dataset_id)

        except (IOError, Warning) as error:
            self.logger.warning("error while removing %r dataset: %s",
                                                              deployment, error)
            return

        return self._reply(remove)
