import json
from collections import OrderedDict
from functools import partial

from django.dispatch import receiver
from django.db.models import signals

from front.models import Deployment as FrontDeployment
from .models import Deployment, Configuration

_is_open_boundary = lambda kind: kind.lower() == 'open'

# _run_type_map = dict(
#     basic2d = Configuration.Target.BAROTROPIC,
#     basic3d = Configuration.Target.BAROCLINIC,
#     waves2d = Configuration.Target.BAROTROPIC_WAVES,
#     waves3d = Configuration.Target.BAROCLINIC_WAVES,
#     mbiofecal2d = Configuration.Target.BAROTROPIC_MICROBIOLOGY,
#     mbiofecal3d = Configuration.Target.BAROCLINIC_MICROBIOLOGY,
#     mbiogtracer2d = Configuration.Target.BAROTROPIC_MICROBIOLOGY,
#     mbiogtracer3d = Configuration.Target.BAROCLINIC_MICROBIOLOGY,
#     mbiofecalwaves2d = Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY,
#     mbiofecalwaves3d = Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY,
#     mbiogtracerwaves2d = Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY,
#     mbiogtracerwaves3d = Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY,
# )
_run_type_traits = ('baroclinic_type', 'waves_type', 'wq_type')
_run_type_map = {
    ('basic2d', 'nowaves', 'nowqtype'): Configuration.Target.BAROTROPIC,
    ('basic2d', 'nowaves', 'fecalwqtype'):
                                   Configuration.Target.BAROTROPIC_MICROBIOLOGY,
    ('basic2d', 'nowaves', 'genericwqtype'):
                                   Configuration.Target.BAROTROPIC_MICROBIOLOGY,

    ('basic2d', 'yeswaves', 'nowqtype'): Configuration.Target.BAROTROPIC_WAVES,
    ('basic2d', 'yeswaves', 'fecalwqtype'):
                             Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY,
    ('basic2d', 'yeswaves', 'genericwqtype'):
                             Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY,

    ('basic3d', 'nowaves', 'nowqtype'): Configuration.Target.BAROCLINIC,
    ('basic3d', 'nowaves', 'fecalwqtype'):
                                   Configuration.Target.BAROCLINIC_MICROBIOLOGY,
    ('basic3d', 'nowaves', 'genericwqtype'):
                                   Configuration.Target.BAROCLINIC_MICROBIOLOGY,

    ('basic3d', 'yeswaves', 'nowqtype'): Configuration.Target.BAROCLINIC_WAVES,
    ('basic3d', 'yeswaves', 'fecalwqtype'):
                             Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY,
    ('basic3d', 'yeswaves', 'genericwqtype'):
                             Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY,
}

_fib_configuration_map = dict(
    fecalwqtype = dict(
        kind = 'fecal',
        unit_factor = 10000.,
        flag_aux_name = 'E-coliEnterococcus',
        constant_names = ('e-coli', 'enterococcus'),
    ),
    genericwqtype = dict(
        kind = 'generic',
        unit_factor = 1.,
        flag_aux_name = 'Tracer',
        constant_names = ('tracer', 'dummy'),
    ),
    nowqtype = None,
)

_fib_flag_map = dict(
    decay_constant = 1,
    decay_Canteras = 2,
    decay_Cervais = 3,
    decay_Chapra = 4,
)

@receiver(signals.pre_save, sender=FrontDeployment)
def update_aux_files_set(instance, **kwargs):

    if not instance.active:
        return

    fib_conf = _fib_configuration_map.get( instance.step1.get('wq_type') )

    if not fib_conf:
        return

    aux_file_entry = lambda name, value: \
        dict(
            name = name,
            constant = value,
            choice = 'C',
            hidden = 'Y',
        )
    aux_file_unit_entry = lambda name, value: aux_file_entry(name,
                                           float(value)*fib_conf['unit_factor'])

    is_generic = fib_conf['kind'] == 'generic'

    new_aux_files = list()
    for aux_file in instance.step6['auxfilesset']:
        aux_name = aux_file['name']


        if aux_name in ('kkfib_1', 'kkfib_2', 'FIB_hvar1', 'FIB_hvar2'):
            continue

        if is_generic and aux_name in ('Fib', 'SedimentationFib'):
            continue

        if aux_name == fib_conf['flag_aux_name']:

            if _fib_flag_map[ aux_file['value'] ] == 1:
                constant_map = aux_file['constant']
                kkfib_1_name, kkfib_2_name = fib_conf['constant_names']

                kkfib_aux_file = lambda name, source_name: \
                            aux_file_unit_entry(name, constant_map[source_name])

                new_aux_files.append( kkfib_aux_file('kkfib_1', kkfib_1_name) )
                new_aux_files.append( kkfib_aux_file('kkfib_2', kkfib_2_name) )

        elif aux_name in ('InitE-coli', 'InitTracer'):
            new_aux_files.append(
                        aux_file_unit_entry('FIB_hvar1', aux_file['constant']) )

        elif aux_name in ('InitEnterococcus', 'InitDummy_hidden'):
            new_aux_files.append(
                        aux_file_unit_entry('FIB_hvar2', aux_file['constant']) )

        new_aux_files.append(aux_file)

    if is_generic:
        new_aux_files.append( aux_file_entry('Fib', 0) )
        new_aux_files.append( aux_file_entry('SedimentationFib', 0) )

    instance.step6['auxfilesset'] = new_aux_files

@receiver(signals.post_save, sender=FrontDeployment)
def fill_deployment_config(instance, **kwargs):
# def fill_deployment_config(instance, update_fields, **kwargs):

    if not instance.active:
#     if not instance.active or update_fields:
        return

    # run_type = _run_type_map[ instance.step1['run_type'] ]
    run_type = _run_type_map[tuple( map(instance.step1.get, _run_type_traits) )]

    x_min, y_min, x_max, y_max = instance.step2['extent']

    x_min_, x_max_ = max(-180., x_min), min(180., x_max)
    y_min_, y_max_ = max(-90., y_min), min(90., y_max)

    # boundaries
    boundaries_geojson = json.loads(instance.step2['boundaries']['geojson'])

    open_boundaries_lengths = dict()
    for feature in boundaries_geojson['features']:

        feature_properties = feature['properties']

        if not _is_open_boundary(feature_properties['type']):
            continue

        boundary_code = feature_properties['code']
        boundary_length = len(feature['geometry']['coordinates'])

        open_boundaries_lengths[boundary_code] = boundary_length

    # forcings
    def boundary_forcings_item(bnd):
        name = bnd['bid']

        return name, dict(
            kind = bnd['ftype'],
            forcings = bnd['fmodel'],
            node_count = open_boundaries_lengths[name],
        )

    forcings = instance.step3.get('forcings') or tuple()
    water_quality_forcings = instance.step7.get('wq_forcings') or tuple()

    for base, water_quality in zip(forcings, water_quality_forcings):
        base['fmodel'] = dict(base['fmodel'], **water_quality['fmodel'])

    boundaries = OrderedDict( map(boundary_forcings_item, forcings) )

    param_in = instance.step5.get('params') or dict()

    vgrid_info = instance.step2.get('vgrid_info')
    vertical_levels_dict = lambda: dict(
        s_levels = vgrid_info['s_levels'],
        z_levels = vgrid_info['z_levels'],
    )

    # microbiology via FIB
    fib_flag = None
    fib_sources = instance.step7.get('sources') or ()

    fib_conf = _fib_configuration_map.get( instance.step1.get('wq_type') ) or {}
    if fib_conf:

        for aux in instance.step6['auxfilesset']:
            if aux['name'] == fib_conf['flag_aux_name']:
                fib_flag = _fib_flag_map[ aux['value'] ]


        def handle_units_of_(forcings):
            for kind, series in forcings.items():
                if kind in ('ecoli', 'enterococcus'):

                    if series['type'] in ('annual', ):
                        series['value'] = ( float(series['value']) *
                                               fib_conf.get('unit_factor', 1.) )

        for boundary in boundaries.values():
            handle_units_of_(boundary['forcings'])

        for source in fib_sources:
            handle_units_of_(source['fmodel'])

    try:
        wwminput_nml = instance.step5['wwm']['params']
    except KeyError:
        wwminput_nml = dict()

    config = dict(
        run_type = run_type,
        period_hours = instance.model_version_period.run_period,
        # timestep_seconds = float(param_in.get('dt')),

        bbox = ( (x_min_, y_min_), (x_max_, y_max_) ),
        boundaries = boundaries,
        boundaries_order = tuple( boundaries.keys() ),
        boundaries_geojson = boundaries_geojson,
        vertical_levels = vertical_levels_dict() if vgrid_info else dict(),

        ocean_elev_forcing = instance.step3.get('oceanmodel'),
        ocean_salt_temp_forcing = instance.step3.get('oceanmodel_temp_salt'),
        ocean_waves_forcing = instance.step3.get('oceanmodel_waves'),
        atmospheric_forcing = instance.step3.get('atmmodel'),

        fib_flag = fib_flag,
        fib_sources = fib_sources,
        fib_kind = fib_conf.get('kind'),

        param_in = param_in,
        wwminput_nml = wwminput_nml,
    )

    updating_fields = dict(
        config = config,
    )
    Deployment.objects.update_or_create(parent=instance,
                                                       defaults=updating_fields)
