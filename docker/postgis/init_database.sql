CREATE USER oc_user
    PASSWORD 'password';

CREATE DATABASE opencoasts
    OWNER oc_user
    TEMPLATE template_postgis;

GRANT ALL PRIVILEGES ON DATABASE opencoasts
    TO oc_user;