# OPENCoastS setup
Instructions to setup the OPENCoastS web-based user interface.  
This document targets Linux operating systems but most (if not all) stuff should
work on other OSes as well (sometimes requiring small adjustments).

> **Installation** and **Configuration** instructions only need to be performed
> once, while **Update** ones should be performed on every update.


## Installation
The next steps provide instructions on how to install the web portal.
This approach may be preferable for development purposes.  
Alternatively, it is also possible to deploy using a [Docker](docker/README.md)
container, which provides a ready-to-use solution, very convenient for
testing/production use cases.

> Instructions to install **PostgreSQL** (plus the PostGIS extension) database
server and **ncWMS** map server are not included here.  
> Dockerfiles ([PostGIS](Dockerfile-postgis) and [ncWMS](Dockerfile-ncwms))
are available to deploy them.

**Python 3.6 is the recommended version but newer ones should also work.**

The python components should be installed in a virtual environment:

    python3 -m venv <env_dir>
    source <env_dir>/bin/activate

Clone this repository:

    git clone https://gitlab.com/opencoasts/eosc-hub/webportal.git

Upgrade PIP and install requirements:

    python -m pip install --upgrade pip
    python -m pip install --requirements webportal/pip_requirements.txt


## Configuration
Before configuring the web portal, the database needs to be created.
[This snippet](init_database.sql) of SQL may be used to do that.

> The PostgreSQL database server **must** have the PostGIS extension installed.

After creating the database, the following commands must be executed
(inside the repository folder, `cd path/to/webportal`):

Create database structures (tables, indexes, etc...):

    python -m manage migrate

Load database required content:

    python -m manage loaddata datamodel front

Create an administrator account:

    python -m manage createsuperuser

Change settings via setup tool:

    python -m ProjOpenCoastS.setup --help

> This tool produces a new `settings_local.py` from `settings_local.py.template`
> (in `ProjOpenCoastS/`) each time it is executed.
> Which means, manual editions should be made in `settings_local.py.template`,
> otherwise they will be overwritten the next time it is executed.
> The setup tool reads its initial values from `settings_local.json` and stores
> its state in `setup.json`.

## Update

> TODO