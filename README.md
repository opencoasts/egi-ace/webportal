# OPENCoastS web portal
This repository contains the service's web-based user interface,
a [Django](https://www.djangoproject.com) web application.

**Warning**: the django version used here is the 1.11, already unsupported,
a newer and supported version will replace it as soon as possible.

[Instructions to setup](SETUP.md) this package.

After setting up, the web portal may be served using the built-in web server
(should only be used for development purposes):

    python -m manage runserver

Or using [gunicorn](https://gunicorn.org), a production grade web server:

    gunicorn --config=python:gunicorn_config ProjOpenCoastS.wsgi
