import lzma
from datetime import datetime, date, timedelta

from django.db.models import (
    Model as dbModel, ForeignKey, IntegerField, BigIntegerField, CharField,
    SlugField, TextField, BooleanField, DateTimeField, DateField, DurationField,
    BinaryField, FilePathField,
)
from django.contrib.postgres.fields import JSONField
from django.conf import settings

from . import specs


class Name(dbModel):
    """
    Abstract Table - Naming info
    """
    name = CharField(max_length=100)
    reference = SlugField(unique=True, max_length=100)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Info(dbModel):
    """
    Abstract Table - Description info
    """
    description = TextField(blank=True)
    notes = TextField(blank=True)

    class Meta:
        abstract = True


class FileBlobTrait(dbModel):
    """
    Abstract Table - File data
    """
    _file_blob = BinaryField(editable=False)
    _file_size = BigIntegerField(editable=False)
    #file_size = BigIntegerField(editable=False)
    _file_compressed = BooleanField(editable=False, default=False)
    #is_file_compressed = BooleanField(editable=False, default=False)
    _file_compressed_size = IntegerField(editable=False, null=True)

    def __init__(self, *args, file_blob=None, file_compression=True, **kwargs):
    #def __init__(self, *args, file_blob, file_compression=True, **kwargs):

        if file_blob:
            _file_blob = lzma.compress(file_blob) if file_compression \
                                                                  else file_blob
            kwargs.update(
                _file_blob = _file_blob,
                _file_size = len(file_blob),
                _file_compressed = file_compression,
                _file_compressed_size = len(_file_blob) if file_compression
                                                                      else None,
            )

        super().__init__(*args, **kwargs)

    @property
    def file_blob(self):
        if self.file_compressed:
            return lzma.decompress(self._file_blob)
        return self._file_blob

    @file_blob.setter
    def file_blob(self, blob):
        self._file_blob = blob
        self._file_size = len(blob)
        self._file_compressed = False

    @property
    def file_compressed(self):
        return self._file_compressed

    @property
    def file_compressed_size(self):
        return self._file_compressed_size

    def file_blob_compressed(self, blob):
        self._file_size = len(blob)
        self._file_blob = lzma.compress(blob)
        self._file_compressed = True
        self._file_compressed_size = len(self._file_blob)

    class Meta:
        abstract = True


class ModelVersion(dbModel):
    """
    Admin Table - references versions of a Forecast Model Type
    """

    V5_4_0 = '5_4_0'
    V5_6_0 = '5_6_0'

    _CHOICES = (
        (V5_4_0, 'v5.4.0'),
        (V5_6_0, 'v5.6.0'),
    )

    # active = BooleanField(default=True)

    version = CharField(unique=True, max_length=30, choices=_CHOICES)

    def __str__(self):
        # return '{0.get_version_display()} (active={0.active})'.format(self)
        return self.get_version_display()


class ModelKind(dbModel):
    """
    Admin Table - references kinds of a Forecast Model
    """

    # BASIC = 'basic'
    # WAVES = 'waves'
    # _CHOICES = (
    #     (BASIC, 'Basic'),
    #     (WAVES, 'Waves & Currents'),
    # )

    BASIC_2D = 'basic2d'
    BASIC_3D = 'basic3d'
    WAVES_2D = 'waves2d'
    WAVES_3D = 'waves3d'

    _CHOICES = (
        (BASIC_2D, 'Basic 2D (only surface)'),
        (BASIC_3D, 'Basic 3D (with depth)'),
        (WAVES_2D, 'Waves & Currents 2D (only surface)'),
        (WAVES_3D, 'Waves & Currents 3D (with depth)'),
    )

    # active = BooleanField(default=False)

    kind = CharField(unique=True, max_length=20, choices=_CHOICES)
    # has_depth = BooleanField()

    def __str__(self):
        # return '{0.get_kind_display()} depth={0.has_depth} (active={0.active})' \
        #                                                            .format(self)
        return self.get_kind_display()


class ModelRunPeriod(dbModel):
    """
    Admin Table - references run periods of a Forecast Model
    """

    P48H = timedelta(hours=48)
    P72H = timedelta(hours=72)

    _CHOICES = (
        (P48H, '48 hours'),
        (P72H, '72 hours'),
    )

    # active = BooleanField(default=False)

    run_period = DurationField(unique=True, choices=_CHOICES)

    def __str__(self):
        # return '{0.get_run_period_display()} (active={0.active})'.format(self)
        return self.get_run_period_display()


class ModelForcing(dbModel):

    ATM_NOAA_GFS = 'atm_noaa_gfs'
    ATM_METEOFR_ARPEGE = 'atm_meteofr_arpege'

    OCEAN_PRISM_2017 = 'ocean_prism_2017'
    OCEAN_FES_2014 = 'ocean_fes_2014'
    OCEAN_CMEMS_GLOBAL = 'ocean_cmems_global'
    OCEAN_CMEMS_IBI = 'ocean_cmems_ibi'

    RIVER = 'river'

    _CHOICES = (
        (ATM_NOAA_GFS, "Atmospheric: NOAA GFS"),
        (ATM_METEOFR_ARPEGE, "Atmospheric: MetéoFrance ARPEGE"),

        (OCEAN_PRISM_2017, "Ocean: PRISM 2017"),
        (OCEAN_FES_2014, "Ocean: FES 2014"),
        (OCEAN_CMEMS_GLOBAL, "Ocean: CMEMS Global"),
        (OCEAN_CMEMS_IBI, "Ocean: CMEMS Iberia Biscay Irish"),

        (RIVER, "River"),
    )

    forcing = CharField(unique=True, max_length=30, choices=_CHOICES)

    def __str__(self):
        return self.get_forcing_display()


class ModelParameterization(dbModel):

    PARAM_IN = 'param.in'
    WWMINPUT_NML = 'wwminput.nml'

    _CHOICES = (
        (PARAM_IN, "param.in"),
        (WWMINPUT_NML, "wwminput.nml"),
    )

    parameterization = CharField(max_length=30, choices=_CHOICES)
    template = TextField()

    def __str__(self):
        return self.get_parameterization_display()


class ModelInput(dbModel):

    MANNING = 'manning'

    _CHOICES = (
        (MANNING, "Manning"),
    )

    inputt_ = CharField(max_length=30, choices=_CHOICES)

    def __str__(self):
        return self.get_inputt_display()


class ModelOutput(dbModel):

    ELEVATION = 'elevation'

    _CHOICES = (
        (ELEVATION, "Elevation"),
    )

    output = CharField(max_length=30, choices=_CHOICES)

    def __str__(self):
        return self.get_output_display()


class ModelVersionKind(dbModel):

    version = ForeignKey(ModelVersion)
    kind = ForeignKey(ModelKind)

    def __str__(self):
        return '{0.version}, {0.kind}'.format(self)


class ModelKindRunPeriod(dbModel):

    kind = ForeignKey(ModelKind)
    run_period = ForeignKey(ModelRunPeriod)

    def __str__(self):
        return '{0.kind}, {0.run_period}'.format(self)


class ModelKindInput(dbModel):

    kind = ForeignKey(ModelKind)
    inputt = ForeignKey(ModelInput)

    def __str__(self):
        return '{0.kind}, {0.inputt}'.format(self)


class ModelKindOutput(dbModel):

    kind = ForeignKey(ModelKind)
    output = ForeignKey(ModelOutput)

    def __str__(self):
        return '{0.kind}, {0.output}'.format(self)


class ModelVersionKindParameterization(dbModel):

    version_kind = ForeignKey(ModelVersionKind)
    parameterization = ForeignKey(ModelParameterization)

    def __str__(self):
        return '{0.version_kind}; {0.parameterization}'.format(self)


class ModelKindRunPeriodForcing(dbModel):

    kind_run_period = ForeignKey(ModelKindRunPeriod)
    forcing = ForeignKey(ModelForcing)

    def __str__(self):
        return '{0.kind_run_period}; {0.forcing}'.format(self)


class ModelParameter(Name, Info):
    pass


class Deployment(Name, Info):
    """
    Table - information regarding Deployments
    """

    creation_datetime = DateTimeField(default=datetime.now)

    active = BooleanField(default=False)
    deleted = BooleanField(default=False)

    priority = IntegerField(blank=True, default=0)

    begin_run_date = DateField(blank=True, null=True)
    end_run_date = DateField(blank=True, null=True)

    last_run_date = DateField(blank=True, null=True)

    begin_date = DateField(blank=True, null=True)
    end_date = DateField(blank=True, null=True)

    model_version = ForeignKey(ModelVersion)
    model_kind = ForeignKey(ModelKind)
    model_run_period = ForeignKey(ModelRunPeriod)

    def __str__(self):
        return 'ID:{0.id} {0.name} [{0.model_version}; {0.model_kind}; ' \
                                            '{0.model_run_period}]'.format(self)
        # return f'ID:{self.id} {self.name}'


class DeploymentFile(FileBlobTrait):
    """
    Table - Binary Data and respective FileTypes connected to Deployments
    """
    class Kind(specs.ModelInput):

        HGRID_LOCAL = 'hgrid_local'
        HGRID = 'hgrid'
        VGRID = 'vgrid'

        _CHOICES = ModelInput._CHOICES + (
            (HGRID_LOCAL, 'hgrid (local coordinates)'),
            (HGRID, 'hgrid'),
            (VGRID, 'vgrid'),
        )

    deployment = ForeignKey(Deployment, related_name='files')
    kind = CharField(max_length=30, choices=Kind._CHOICES)

    def __str__(self):
        return '{0.deployment}: {0.kind} ({0.file_size_mb}MB)'.format(self)
        # return f'{self.deployment}: {self.file_type} ({self.file_size_mb}MB)'


class DeploymentParameter(dbModel):
    """
    Table - connects Parameters to Deployments with a specific value
    """
    deployment = ForeignKey(Deployment)
    parameter = ForeignKey(ModelParameter)

    value = TextField(blank=True)
    notes = TextField(blank=True)

    def __str__(self):
        return '{0.deployment}: {0.parameter} = {0.value}'.format(self)
        # return f'{self.deployment}: {self.parameter} = {self.value}'


class DeploymentForcing(dbModel):
    """
    Table - connects Forcings to Deployments
    """
    deployment = ForeignKey(Deployment)
    forcing = ForeignKey(ModelForcing)
    priority = IntegerField(blank=True, default=0)

    def __str__(self):
        return '{0.deployment} forced by {0.forcing} #{0.priority}'.format(self)
        # return f'{self.deployment} forced by {self.forcing} #{self.priority}'


class DeploymentOutputFilepath(dbModel):

    deployment = ForeignKey(Deployment)
    date = DateField(default=date.today)
    filepath = FilePathField(
        path=settings.DEPLOYMENTS_PATH,
        match=settings.OUTPUT_FILES_MATCH,
        recursive=True,
        max_length=254,
    )

    def __str__(self):
        return '{0.deployment} on {0.date}: {0.filepath}'.format(self)
        #return f'{self.deployment} on {self.date}: {self.filepath}'


class DeploymentRun(dbModel):

    class State:

        PENDING = 'pending'
        STARTED = 'started'
        FINISHED = 'finished'
        FAILED = 'failed'

        _CHOICES = (
            (PENDING, 'Pending'),
            (STARTED, 'Started'),
            (FINISHED, 'Finished'),
            (FAILED, 'Failed'),
        )

    deployment = ForeignKey(Deployment)
    run_datetime = DateTimeField()
    state = CharField(choices=State._CHOICES, max_length=8)
    begin = DateTimeField(null=True, blank=True)
    end = DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{0.deployment} on {0.run_datetime}: {1}'.format( self,
                                                      self.get_state_display() )
        # return f'{self.deployment} on {self.run_datetime}: ' \
        #                                                 self.get_state_display()


class DeploymentRunMessage(dbModel):

    class Kind:

        ERROR = 'error'
        TIMINGS = 'timings'

        _CHOICES = (
            (ERROR, 'Error'),
            (TIMINGS, 'Timings'),
        )

    deployment_run = ForeignKey(DeploymentRun)
    kind = CharField(choices=Kind._CHOICES, max_length=8)
    message = TextField(blank=True)

    def __str__(self):
        return '{0.deployment_run}; {0.kind}: {0.message}'.format(self)
        # return f'{self.deployment_run}; {self.kind}: {self.message}'


class DeploymentRunOutputFilepath(dbModel):

    class Kind:

        RESULTS = 'results'
        LOGS = 'logs'

        _CHOICES = (
            (RESULTS, 'Results'),
            (LOGS, 'Log(s)'),
        )

    deployment_run = ForeignKey(DeploymentRun)
    kind = CharField(choices=Kind._CHOICES, max_length=8)
    filepath = FilePathField(
        path=settings.DEPLOYMENTS_ROOT_PATH,
        match=r'^{}$'.format(settings.OUTPUT_FILES_MATCH),
        recursive=True,
        max_length=254,
    )

    def __str__(self):
        return '{0.deployment_run}; {0.kind}: {0.filepath}'.format(self)
        # return f'{self.deployment_run}; {self.kind}: {self.filepath}'
