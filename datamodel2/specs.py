from datetime import timedelta

from .models import *

def filter_single_map(rule_map, rule_key):
    return { key: values[rule_key] for key, values in rule_map.items() }

def filter_double_map(rule_map, rule_key):
    return { (fst_key, snd_key): values[rule_key]
        for fst_key, snd_map in rule_map.items()
            for snd_key, values in snd_map.items()
    }

class ModelVersion:

    V5_4_0 = '5_4_0'
    V5_6_0 = '5_6_0'


class ModelKind:

    BASIC_2D = 'basic2d'
    BASIC_3D = 'basic3d'
    WAVES_2D = 'waves2d'
    WAVES_3D = 'waves3d'


class ModelRunPeriod:

    P48H = timedelta(hours=48)
    P72H = timedelta(hours=72)


class ModelForcing:

    class Atmospheric:

        NOAA_GFS = 'atm_noaa_gfs'
        METEOFR_ARPEGE = 'atm_meteofr_arpege'

    class Ocean:

        PRISM_2017 = 'ocean_prism_2017'
        FES_2014 = 'ocean_fes_2014'
        CMEMS_GLOBAL = 'ocean_cmems_global'
        CMEMS_IBI = 'ocean_cmems_ibi'


class ModelParameterization:

    PARAM_IN = 'param.in'
    WWMINPUT_NML = 'wwminput.nml'


class ModelInput:

    ALBEDO = 'albedo'
    DIFFMIN = 'diffmin'
    DIFFMAX = 'diffmax'
    DRAG = 'drag'
    HGRID = 'hgrid'
    MANNING = 'manning'
    SALINITY = 'salinity'
    TEMPERATURE = 'temperature'
    VGRID = 'vgrid'
    WATERTYPE = 'watertype',
    WINDROT = 'windrot',


class ModelOutput:

    ELEVATION = 'elevation'
    VELOCITY = 'velocity'
    SALINITY = 'salinity'
    TEMPERATURE = 'temperature'


ModelVersion_rule_map = {
    ModelVersion.V5_4_0: dict(
        ModelKind = (
            ModelKind.BASIC_2D,
            ModelKind.BASIC_3D,
        ),
    ),

    ModelVersion.V5_6_0: dict(
        ModelKind = (
            ModelKind.BASIC_2D,
            ModelKind.BASIC_3D,
        ),
    ),
}

class ModelVersionKind:

    rule_map = filter_single_map(ModelVersion_rule_map, 'ModelKind')


ModelKind_rule_map = {
    ModelKind.BASIC_2D: dict(
        ModelRunPeriod = (
            ModelRunPeriod.P48H,
        ),
        ModelInput = (
            ModelInput.MANNING,
        ),
        ModelOutput = (
            ModelOutput.ELEVATION,
            ModelOutput.VELOCITY
        ),
    ),

    ModelKind.BASIC_3D: dict(
        ModelRunPeriod = (
            ModelRunPeriod.P48H,
        ),
        ModelInput = (
            ModelInput.ALBEDO,
            ModelInput.DIFFMIN,
            ModelInput.DIFFMAX,
            ModelInput.DRAG,
            ModelInput.HGRID,
            ModelInput.SALINITY,
            ModelInput.TEMPERATURE,
            ModelInput.VGRID,
            ModelInput.WATERTYPE,
            ModelInput.WINDROT
        ),
        ModelOutput = (
            ModelOutput.ELEVATION,
            ModelOutput.VELOCITY,
            ModelOutput.SALINITY,
            ModelOutput.TEMPERATURE
        ),
    )
}

class ModelKindRunPeriod:

    rule_map = filter_single_map(ModelKind_rule_map, 'ModelRunPeriod')


class ModelKindInput:

    rule_map = filter_single_map(ModelKind_rule_map, 'ModelInput')


class ModelKindOutput:

    rule_map = filter_single_map(ModelKind_rule_map, 'ModelOutput')

_BASIC_PARAMETERIZATION = (
    ModelParameterization.PARAM_IN,
),
_WAVES_PARAMETERIZATION = (
    ModelParameterization.PARAM_IN,
    ModelParameterization.WWMINPUT_NML,
),

ModelVersionKind_rule_map = {
    ModelVersion.V5_4_0: {
        ModelKind.BASIC_2D: dict(
            ModelParameterization = _BASIC_PARAMETERIZATION,
        ),
        ModelKind.BASIC_3D: dict(
            ModelParameterization = _BASIC_PARAMETERIZATION,
        ),
        ModelKind.WAVES_2D: dict(
            ModelParameterization = _WAVES_PARAMETERIZATION,
        ),
        ModelKind.WAVES_3D: dict(
            ModelParameterization = _WAVES_PARAMETERIZATION,
        ),
    },

    # ModelVersion.V5_6_0: {

    # },
}

class ModelVersionKindParameterization:

    rule_map = filter_double_map(ModelVersionKind_rule_map,
                                                        'ModelParameterization')

_COMMON_ATMOSPHERIC_FORCINGS = (
    ModelForcing.Atmospheric.NOAA_GFS,
    ModelForcing.Atmospheric.METEOFR_ARPEGE,
)
_CMEMS_OCEAN_FORCINGS = (
    ModelForcing.Ocean.CMEMS_GLOBAL,
    ModelForcing.Ocean.CMEMS_IBI,
)

ModelKindRunPeriod_rule_map = {
    ModelKind.BASIC_2D: {
        ModelRunPeriod.P48H: dict(
            ModelForcing = (
                *_COMMON_ATMOSPHERIC_FORCINGS,

                *_CMEMS_OCEAN_FORCINGS,
                ModelForcing.Ocean.PRISM_2017,
                ModelForcing.Ocean.FES_2014,
            ),
        ),
        ModelRunPeriod.P72H: dict(
            ModelForcing = (
                *_COMMON_ATMOSPHERIC_FORCINGS,

                *_CMEMS_OCEAN_FORCINGS,
                ModelForcing.Ocean.FES_2014,
            ),
        ),
    },

    ModelKind.BASIC_3D: {
        ModelRunPeriod.P48H: dict(
            ModelForcing = (
                *_COMMON_ATMOSPHERIC_FORCINGS,

                *_CMEMS_OCEAN_FORCINGS
            ),
        ),
        ModelRunPeriod.P72H: dict(
            ModelForcing = (
                *_COMMON_ATMOSPHERIC_FORCINGS,

                *_CMEMS_OCEAN_FORCINGS
            ),
        ),
    },
}

class ModelKindRunPeriodForcing:

    rule_map = filter_double_map(ModelKindRunPeriod_rule_map, 'ModelForcing')
