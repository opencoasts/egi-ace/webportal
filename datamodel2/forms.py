from django.forms import ModelForm, ValidationError

from . import specs


class ModelVersionKindAdminForm(ModelForm):

    def clean(self):
        version = self.cleaned_data.get('version').version
        kind = self.cleaned_data.get('kind').kind

        version_kinds = specs.ModelVersionKind.rule_map.get(version)
        if not (version_kinds and kind in version_kinds):
            raise ValidationError( 'Kind {} not available in version {}'.
                                                         format(kind, version) )

        super().clean()


class ModelKindRunPeriodAdminForm(ModelForm):

    def clean(self):
        kind = self.cleaned_data.get('kind').kind
        run_period = self.cleaned_data.get('run_period').run_period

        kind_run_periods = specs.ModelKindRunPeriod.rule_map.get(kind)
        if not (kind_run_periods and run_period in kind_run_periods):
            raise ValidationError( 'Run period {} not available in kind {}'.
                                                      format(run_period, kind) )

        super().clean()


class ModelKindInputAdminForm(ModelForm):

    def clean(self):
        kind = self.cleaned_data.get('kind').kind
        inputt = self.cleaned_data.get('inputt').inputt

        kind_run_inputts = specs.ModelKindInput.rule_map.get(kind)
        if not (kind_run_inputts and inputt in kind_run_inputts):
            raise ValidationError( 'Input {} not available in kind {}'.
                                                           format(input, kind) )

        super().clean()


class ModelKindOutputAdminForm(ModelForm):

    def clean(self):
        kind = self.cleaned_data.get('kind').kind
        output = self.cleaned_data.get('output').output

        kind_run_outputs = specs.ModelKindRunOutput.rule_map.get(kind)
        if not (kind_run_outputs and output in kind_run_outputs):
            raise ValidationError( 'Ouput {} not available in kind {}'.
                                                      format(output, kind) )

        super().clean()