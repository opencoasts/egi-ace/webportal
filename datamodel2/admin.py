from django.contrib.admin import ModelAdmin, register

from .models import *
from .forms import *

@register(ModelVersion)
class ModelVersionAdmin(ModelAdmin):
    ordering = ('version', )


@register(ModelKind)
class ModelKindAdmin(ModelAdmin):
    ordering = ('kind', )


@register(ModelRunPeriod)
class ModelRunPeriodAdmin(ModelAdmin):
    ordering = ('run_period', )


@register(ModelForcing)
class ModelForcingAdmin(ModelAdmin):
    ordering = ('forcing', )


@register(ModelParameterization)
class ModelParameterizationAdmin(ModelAdmin):
    ordering = ('parameterization', )


@register(ModelInput)
class ModelInputAdmin(ModelAdmin):
    ordering = ('inputt', )


@register(ModelOutput)
class ModelOutputAdmin(ModelAdmin):
    ordering = ('output', )


@register(ModelVersionKind)
class ModelVersionKindAdmin(ModelAdmin):
    ordering = ('version', 'kind')
    form = ModelVersionKindAdminForm


@register(ModelKindRunPeriod)
class ModelKindRunPeriodAdmin(ModelAdmin):
    ordering = ('kind', 'run_period')


@register(ModelKindInput)
class ModelKindInputAdmin(ModelAdmin):
    ordering = ('kind', 'inputt')


@register(ModelKindOutput)
class ModelKindOutputAdmin(ModelAdmin):
    ordering = ('kind', 'output')


@register(ModelVersionKindParameterization)
class ModelVersionKindParameterizationAdmin(ModelAdmin):
    ordering = ('version_kind', 'parameterization')


@register(ModelKindRunPeriodForcing)
class ModelKindRunPeriodForcingAdmin(ModelAdmin):
    ordering = ('kind_run_period', 'forcing')


@register(Deployment)
class ModelDeploymentAdmin(ModelAdmin):
    ordering = ('-end_date', 'begin_date')


@register(DeploymentFile)
class ModelDeploymentFileAdmin(ModelAdmin):
    list_display = ('deployment', 'kind', 'file_size', 'file_compressed',
                      'file_compressed_size', 'file_compressed_size_percentage')
    ordering = ('deployment', 'kind')

    def has_add_permission(self, request, obj=None):
        return False

    @staticmethod
    def size_in_megabytes(size_in_bytes):
        return size_in_bytes / 1024 / 1024

    def file_size(self, record):
        return '{:.1f} MB'.format( self.size_in_megabytes(record.file_size) )

    def file_compressed_size(self, record):
        if record.file_compressed:
            return '{:.1f} MB'.format(
                           self.size_in_megabytes(record.file_compressed_size) )
        return ''

    def file_compressed_size_percentage(self, record):
        if record.file_compressed:
            percentage = int( (record.file_compressed_size / record.file_size)
                                                                         * 100 )
            return '{}%'.format(percentage)
        return ''
