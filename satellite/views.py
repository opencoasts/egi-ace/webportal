from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

import raster.models as raster_models
from .models import SatelliteRaster

logger = settings.LOGGER.getChild('viewer')


@login_required(login_url='/login/')
def get_rasters(request, deploy_id):

    rasters_deploy = []

    try:

        rasters = raster_models.RasterLayer.objects.all()
        satellite_rasters = SatelliteRaster.objects.all()
        n_rasters = rasters.count()
        n_satellite_rasters = satellite_rasters.count()

        for i in range(0, n_rasters):
            for j in range(0, n_satellite_rasters):
                if(satellite_rasters[j].raster_name == rasters[i].name and satellite_rasters[j].raster_deployment.id == int(deploy_id)):
                    raster = {
                        "deploy_id": satellite_rasters[j].raster_deployment.id,
                        "properties": {
                            "id": rasters[i].id,
                            "name": rasters[i].name,
                            "date": satellite_rasters[j].raster_date.strftime('%d/%m/%Y'),
                        }
				    }

                    rasters_deploy.append(raster)

    except Exception:
        logger.exception('Error getting deployments sattelite images')
        rasters_deploy = None
        pass

    return JsonResponse({'rasters': rasters_deploy})
