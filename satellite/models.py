from django.db.models import Model, ForeignKey, OneToOneField, CASCADE, \
                                                 CharField, TextField, DateField

import raster.models
from front.models import Deployment


class SatelliteRaster(Model):

    raster = OneToOneField(raster.models.RasterLayer, on_delete=CASCADE, blank=True, null=True)
    raster_name = TextField(blank=True)
    raster_date = DateField(blank=False)
    raster_observatory = CharField(max_length = 100, null=True, blank=True)
    raster_threshold = CharField(max_length = 200, null=True, blank=True)
    #raster_image = ImageField(upload_to="viewer/satellites", null=True, max_length = 10000)
    raster_deployment = ForeignKey(Deployment, on_delete=CASCADE, null=True, blank=True)

    def __str__(self):
        return self.raster_name

    @classmethod
    def setThreshold(cls, threshold):
        cls.raster_threshold = threshold
