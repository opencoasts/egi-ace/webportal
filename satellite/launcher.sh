#!/usr/bin/env bash

DJANGO_SETTINGS_MODULE='ProjOpenCoastS.settings' \
    python -m satellite.utilities.Coast_Line \
        --source $PWD \
        --base $PWD/satellite/utilities \
        --path $PWD/satellite/images
