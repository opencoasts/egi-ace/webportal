#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#========================================================================
# File:   worsica_ImgDownloads.py
#========================================================================
__author__ = 'Alberto Azevedo'
__doc__ = "Script to search and download Sentinel 1 and 2 satellite data"
__datetime__ = '& March 2020 &'
__email__ = 'aazevedo@lnec.pt'
#========================================================================
"""
###################################################################################
###################################################################################
  This script uses the Sentinelsat package.
  To download and install this package check the following link:
        https://pypi.python.org/pypi/sentinelsat
  Example of the "Polygon.txt" ASCII file.
  This file is used to define the Region of Interest (ROI)
  List of points (lon,lat)
  Polygon.txt
    -9.0 40.0
    -9.0 41.0
    -8.0 41.0
    -8.0 40.0
Examples of usage:
- Get all the images from sentinel-2, from 2017/11/01 until present, for the ROI.
./worsica_ImgDownloads.py -i 20171101 -f NOW -s 2 -p Polygon.txt
- Get all the images from sentinel-2, from 2017/11/01 until present, for the ROI, with more information for each image.
./worsica_ImgDownloads.py -i 20171101 -f NOW -s 2 -p Polygon.txt -v
- Get all the images from sentinel-2, from 2017/11/01 until present, for the ROI and download of all the products.
./worsica_ImgDownloads.py -i 20171101 -f NOW -s 2 -p Polygon.txt -d
- Get all the images from sentinel-2, from 2017/11/01 until present, for the ROI, with more information for each image and download of all the products with a maximum cloud coverage of 30%.
./worsica_ImgDownloads.py -i 20171101 -f NOW -s 2 -p Polygon.txt -c 30 -v -d
- Get all the images from sentinel-2, from 2016/12/01 until 2017/01/01, for the ROI, with more information for each image and download of all the products with a maximum cloud coverage of 10%.
./worsica_ImgDownloads.py -i 20161201 -f 20170101 -s 2 -p Polygon.txt -c 10 -v -d
###################################################################################
###################################################################################
"""
import os
import sys
import argparse
import numpy as np
from datetime import date, datetime
from multiprocessing import Pool, cpu_count
from sentinelsat.sentinel import SentinelAPI, read_geojson, geojson_to_wkt
import gdal
from pathlib import Path
from glob import glob
import shutil

#source_dir = '/srv/www/mosaic/web-portal'
#source_dir = '/home/morocha/OPENCoastS'

source_dir = ''
base_dir = ''

def worsicaRGB(imgZip):
    os.system(f"unzip -u {imgZip}")
    fileLista = []
    for root, dirs, files in os.walk(f"{imgZip[:-4]}.SAFE", topdown=True):
        for name in files:
            fileLista.append(os.path.join(root, name))
        for name in dirs:
            fileLista.append(os.path.join(root, name))
    l10JP2 = [i for i in fileLista if (i.endswith("B02_10m.jp2") or i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2"))]
    # l10JP2 = [i for i in fileLista if (i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
    l1020 = l10JP2
    BandsRGB = {}
    pathAux = Path(".") / 'auxRGB'
    pathAux.mkdir(exist_ok=True)
    foutFiles = []
    for img in l1020:
        fout = "".join((str(pathAux), "/", img.split("SAFE")[0][:-1], img.split("/")[-1][-12:-4], ".tif"))
        fout2 = "./" + "_".join((img.split("SAFE")[0][:-1], "RGB.tif"))
        ds = gdal.Open(img)
        a = ds.GetRasterBand(1)
        Metadata = ds.GetMetadata()
        ncols, nrows = a.XSize, a.YSize
        GCPs = ds.GetGCPs()
        GCPsProj = ds.GetGCPProjection()
        geotransform = ds.GetGeoTransform()
        Description = ds.GetDescription()
        gdal.Translate(fout, ds, format="GTiff", resampleAlg='cubic', xRes=10., yRes=10., outputType=gdal.GDT_UInt16)
        ds = None
        if "B02_10m" in fout:
            BandsRGB["B"] = fout
        elif "B03_10m" in fout:
            BandsRGB["G"] = fout
        elif "B04_10m" in fout:
            BandsRGB["R"] = fout
    vrt_options = gdal.BuildVRTOptions(separate=True, hideNodata=True)
    gdal.BuildVRT('./auxRGB/aux.vrt', [BandsRGB['R'], BandsRGB['G'], BandsRGB['B']], options=vrt_options)
    ds2 = gdal.Open('./auxRGB/aux.vrt')
    ds2 = gdal.Translate(fout2, ds2)
    ds2 = None
    # dsOut = gdal.Open(fout2)
    # dsOut.SetMetadata(Metadata)
    # dsOut.SetDescription(Description)
    # dsOut.GetRasterBand(1).SetDescription('B2')
    # dsOut.GetRasterBand(2).SetDescription('B3')
    # dsOut.GetRasterBand(3).SetDescription('B4')
    # dsOut = None
    fDelete = imgZip.replace(".zip", ".SAFE")
    shutil.rmtree(pathAux)
    shutil.rmtree(f"{fDelete}")
    print(f"{fout2} :: Done!!!")
    return None
wdir = os.getcwd()
paths = ['../../.', wdir]
for i in paths:
    sys.path.append(i)
def getCredentials():
    from .SSecrets import Credentials
    nCredentials = len(Credentials.keys())
    Users = {}
    for n in range(1, nCredentials + 1):
        Users[n] = Credentials[f"user{n}"]
    return Users
def instAPI(Users):
    api = []
    for i in Users.keys():
        api.append(SentinelAPI(Users[i]["user"], Users[i]["password"], 'https://scihub.copernicus.eu/dhus'))
    return api
# Get ESA credentials and instanciate api
Users = getCredentials()
nAccounts = len(Users.keys())
PossibleKeys = np.arange(1, nAccounts + 1)
api = instAPI(Users)
###########################################################################################################
###########################################################################################################
###########################################################################################################
def yes_or_no(question):
    reply = str(raw_input(question)).lower().strip()
    if (reply[0] == 'y' or reply[0] == 'Y'):
        return True
    if (reply[0] == 'n' or reply[0] == 'N'):
        return False
    else:
        yes_or_no(question)
def convert_size_to_MB(size_str):
    # size is string, and can be KB, MB or GB
    size_split = size_str.split(" ")
    size_val = float(size_split[0])
    if (size_split[1] == 'GB'):
        size_val = size_val * 1024
    elif (size_split[1] == 'KB'):
        size_val = size_val / 1024
    return size_val
def write_geojson(filein):
    print(filein)
    points = np.loadtxt(filein)
    poly = []
    fout = open('search_polygon.geojson', "w")
    fout.write("""{ "type": "Polygon",\n      "coordinates": [\n""")
    for i in points:
        poly.append([i[0], i[1]])
    poly.append([points[0][0], points[0][1]])
    poly = str(poly)
    fout.write("       " + poly + "\n       ]\n    }")
    fout.close()
    return None
def downloadUUID(uid):
    download = True
    c, maxTry = 0, 1000
    while download:
        try:
            api[uid[0]].download(uid[1])
            download = False
        except:
            c += 1
            if c == maxTry:
                download = False
    return None
def getARGScmd():
    parser = argparse.ArgumentParser(description='worsica_ImgDownloads: Script to search and download data from satellites sentinel 1/2')
    parser.add_argument('-i', '--start', help='Start date of the query in the format YYYYMMDD', default="20200101", required=False)
    parser.add_argument('-f', '--end', help='End date of the query in the format YYYYMMDD', required=False)
    parser.add_argument("-s", "--satellite", help="Sentinel satellite 1 or 2", type=int, default=2, required=False)
    parser.add_argument("-l", "--level", help="Sentinel satellite aquisition Level 1, 2 or 0 (for both levels)", type=int, default=2, required=False)
    parser.add_argument('-p', '--polygon', help='Polygon coords list file', default="Polygon.txt", required=False)
    parser.add_argument("-c", "--cloud", help="Maximum cloud cover in percent. (requires –sentinel to be 2)", type=int, required=False)
    parser.add_argument("-t", "--type", help="Limit search to a Sentinel product type (SLC, GRD, etc.)", required=False)
    parser.add_argument("-q", "--query", help="Extra search keywords you want to use in the query. Separate keywords with comma. Example: ‘producttype=GRD,polarisationmode=HH’", required=False)
    parser.add_argument("-d", "--download", help="Download all results of the query", action="store_true", required=False)
    parser.add_argument("-uuid", help="Download images with uuid. Separate uuid keys with comma. Example: ‘-uuid d572e0a2-e2cb-4b2d-aff7-e6726b035c1b,d44606e8-0d58-4a8b-a38c-eebc531fdc81’", required=False)
    parser.add_argument("-v", "--verbose", help="Show metadata from image", action="store_true", required=False)
    parser.add_argument("-rgb", help="Create the RGB images of the downloaded products.", action="store_true", required=False)
    parser.add_argument('-sou', '--source', help='Source image folder.', required=True)
    parser.add_argument('-b', '--base', help='Base image folder.', required=True)
    args = parser.parse_args()
    return args
def getARGSfile():
    from argsSentinel import args
    return args
def getUUIDS(uuids, nAccounts):
    set0 = [[uuids[i::nAccounts]] for i in range(0, nAccounts)]
    imgsSET = []
    for n, i in enumerate(set0):
        for j in i[0]:
            imgsSET.append([n, j])
    # print(imgsSET)
    pool = Pool(2 * cpu_count())
    results = pool.map_async(downloadUUID, imgsSET)
    pool.close()
    pool.join()
    return None
def checkDownloaded(filesDownloaded, obs):
    obs_split = obs.split("/")
    print(obs_split)
    observatory_txt = obs_split[1]
    observatory = observatory_txt.split(".")
    path_to_save = os.path.join(observatory[0])
    print(path_to_save)
    now = datetime.now()
    strNow = now.strftime("%Y-%m-%dT%H:%M:%S")
    nImgs = len(filesDownloaded)
    Fails = []
    with open("downloadedIMGS.out", "w") as fout:
        fout.write(f"Run {strNow} => {nImgs} images\n")
        for n, file in enumerate(filesDownloaded):
            if os.path.exists(file[0]):
                fout.write(f"{n+1}: {file[0]} => Downloaded!!!\n")
            else:
                fout.write(f"{n+1}: {file[0]} => FAIL !!!\n")
                Fails.append(f"{file[1]}\n")
        if len(Fails) != 0:
            strFails = "".join(Fails)
            fout.write("\n\nID of FAILS:\n")
            fout.write(strFails)
    return None
def worsicaDownloads(args):
    downFile = "downloadedIMGS.out"
    filesDownloaded = []
    if os.path.exists(downFile):
        os.remove(downFile)
    # Check arguments
    if args.uuid != None:
        if "." in args.uuid:
            with open(args.uuid, "r") as fin:
                uuids = fin.readlines()
            uuids = [img.strip() for img in uuids]
        else:
            uuids = [img.strip() for img in args.uuid.split(",")]
        getUUIDS(uuids, nAccounts)
    else:
        # search by polygon, time, and Hub query keywords
        if args.start > args.end:
            sys.exit("ERROR: Start date should be before than the finish date!")
        if args.satellite == 1:
            sat = 'Sentinel-1'
        elif args.satellite == 2:
            sat = 'Sentinel-2'
        else:
            sys.exit("ERROR: Invalid platform name! Check platform argument...")
        write_geojson(args.polygon)
        footprint = geojson_to_wkt(read_geojson('search_polygon.geojson'))
        os.remove('search_polygon.geojson')
        query_kwargs = {'date': (args.start, args.end), 'platformname': sat}
        if args.type != None:
            query_kwargs['producttype'] = args.type
        if args.cloud != None:
            if args.satellite == 2:
                query_kwargs['cloudcoverpercentage'] = (0, args.cloud)
            else:
                print("Warning: Cloud cover argument is not appliable for sentinel-1 products, ignoring it.")
        if args.level == 0:
            pass
        elif args.level == 1:
            query_kwargs['processinglevel'] = 'Level-1C'
        elif args.level == 2:
            query_kwargs['processinglevel'] = 'Level-2A'
        else:
            sys.exit("Error: check the processing level argument.")
        if args.query != None:
            queries = args.query.split(",")
            for item in queries:
                print(item.split("=")[0], item.split("=")[1])
                query_kwargs[item.split("=")[0]] = item.split("=")[1]
        products = api[0].query(footprint, **query_kwargs)
        for Id in products.keys():
            filesDownloaded.append([f"{products[Id]['identifier']}.zip", Id])
        if args.verbose:
            for Id in products.keys():
                print('Product %s ' % (Id))
                for keywords in products[Id]:
                    print('%s = %s' % (keywords, products[Id][keywords]))
                print('########################################################################################################################\n\n\n')
        else:
            N_products = len(products.keys())
            size = 0.  # in MB
            print("\nFound " + str(N_products) + " products\n")
            for Id in products.keys():
                print('%s ' % (Id))
                print('Filename: %s.zip ' % (products[Id]["identifier"]))
                print('Date: %s ' % (products[Id]["beginposition"]))
                print('Satellite: %s ' % (products[Id]["platformname"]))
                print('Instrument: %s ' % (products[Id]["instrumentshortname"]))
                szfileMB = convert_size_to_MB(products[Id]["size"])
                print('Size: %s MB (%s)\n' % (szfileMB, products[Id]["size"]))
                size = size + szfileMB
            print("---")
            print(str(N_products) + " products found with a total size of " + str(size) + " MB ")  # <-- corrected here was GB and should be MB
            print('########################################################################################################################\n\n\n')
        if args.download:
            # download all results from the search
            uuids = list(products.keys())
            getUUIDS(uuids, nAccounts)
            if args.rgb:
                for fileTOrgb in filesDownloaded:
                    worsicaRGB(fileTOrgb[0])
    checkDownloaded(filesDownloaded, args.polygon)
    return None
#############################################################################################################################
#     For more query keywords check the following link: https://scihub.copernicus.eu/userguide/3FullTextSearch
#############################################################################################################################
if __name__ == '__main__':
    args = getARGScmd()

    source_dir = args.source
    base_dir = args.base

    worsicaDownloads(args)

    file_names = os.listdir(source_dir)
    for file_name in file_names:
        if".zip" in file_name or ".tif" in file_name:
            print("DOWNLOAD FICHEIROS LISTA")
            print(file_name)
            shutil.move(os.path.join(source_dir + file_name), source_dir + "satellite/utilities")