# -*- coding: utf-8 -*-

#========================================================================
# File:   worsica_ph1_resample_crop_rgb.py
# This script is based on worsica-resampling.py created by Alberto Azevedo
#
# Main diferences between this one and the original are:
# 1- This script already applies Resampling and RGB, in one step.
# 2- This script already crops the imageset according to WKT the user provides 
#    (and this script automatically converts WKT to boundingbox)
# 3- This script uses a different file structure from the original
#     (but later this needs to be refined)
# -----------------------------------------------------------------------
# Log:
# October 20, 2020: Applied some fixes from the worsica_resampling.py to here
# September 22, 2020: Initial version
#========================================================================
__author__ = 'Ricardo Martins (original author: Alberto Azevedo)'
__doc__ = "Script to resample Sentinel 2 images to 10 m resolution"
__datetime__ = '& September 2020 &'
__email__ = 'rjmartins (original author: aazevedo@lnec.pt)'
#========================================================================

import os
import sys
import gdal
from pathlib import Path
from glob import glob
import numpy as np
import argparse
from multiprocessing import Pool, cpu_count
import shutil
import traceback

from PIL import Image

from . import worsica_common_script_functions

projWinArgs = None

def worsicaResampling(procDict):
    try:
        imgZip = procDict['imgZip']
        print(imgZip)
        projWinArgs = procDict['projWinArgs']

        #ACTUAL_PATH = os.getcwd()
        if('path' in procDict):
            ACTUAL_PATH = procDict['path']
        else:
            ACTUAL_PATH = os.getcwd()

        #print('ACTUAL_PATH')
        print(ACTUAL_PATH)
        #print(imgZip)
        #print(projWinArgs)

        imgZip = ACTUAL_PATH + imgZip

        print("Sou o zip:" + imgZip)

        #print('worsicaResampling')
        os.system(f"unzip -u {imgZip} -d {ACTUAL_PATH}")
        fileLista = []
        for root, dirs, files in os.walk(f"{imgZip[:-4]}.SAFE", topdown=True):
            for name in files:
                fileLista.append(os.path.join(root, name))
            for name in dirs:
                fileLista.append(os.path.join(root, name))

        l10JP2 = [i for i in fileLista if (i.endswith("B02_10m.jp2") or i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
        # l10JP2 = [i for i in fileLista if (i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
        l20JP2 = [i for i in fileLista if i.endswith("B11_20m.jp2") or i.endswith("B12_20m.jp2")]
        l1020 = l10JP2 + l20JP2
        
        BandsRGB = {}
        foutFiles = []
        for img in l1020:
            #fout = "".join((img.split("SAFE")[0][:-1], img.split("/")[-1][-12:-4], ".tif"))
            fout = "".join((ACTUAL_PATH + "auxResample/", img.split("SAFE")[0][:-1].split("/")[-1], img.split("/")[-1][-12:-4], ".tif"))
            #fout2 = "resampled/" + "_".join((img.split("SAFE")[0][:-1], "resampled.tif"))
            fout2 = ACTUAL_PATH + "resampled/" + "_".join((img.split("SAFE")[0][:-1].split("/")[-1], "resampled.tif"))
            print("FOUT2: ", fout2)
            foutFiles.append(fout)

            ds = gdal.Open(img)
            band = ds.GetRasterBand(1)
            arr = band.ReadAsArray()
            [cols, rows] = arr.shape
            # ncols, nrows = a.XSize, a.YSize
            Metadata = ds.GetMetadata()
            GCPs = ds.GetGCPs()
            GCPsProj = ds.GetGCPProjection()
            geotransform = ds.GetGeoTransform()
            Description = ds.GetDescription()

            gdal.Translate(fout, ds, format="GTiff", resampleAlg='cubic', xRes=10., yRes=10., outputType=gdal.GDT_Float32, projWinSRS = projWinArgs['projWinSRS'], projWin = projWinArgs['projWin'])
            ds = None

            if "B02_10m" in fout:
                BandsRGB["B"] = fout
            elif "B03_10m" in fout:
                BandsRGB["G"] = fout
            elif "B04_10m" in fout:
                BandsRGB["R"] = fout
            elif "B08_10m" in fout:
                BandsRGB["NIR"] = fout
            elif "B11_20m" in fout:
                BandsRGB["SWIR1"] = fout
            elif "B12_20m" in fout:
                BandsRGB["SWIR2"] = fout
        
        vrt_options = gdal.BuildVRTOptions(separate=True, hideNodata=True)
        gdal.BuildVRT(ACTUAL_PATH+'auxResample/aux.vrt', [BandsRGB['B'], BandsRGB['G'], BandsRGB['R'], BandsRGB['NIR'], BandsRGB['SWIR1'], BandsRGB['SWIR2']], options=vrt_options)
        ds2 = gdal.Open(ACTUAL_PATH+'auxResample/aux.vrt')
        ds2 = gdal.Translate(fout2, ds2)
        ds2 = None
        
        dsOut = gdal.Open(fout2)
        dsOut.SetMetadata(Metadata)
        dsOut.SetDescription(Description)
        dsOut.GetRasterBand(1).SetDescription('Blue')
        dsOut.GetRasterBand(2).SetDescription('Green')
        dsOut.GetRasterBand(3).SetDescription('Red')
        dsOut.GetRasterBand(4).SetDescription('NIR')
        dsOut.GetRasterBand(5).SetDescription('SWIR1')
        dsOut.GetRasterBand(6).SetDescription('SWIR2')
        dsOut = None
        
        worsicaRGB(fileLista, ACTUAL_PATH)
        
        foutFiles.append(ACTUAL_PATH+"auxResample/aux.vrt")
        for trash in foutFiles:
            os.remove(trash)
        fDelete = imgZip.replace(".zip", ".SAFE")
        shutil.rmtree(f"{fDelete}")
        print(f"{fout2} :: Done!!!")
        return None
    except:
        traceback.print_exc()


def worsicaRGB(fileLista, actual_path):
    try:
        l10JP2 = [i for i in fileLista if (i.endswith("B02_10m.jp2") or i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2"))]
        # l10JP2 = [i for i in fileLista if (i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
        l1020 = l10JP2

        BandsRGB = {}
        pathAux = Path(actual_path) / 'auxResample'
        pathAux.mkdir(exist_ok=True)  
        for img in l1020:
            fout = "".join((str(pathAux),"/", img.split("SAFE")[0][:-1].split("/")[-1], img.split("/")[-1][-12:-4], ".tif"))
            fout2 = actual_path+"/" + "_".join((img.split("SAFE")[0][:-1].split("/")[-1], "RGB.tif"))
            
            #that comes from the zip, we are not using from the zip
            #ds = gdal.Open(imgZip)
            #a = ds.GetRasterBand(1)
            #Metadata = ds.GetMetadata()
            #ncols, nrows = a.XSize, a.YSize
            #GCPs = ds.GetGCPs()
            #GCPsProj = ds.GetGCPProjection()
            #geotransform = ds.GetGeoTransform()
            #Description = ds.GetDescription()

            #gdal.Translate(fout, ds, format="GTiff", resampleAlg='cubic', xRes=10., yRes=10., outputType=gdal.GDT_Float32)
            #ds = None

            if "B02_10m" in fout:
                BandsRGB["B"] = fout
            elif "B03_10m" in fout:
                BandsRGB["G"] = fout
            elif "B04_10m" in fout:
                BandsRGB["R"] = fout

        vrt_options = gdal.BuildVRTOptions(separate=True, hideNodata=True)
        gdal.BuildVRT(actual_path+'auxRGB/auxRGB.vrt', [BandsRGB['R'], BandsRGB['G'], BandsRGB['B']], options=vrt_options)
        ds2 = gdal.Open(actual_path+'auxRGB/auxRGB.vrt')   
        ds2 = gdal.Translate(fout2, ds2, outputType=gdal.GDT_Float32)
        ds2 = None

        # dsOut = gdal.Open(fout2)
        # dsOut.SetMetadata(Metadata)
        # dsOut.SetDescription(Description)
        # dsOut.GetRasterBand(1).SetDescription('B2')
        # dsOut.GetRasterBand(2).SetDescription('B3')
        # dsOut.GetRasterBand(3).SetDescription('B4')
        # dsOut = None

        #fDelete = imgZip.replace(".zip", ".SAFE")
        #shutil.rmtree(pathAux)
        #shutil.rmtree(f"{fDelete}")
        print(f"{fout2} :: Done!!!")
        #return None
    except:
        traceback.print_exc()




def getARGScmd():
    parser = argparse.ArgumentParser(description='worsica_resampling: Script to resample images from satellites sentinel 1/2 to 10m resolution')
    parser.add_argument('-l', '--list', help='File with the name of the images to be resampled.', required=False)
    parser.add_argument('-b', '--wktpolygon', help='WKT polygon for bounding box for subset.', metavar='N', type=str, nargs='+', required=False)
    args = parser.parse_args()
    return args


def run_resample_rgb(procDicts):
    try:
        print('run_resample_rgb')
        
        for procDict in procDicts:
            if('path' in procDict):
                ACTUAL_PATH = procDict['path']
            else:
                ACTUAL_PATH = os.getcwd()
            
            print('ACTUAL_PATH')
            print(ACTUAL_PATH)

            if not os.path.exists(ACTUAL_PATH+'resampled'):
                os.makedirs(ACTUAL_PATH+'resampled', exist_ok=True)
            if not os.path.exists(ACTUAL_PATH+'auxResample'):
                os.makedirs(ACTUAL_PATH+'auxResample', exist_ok=True)
            if not os.path.exists(ACTUAL_PATH+'auxRGB'):
                os.makedirs(ACTUAL_PATH+'auxRGB', exist_ok=True)

        #print(procDict)
        pool = Pool(cpu_count())
        results = pool.map_async(worsicaResampling, procDicts)
        pool.close()
        pool.join()
        return None
    except:
        traceback.print_exc()

def tifToJPG():
    for infile in os.listdir("./"):
        print("file : " + infile)
        if infile[-3:] == "tif" or infile[-3:] == "bmp" :
            #    print "is tif or bmp"
            outfile = infile[:-3] + "jpeg"
            im = Image.open(infile)
            print("new filename : " + outfile)
            out = im.convert("RGB")
            out.save(outfile, "JPEG", quality=90)

if __name__ == '__main__':
    args = getARGScmd()
    projWinArgs = None
    if args.list:
        with open(args.list, "r") as fin:
            imgs = fin.readlines()
            imgs = [i.strip() for i in imgs]
    else:
        imgs = glob("*MSIL2A*.zip")
    if args.wktpolygon:
        #bbox = -9.28 38.68 -9.17 38.57
        bbox = worsica_common_script_functions.generate_bbox_from_wkt(str(args.wktpolygon[0]))
        projWinArgs = {'projWinSRS': 'EPSG:4326', 'projWin': bbox} 
        
    

    procDicts = [{'imgZip': img, 'path': os.getcwd(), 'projWinArgs': projWinArgs} for img in imgs]
    #print(procDict)
    run_resample_rgb(procDicts)
    #shutil.rmtree("./auxResample")
    #imgZip = [img.replace(".zip", ".SAFE") for img in imgs]
    #for fDelete in imgZip:
    #    shutil.rmtree(f"{fDelete}")
    
