import os
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#========================================================================
# File:   worsica-resampling.py
#========================================================================
__author__ = 'Alberto Azevedo'
__doc__ = "Script to calculate the NDWI water index "
__datetime__ = '& October 2020 &'
__email__ = 'aazevedo@lnec.pt'
#========================================================================

import sys
import re
import gdal
import osr
import shutil
import argparse
import numpy as np
from pathlib import Path
from scipy.signal import argrelextrema

np.seterr(divide='ignore', invalid='ignore')

ACTUAL_PATH = ''

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def replaceMinMaxValuesNan(arr, minVal, maxVal):
    arr1 = np.where(arr >= maxVal, np.nan, arr)
    arr2 = np.where(arr1 <= minVal, np.nan, arr1)
    return arr2


def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w


def Resampling(imgZip):
    os.system(f"unzip -u {imgZip}")
    fileLista = []
    for root, dirs, files in os.walk(f"{imgZip[:-4]}.SAFE", topdown=True):
        for name in files:
            fileLista.append(os.path.join(root, name))
        for name in dirs:
            fileLista.append(os.path.join(root, name))

    l10JP2 = [i for i in fileLista if (i.endswith("B02_10m.jp2") or i.endswith("B03_10m.jp2") or i.endswith("B04_10m.jp2") or i.endswith("B08_10m.jp2"))]
    l20JP2 = [i for i in fileLista if i.endswith("B11_20m.jp2") or i.endswith("B12_20m.jp2")]
    l1020 = l10JP2 + l20JP2

    BandsRGB = {}
    foutFiles = []
    for img in l1020:
        fout = "".join((img.split("SAFE")[0][:-1], img.split("/")[-1][-12:-4], ".tif"))
        fout2 = img.split("SAFE")[0][:-1] + "_resampled.tif"
        foutFiles.append(fout)

        ds = gdal.Open(img)
        band = ds.GetRasterBand(1)
        arr = band.ReadAsArray()
        [cols, rows] = arr.shape
        Metadata = ds.GetMetadata()
        GCPs = ds.GetGCPs()
        GCPsProj = ds.GetGCPProjection()
        geotransform = ds.GetGeoTransform()
        Description = ds.GetDescription()

        gdal.Translate(fout, ds, format="GTiff", resampleAlg='cubic', xRes=10., yRes=10., outputType=gdal.GDT_Float32)
        ds = None

        if "B02_10m" in fout:
            BandsRGB["B"] = fout
        elif "B03_10m" in fout:
            BandsRGB["G"] = fout
        elif "B04_10m" in fout:
            BandsRGB["R"] = fout
        elif "B08_10m" in fout:
            BandsRGB["NIR"] = fout
        elif "B11_20m" in fout:
            BandsRGB["SWIR1"] = fout
        elif "B12_20m" in fout:
            BandsRGB["SWIR2"] = fout

    vrt_options = gdal.BuildVRTOptions(separate=True, hideNodata=True)
    gdal.BuildVRT('aux.vrt', [BandsRGB['B'], BandsRGB['G'], BandsRGB['R'], BandsRGB['NIR'], BandsRGB['SWIR1'], BandsRGB['SWIR2']], options=vrt_options)
    ds2 = gdal.Open('aux.vrt')
    ds2 = gdal.Translate(fout2, ds2)
    ds2 = None

    dsOut = gdal.Open(fout2)
    dsOut.SetMetadata(Metadata)
    dsOut.SetDescription(Description)
    dsOut.GetRasterBand(1).SetDescription('Blue')
    dsOut.GetRasterBand(2).SetDescription('Green')
    dsOut.GetRasterBand(3).SetDescription('Red')
    dsOut.GetRasterBand(4).SetDescription('NIR')
    dsOut.GetRasterBand(5).SetDescription('SWIR1')
    dsOut.GetRasterBand(6).SetDescription('SWIR2')
    dsOut = None

    foutFiles.append("aux.vrt")
    for trash in foutFiles:
        os.remove(trash)
    fDelete = imgZip.replace(".zip", ".SAFE")
    shutil.rmtree(f"{fDelete}")
    return fout2


def array2raster(img, data, EPSG_out=4326, fileout="ImgOut.tif"):
    # print("entrei aqui")
    output_raster = gdal.GetDriverByName('GTiff').Create(fileout, data["Dims"][1], data["Dims"][0], 1, gdal.GDT_Float32)  # Open the file
    # print("cheguei")
    output_raster.SetGeoTransform(data["geotransform"])
    # print("kek")
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(EPSG_out)
    output_raster.SetProjection(srs.ExportToWkt())
    output_raster.GetRasterBand(1).WriteArray(img)
    output_raster.GetRasterBand(1).SetNoDataValue(0)
    return None


def readImg(img):
    gdalImg = gdal.Open(img, gdal.GA_ReadOnly)
    Blue_B2 = np.array(gdalImg.GetRasterBand(1).ReadAsArray()) * 0.001
    Green_B3 = np.array(gdalImg.GetRasterBand(2).ReadAsArray()) * 0.001
    Red_B4 = np.array(gdalImg.GetRasterBand(3).ReadAsArray()) * 0.001
    Nir_B8 = np.array(gdalImg.GetRasterBand(4).ReadAsArray()) * 0.001
    Swir_B11 = np.array(gdalImg.GetRasterBand(5).ReadAsArray()) * 0.001
    Swir_B12 = np.array(gdalImg.GetRasterBand(6).ReadAsArray()) * 0.001
    Metadata = gdalImg.GetMetadata()
    band = gdalImg.GetRasterBand(1)
    arr = band.ReadAsArray()
    [cols, rows] = arr.shape
    GCPs = gdalImg.GetGCPs()
    GCPsProj = gdalImg.GetGCPProjection()
    geotransform = gdalImg.GetGeoTransform()
    Description = gdalImg.GetDescription()
    proj = osr.SpatialReference(wkt=gdalImg.GetProjection())

    data = {"Blue": Blue_B2,
            "Green": Green_B3,
            "Red": Red_B4,
            "Nir": Nir_B8,
            "Swir1": Swir_B11,
            "Swir2": Swir_B12,
            "Metadata": Metadata,
            "Dims": [cols, rows],
            "EPSG": np.int(proj.GetAttrValue('AUTHORITY', 1)),
            "GCPs": GCPs,
            "GCPsProj": GCPsProj,
            "geotransform": geotransform,
            "Description": Description}
    return data


def worsicaAutomaticThreshold(image, graphics=False):
    # Modified histogram bimodal method (MHBM)
    image2 = replaceMinMaxValuesNan(image, image.min(), image.max())
    image2 = image2[~np.isnan(image2)]
    # print(image2.min(), image2.max())
    if graphics:
        plt.figure()
        plt.subplot(311)
    hist = plt.hist(image2.ravel(), bins=256)
    histFiltered = moving_average(hist[0], 5)
    xHist = np.linspace(image2.min(), image2.max(), len(histFiltered))
    if graphics:
        plt.subplot(312)
        plt.plot(xHist, histFiltered)
        plt.ylim([0, histFiltered.max()])
    # for local maxima
    maxs = argrelextrema(histFiltered, np.greater)

    if graphics:
        for j in maxs[0]:
            plt.axvline(xHist[j], color="g")

    val = find_nearest(xHist, 0.)
    # print(val)
    idx = np.where(xHist == val)
    # print(idx)
    refVal = idx[0][0]
    # print(refVal)
    val = find_nearest(maxs[0], refVal)
    # print(val)
    if val >= refVal:
        maxHigh = val
        idxHigh = np.where(maxs[0] == val)
        maxLow = maxs[0][idxHigh[0][0] - 1]
        idxLow = np.where(maxs[0] == maxLow)
    elif val < refVal:
        maxLow = val
        idxLow = np.where(maxs[0] == val)
        maxHigh = maxs[0][idxLow[0][0] + 1]
        idxHigh = np.where(maxs[0] == maxHigh)

    # print(maxLow, maxHigh)
    # print(idxLow[0][0], idxHigh[0][0])

    minHistFilt = np.min(histFiltered[maxLow: maxHigh])
    idx = np.where(histFiltered[maxLow: maxHigh] == minHistFilt)
    thresh_min = xHist[maxLow: maxHigh][idx[0][0]]
    print("Auto-Threshold:", thresh_min)

    if graphics:
        plt.subplot(313)
        plt.plot(xHist, histFiltered)
        plt.ylim([0, histFiltered.max()])
        plt.axvline(thresh_min, color="r")
        arr1 = np.where(image > thresh_min, 1, image)
        arr2 = np.where(image < thresh_min, 0, arr1)
        # Creates two subplots and unpacks the output array immediately
        f, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True)
        ax1.imshow(image, cmap="gray")
        ax2.imshow(arr2, cmap="gray")
        plt.show()

    return thresh_min


def WaterIndex(img):
    imgSTR = str(Path(img).resolve())

    #Replace, MOSAIC only. defines correct path for folder
    #ACTUAL_PATH = '/home/morocha/OPENCoastS/utilities/satellite/'
    #ACTUAL_PATH = '/srv/www/mosaic/web-portal/utils/satellite/'

    imgSTR = ACTUAL_PATH + img.split("/")[-1]

    data = readImg(imgSTR)
    OutIndex = 0
    # Normalized  Difference  water  Index (NDWI) 1 - For waterbodies
    # NDWI=(GREEN−NIR/GREEN+NIR)
    NDWI = (data["Green"] - data["Nir"]) / (data["Green"] + data["Nir"])
    OutIndex = np.array(NDWI)
    try:
        threshold = worsicaAutomaticThreshold(OutIndex)
    except:
        exit()

    Out = np.where(OutIndex < threshold, np.nan, 1.)
    # plt.figure()
    # plt.imshow(Out, cmap="gray")
    # plt.title("NDWI")
    # plt.show()
    # plt.savefig('test.png')
    print("imgSTR: " + imgSTR)

    fout = "".join((imgSTR[:-4], "_NDWI.tif"))
    print(fout)

    #f = open(fout.split(".")[0] + ".txt", "w+")
    #f.write(str(threshold))
    #f.close()

    array2raster(Out, data, EPSG_out=data["EPSG"], fileout=fout)
    return Out


def getARGScmd():
    parser = argparse.ArgumentParser(description='worsica_WaterIndexes: Script to resample and generate the water index NDWI from satellites sentinel 2 to 10m resolution')
    parser.add_argument('-img', '--image', help='Name of the input image.', required=True)
    #parser.add_argument("-t", "--threshold", help="NDWI threshold (Default= 0.). Positive values represent water content. Negative values represent no vegetation or water content. Example: -t 0 ", default=0., required=False)
    parser.add_argument('-i', '--path', help='Final path of the input image.', required=True)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = getARGScmd()
    #resampled = Resampling(args.image)

    ACTUAL_PATH = args.path

    resampled = args.image
    out = WaterIndex(resampled)
