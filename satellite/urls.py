from django.conf.urls import url

from .views import get_rasters


urlpatterns = [
    url(r'^get_rasters/(?P<deploy_id>[0-9]+)/$', get_rasters, name='get_rasters'),
]
