from django.conf.urls import url, include
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('django_auth_oidc.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^', include('front.urls')),
    url(r'^wizard/', include('wizard.urls')),
    url(r'^viewer/', include('viewer.urls')),

    url(r'^raster/', include('raster.urls')),
    url(r'^satellite/', include('satellite.urls')),
]

handler400 = 'front.views.handler400'
handler403 = 'front.views.handler403'
handler404 = 'front.views.handler404'
handler500 = 'front.views.handler500'
