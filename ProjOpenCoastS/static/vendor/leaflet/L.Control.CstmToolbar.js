(function (window, document, undefined) {
    L.Control.CstmToolbar = L.Control.extend({
        options: {
            position: 'topright',
            id: '',
            title: '',
            classes: '',
            buttons: [],    //id options: zoomin, zoomout, fullout, zoomlayer, mapclick (addpoint)
        },
        container: null,

        onAdd: function (map) {
            this.container = L.DomUtil.create('div');
            this.container.id = this.options.id;
            this.container.title = this.options.title;
            this.container.className = this.options.classes;

            for (var i = 0; i < this.options.buttons.length; i++){
                var opts = this.options.buttons[i];
                var button = this.buildButton(opts['id'], opts['title'], opts['classes'], opts['active'], map);
                this.container.appendChild(button);
            }

            /* Prevent click events propagation to map */
            L.DomEvent.disableClickPropagation(this.container);

            /* Prevent right click event propagation to map */
            L.DomEvent.on(this.container, 'contextmenu', function (ev)
            {
                L.DomEvent.stopPropagation(ev);
            });

            /* Prevent scroll events propagation to map when cursor on the div */
            L.DomEvent.disableScrollPropagation(this.container);

            map.on('zoomend zoomlevelschange', this.updateDisabled, this)

            return this.container;
        },

        buildButton: function (id, title, classes, active, _map) {
            var button = L.DomUtil.create('button', 'btn btn-default leaflet-button');
            button.type = 'button';
            button.id = id;
            button.title = title;
            if (id == 'addpoint' && active) {
                L.DomUtil.addClass(button, 'active');
                $(_map._container).addClass('cursor-crosshair');
            }

            var icon = L.DomUtil.create('i');
            L.DomUtil.addClass(icon, classes);
            button.appendChild(icon);

            // Add event
            button.onclick = function() {
                this.blur();

                if (this.id === 'zoomin') {
                    //Zoom in
                    _map.zoomIn();
                } else if (this.id === 'zoomout') {
                    //Zoom out
                    _map.zoomOut();
                } else if (this.id === 'fullout') {
                    if (bboxLayer) {
                        //Zoom to full bounds
                        _map.fitBounds(bboxLayer.getBounds());
                    } else {
                        _map.fitWorld();
                    }
                } else if (this.id === 'zoomlayer') {
                    if (boundariesLayer) {
                        //Zoom to layers
                        _map.fitBounds(boundariesLayer.getBounds(), {'padding': [50,50]});
                    }
                } else if (this.id === 'addpoint') {
                    if ($(this).hasClass('active')) {
                        $(_map._container).removeClass('cursor-crosshair');
                        $(this).removeClass('active');
                        pointState = false;
                    } else {
                        $(_map._container).addClass('cursor-crosshair');
                        $(this).addClass('active');
                        pointState = true;
                    }
                }
            }

            return button;
        },

        updateDisabled: function () {
            // Enable/disable zoom buttons if max or min zooms
            var level = map.getZoom();
            if (level === map.getMinZoom()) {
                $('#fullout, #zoomout').attr('disabled', '');
            } else {
                $('#fullout, #zoomout').removeAttr('disabled');
            }

            if (level === map.getMaxZoom()) {
                $('#zoomin').attr('disabled', '');
            } else {
                $('#zoomin').removeAttr('disabled');
            }
        },
    });

    L.control.cstmtoolbar = function (options) {
        return new L.Control.CstmToolbar(options);
    };

}(window, document));