$(function(){
    $('#langmenu button').click(function(e){
        e.preventDefault();
        $('#language').val($(this).attr('lang_code'));
        $('#language-form').submit();
    });

    // Dismiss focus on click
    $('a').on('click', function () {
        $(this).blur();
    });

    // Dismiss focus on click
    $('input[type="radio"]').on('click', function () {
        $(this).blur();
    });

    // Dismiss collapse on click
    $('[data-toggle="collapse"]').on('click', function () {
        $(this).blur();
    });

    // Enable all tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Dismiss tooltip on click
    $('[data-toggle="tooltip"]').on('click', function () {
        $(this).blur();
    });

    // Tips buttons
    $('#tips_btn').click(function(e){
        intro.start();
    });

    // Help buttons
    $('#help_btn').click(function(e){
        $('#help').slideToggle();

        var self = $(this);
        // Toggle Class
        self.toggleClass('active');
        // Toggle tooltip title
        if (self.hasClass("active")) {
            self.attr("title", self.attr('data-title_alt'));
        } else {
            self.attr("title", self.attr('data-title'));
        }
        self.tooltip('fixTitle');
    });

    // Prevent errors with floats and localization
    $('input[type="number"]').on('blur', function () {
        var val = parseFloat($(this).val());
        $(this).val(val);
    });
});

// Function that validates server-side if user can create new deploy
// Commented calls to this function, creating new_systems is allowed, but submitting them is not
function newdeploy(validateurl, sendtourl) {
    $.ajax({
        type: 'GET',
        url: validateurl,
        success: function(data) {
            if (data['msg']) {
                $('#msg_str').html(data['msg']);
                $('#divModal_msg').modal('show');
            } else {
                window.location.href = sendtourl;
            }
        },
        error: function(e, x, r) {
            console.log(r + ' - ' + e);
            $('#msg_str').html('{% trans "Ocorreu um erro! Por favor tente mais tarde." %}');
            $('#divModal_msg').modal('show');
        }
    });
}

// Show/hide spinner effect
function spinner(on) {
    if (on) {
        // Add loading cursor
        $("html,body").addClass('cursor-wait');
        if ($('#map').length) $('#map').addClass('cursor-wait');
    } else {
        // Remove loading cursor
        $("html,body").removeClass('cursor-wait');
        if ($('#map').length) $('#map').removeClass('cursor-wait');
    }
}

// Return random colors for an interval
function getRandomColors(n, nrcolors) {
    if (nrcolors < 1) nrcolors = 1;
    return "hsl(" + n * (360 / nrcolors) % 360 + ", 100%, 40%)";
}

// Return random color
function getRandomColor() {
    return "hsl(" + (360 - Math.floor(Math.random() * 360) + 1) % 360 + ", 100%, 40%)";
}

// Browser supports svg
function supports_SVG() {
    return !!(document.createElementNS && document.createElementNS('http:www.w3.org/2000/svg', 'svg').createSVGRect);
}