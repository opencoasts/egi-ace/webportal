import os
import json
from argparse import ArgumentParser, Action as ArgumentAction, SUPPRESS, \
                                                   ArgumentDefaultsHelpFormatter
from collections import namedtuple
from django.core.management.utils import get_random_secret_key
from pathlib import Path
from pprint import pprint

_ENV_PREFIX = 'OC_'

# TODO: include options for actions like directory (logs, runs, etc...) creation

setting_class = namedtuple('Setting', 'name default cli_kwargs')

def parse_settings(setup_options_json_path, local_settings_json_path):
    setup_json = json.loads( setup_options_json_path.read_text() )

    try:
        settings_json = json.loads( local_settings_json_path.read_text() )
    except FileNotFoundError:
        settings_json = dict()

    def parse_setting_(value):
        try:
            kind, separator, reference = value.partition(':')
            if separator:

                if kind == 'fn':
                    return globals()[reference]()

        except AttributeError:
            pass

        return value

    env_value = lambda name: os.getenv(_ENV_PREFIX+name)
    setup_value = lambda name: settings_json.get(name)
    
    def setting(name, default=None, **cli_kwargs):
        default_value = env_value(name) or setup_value(name) or \
                                                         parse_setting_(default)
        return setting_class(name, default_value, cli_kwargs)

    return tuple( setting(**entry) for entry in setup_json )


class ListSettingsDefaultValueAction(ArgumentAction):

    def __init__(self, *args, default=SUPPRESS, nargs=None, settings=None,
                                                                      **kwargs):
        if nargs is not None:
            raise ValueError(f"{type(self).__name__} doesn't allow setting "
                                                                       "nargs!")

        self.settings = settings or tuple()
        super().__init__(*args, nargs=0, default=default, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        print( '\n'.join( f'{entry.name}={entry.default}'
                     for entry in self.settings if entry.default is not None ) )
        exit()


def parse_cli(settings):
    parser = ArgumentParser(
        description = "OPENCoastS setup",
        # argument_default = SUPPRESS,
        formatter_class = ArgumentDefaultsHelpFormatter,
        epilog = f"""You may use environment variables to define arguments
            default by prefixing them with {_ENV_PREFIX} (eg:
            {_ENV_PREFIX}ARGUMENT_NAME=default).""",
    )

    parser.add_argument('-d', '--defaults',
        action=ListSettingsDefaultValueAction, settings=settings,
        help = 'list arguments which provide a default value',
    )

    for entry in settings:
        is_required = entry.default is None
        parser.add_argument(f'-{entry.name}', default=entry.default,
                                       required=is_required, **entry.cli_kwargs)

    return vars( parser.parse_args() )

_SELF_PATH = Path(__file__)
LOCAL_SETTINGS_JSON_PATH = _SELF_PATH.parent / 'local_settings.json'

def cli_create_local_settings(template_path, setup_options_path):
    options = parse_settings(setup_options_path, LOCAL_SETTINGS_JSON_PATH)
    argparse_map = parse_cli(options)

    template_text = template_path.read_text()
    template_path.with_suffix('').write_text(
                                        template_text.format_map(argparse_map) )
    LOCAL_SETTINGS_JSON_PATH.write_text(
                             json.dumps(argparse_map, skipkeys=True, indent=4) )
    print("Updated local_settings module and json file!")

SETUP_OPTIONS_PATH = _SELF_PATH.with_suffix('.json')
LOCAL_SETTINGS_TEMPLATE_PATH = _SELF_PATH.parent / 'local_settings.py.template'

if __name__ == '__main__':
    cli_create_local_settings(LOCAL_SETTINGS_TEMPLATE_PATH, SETUP_OPTIONS_PATH)
