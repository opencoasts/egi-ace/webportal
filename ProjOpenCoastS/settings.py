# -*- coding: utf-8 -*-
"""
Django settings for ProjOpenCoastS project.

Generated by 'django-admin startproject' using Django 1.11.
For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""
import os
import logging

WEBSITE_NAME = "OPENCoastS"

LOGGER_NAME = WEBSITE_NAME.lower()
LOGGER = logging.getLogger(LOGGER_NAME)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'datamodel.apps.DatamodelConfig',
    # 'datamodel2.apps.Datamodel2Config',
    'front.apps.FrontConfig',
    'wizard.apps.WizardConfig',
    'viewer.apps.ViewerConfig',
    
    'django_auth_oidc',
    'django_tables2',
    'django_json_widget',
    'django_countries',
    'django.contrib.gis',
]

MIDDLEWARE = [
    #'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',   # TODO: handling static file serving temporarily

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'django.middleware.locale.LocaleMiddleware',
]

ROOT_URLCONF = 'ProjOpenCoastS.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'ProjOpenCoastS', 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
            ],
        },
    },
]

WSGI_APPLICATION = 'ProjOpenCoastS.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Logging configuration
LOGGING_TO_ = lambda log_dir: dict(
    version = 1,
    disable_existing_loggers = False,
    filters = {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    formatters = {
        'simple': {
            'format': '%(asctime)s %(levelname)s %(module)s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'verbose': {
            'format': '%(asctime)s %(levelname)s %(pathname)s.%(funcName)s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    handlers = {
        'debug_logfile': {
            'level': 'DEBUG',
            #'filters': ['require_debug_true'],
            'class': 'logging.FileHandler',
            'filename': os.path.join(log_dir, 'debug.log'),
            'formatter': 'simple',
        },
        'error_logfile': {
            'level': 'ERROR',
            #'filters': ['require_debug_true'],
            'class': 'logging.FileHandler',
            'filename': os.path.join(log_dir, 'error.log'),
            'formatter': 'verbose',
        },
        # 'mail_admins': {
        #     'level': 'ERROR',
        #     #'filters': ['require_debug_false'],
        #     'class': 'django.utils.log.AdminEmailHandler',
        #     'include_html': True,
        # },
    },
    loggers = {
        LOGGER_NAME: {
            'handlers': ['debug_logfile', 'error_logfile'],
            'level': 'DEBUG',
        },
    },
)

DEFAULT_LOGS_DIR = os.path.join(BASE_DIR, '_logs')
LOGGING = LOGGING_TO_(DEFAULT_LOGS_DIR)

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/
LANGUAGE_CODE = 'pt'

from django.utils.translation import ugettext_lazy as _

LANGUAGES = [
  ('pt', _('Português')),
  ('en-gb', _('English')),
]

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = False
USE_TZ = True
FILE_CHARSET = 'utf-8'

DATA_UPLOAD_MAX_MEMORY_SIZE = 10 * 1024 * 1024 # 10 MB

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, "static")

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'ProjOpenCoastS', 'static'),
)

# Email notifications settings
NOTIFICATIONS_ENABLED = False

# Authentication
AUTH_PROFILE_MODULE = 'front.UserProfile'

# OpenID Connect Authentication (django_auth_oidc)
#AUTH_SERVER = 'https://aai.egi.eu/oidc/'
#AUTH_SCOPE = ('openid', 'profile', 'email')
AUTH_SERVER = 'https://aai.egi.eu/auth/realms/egi'
AUTH_SERVER_USERINFO = AUTH_SERVER + '/protocol/openid-connect/userinfo'
AUTH_SCOPE = ('openid', 'profile', 'email', 'voperson_id')
LOGIN_REDIRECT_URL = '/login_egi/'

# output result files settings, used in datamodel.DeploymentOutputFilepath
_re_results = r'(?:schouts_\d+|hotstart_it=\d+|wwm_hotstart)\.nc'
_re_logs = r'(?:logs|confs|forcings)\.tar\.xz'
OUTPUT_FILES_MATCH = r'(?:{results}|{logs})'.format(results=_re_results,
                                                                  logs=_re_logs)


try:
    from .local_settings import *
except ModuleNotFoundError:
    print("ERROR: local_settings module not found, you may use "
                                f"{__package__}.setup cli module to create it!")
