Django==2.2.*
django-tables2==1.10.*
django_countries==5.3
django-json-widget==1.*
django-auth-oidc==0.6.*
psycopg2-binary==2.7.*
numpy==1.16.*
netCDF4==1.5.*
matplotlib==2.0.2
geojson==2.3.*
Shapely==1.6.3
requests==2.*
whitenoise==3.*
gunicorn

f90nml

# satellite app
Pillow
django-raster
sentinelsat
scipy
