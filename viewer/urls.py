from django.conf.urls import url
from django.views.static import serve

from .views import *


urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^get_station_data/(?P<station_id>[0-9]+)/(?P<param>[[\w\-]+)/(?P<startdate>\d{2}-\d{2}-\d{4})/(?P<enddate>\d{2}-\d{2}-\d{4})/$',
        get_station_data, name='get_station_data'),
    url(r'^files/(?P<path>(?:\w|/|-)+/{})$'.format(settings.OUTPUT_FILES_MATCH),
        serve, dict(document_root=settings.DEPLOYMENTS_PATH),
        name='viewer-files'),
]

# from django.conf.urls.static import static

# urlpatterns.extend(
#    static('files/', document_root=settings.DEPLOYMENTS_PATH)
# )
