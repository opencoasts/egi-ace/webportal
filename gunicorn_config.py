import multiprocessing
from pathlib import Path

# general
bind = '0.0.0.0'

_min_workers = min(multiprocessing.cpu_count()*2, 16) # at most 16 workers
workers = max(_min_workers, 4) # at least 4 workers

# timeout
timeout = 120

# logging
log_dir = Path('_logs')

accesslog = str(log_dir/'gunicorn_access')
#errorlog = str(log_dir/'gunicorn_error')
#capture_output = True

pidfile = str(log_dir/'gunicorn_pidfile')
