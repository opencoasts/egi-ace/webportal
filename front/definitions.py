from wiff.opencoasts.models import Configuration

run_type_map = {
    ('basic2d', 'nowaves', 'nowqtype'): Configuration.Target.BAROTROPIC,
    ('basic2d', 'nowaves', 'fecalwqtype'):
                                   Configuration.Target.BAROTROPIC_MICROBIOLOGY,
    ('basic2d', 'nowaves', 'genericwqtype'):
                                   Configuration.Target.BAROTROPIC_MICROBIOLOGY,

    ('basic2d', 'yeswaves', 'nowqtype'): Configuration.Target.BAROTROPIC_WAVES,
    ('basic2d', 'yeswaves', 'fecalwqtype'):
                             Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY,
    ('basic2d', 'yeswaves', 'genericwqtype'):
                             Configuration.Target.BAROTROPIC_WAVES_MICROBIOLOGY,

    ('basic3d', 'nowaves', 'nowqtype'): Configuration.Target.BAROCLINIC,
    ('basic3d', 'nowaves', 'fecalwqtype'):
                                   Configuration.Target.BAROCLINIC_MICROBIOLOGY,
    ('basic3d', 'nowaves', 'genericwqtype'):
                                   Configuration.Target.BAROCLINIC_MICROBIOLOGY,

    ('basic3d', 'yeswaves', 'nowqtype'): Configuration.Target.BAROCLINIC_WAVES,
    ('basic3d', 'yeswaves', 'fecalwqtype'):
                             Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY,
    ('basic3d', 'yeswaves', 'genericwqtype'):
                             Configuration.Target.BAROCLINIC_WAVES_MICROBIOLOGY,
}

kind_type_map = {
    ('param'): Configuration.Kind.PARAM_NML,
    ('wwminput'): Configuration.Kind.WWMINPUT_NML,
}