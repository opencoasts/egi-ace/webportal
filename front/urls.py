from django.conf.urls import url
from .views import *
from django.views.generic import TemplateView

# TODO: when there is more than one app: point to views inside 'front' redirecting to the app selected by the user
from wizard.views import deployments, new_deploy, view_deploy, open_deploy, clone_deploy, deactivate_deploy, \
    reactivate_deploy, delete_deploy, extend_deploy, view_extend_deploys, get_file, get_definition, print_deploy, \
    get_deploys, get_boundaries


urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^home/$', home, name='home'),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', logout_user, name='logout'),
    url(r'^register/$', register, name='register'),
    url(r'^register/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', register_confirm, name='register_confirm'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^disclaimer/$', disclaimer, name='disclaimer'),

    url(r'^ratings/$', rate_service, name='rate_service'),

    url(r'^password/change/$', password_change, name='password_change'),
    url(r'^password/recover/$', password_recover, name='password_recover'),
    url(r'^password/recover/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', password_recover_confirm, name='password_recover_confirm'),

    url(r'^userprofile/activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', userprofile_activate, name='userprofile_activate'),
    url(r'^activate_user/(?P<username>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$', activate_user, name='activate_user'),

    url(r'^login_egi/$', login_egi, name='login_egi'),
    url(r'^login_egi/complete/$', login_egi_complete, name='login_egi_complete'),
    url(r'^logout_egi/$', logout_egi, name='logout_egi'),

    url(r'^deployments/$', deployments, name='deployments'),
    url(r'^deployments/new/$', new_deploy, name='new_deploy'),
    url(r'^deployments/view/(?P<deploy_id>[0-9]+)/$', view_deploy, name='view_deploy'),
    url(r'^deployments/open/(?P<deploy_id>[0-9]+)/$', open_deploy, name='open_deploy'),
    url(r'^deployments/clone/(?P<deploy_id>[0-9]+)/$', clone_deploy, name='clone_deploy'),
    url(r'^deployments/deactivate/(?P<deploy_id>[0-9]+)/$', deactivate_deploy, name='deactivate_deploy'),
    url(r'^deployments/reactivate/(?P<deploy_id>[0-9]+)/$', reactivate_deploy, name='reactivate_deploy'),
    url(r'^deployments/delete/(?P<deploy_id>[0-9]+)/$', delete_deploy, name='delete_deploy'),
    url(r'^deployments/extend/$', extend_deploy, name='extend_deploy'),
    url(r'^deployments/view_extends/$', view_extend_deploys, name='view_extend_deploys'),
    url(r'^deployments/get_deploys/$', get_deploys, name='get_deploys'),
    url(r'^deployments/get_definition/(?P<name>[[\w\-]+)/$', get_definition, name='get_definition'),
    url(r'^deployments/get_definition/(?P<name>[[\w\-]+)/(?P<action>[[\w\-]+)/(?P<deploy_id>[0-9]+)$', get_definition, name='get_definition'),

    url(r'^deployments/file/(?P<action>[[\w\-]+)/(?P<deploy_id>[0-9]+)/(?P<name>[[\w\-]+)/(?P<format>[[\w\-]+)/$', get_file, name='get_file'),
    url(r'^deployments/print/(?P<deploy_id>[0-9]+)/$', print_deploy, name='print_deploy'),
    url(r'^print/$', TemplateView.as_view(template_name="front/modal/printdeploy.html"), name='print'),

    url(r'^deployments/get_boundaries/(?P<deploy_id>[0-9]+)/$', get_boundaries, name='get_boundaries'),

    url(r'^download_file/(?P<file_cat>[[\w\-]+)/(?P<file_id>[0-9]+)/$', download_file, name='download_file'),

    url(r'^run_stats/monthly_time/(?P<year>\d{4})/(?P<month>\d{2})$', monthly_run_time),
]
