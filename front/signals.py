from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.contrib.auth.models import User
from django.utils import timezone

from front.utils import notify_set_active, notify_extend_deploy_response
from front.models import Deployment, ExtendDeployRequest
from django.conf import settings

logger = settings.LOGGER.getChild('front')


@receiver(pre_save, sender=User)
def set_new_user_inactive(sender, instance, **kwargs):
    # On User creation set is_active = False
    # Send email notification to user when is_active changes
    try:
        if not instance.is_superuser and not instance.is_staff:
            if instance._state.adding is True:
                # On User creation set is_active to False
                instance.is_active = False
            else:
                old_user = User.objects.get(pk=instance.pk)

                if not old_user.is_active and instance.is_active:
                    # User was activated - Send email to user
                    notify_set_active(instance, True)
                elif old_user.is_active and not instance.is_active:
                    # User was deactivated - Send email to user
                    notify_set_active(instance, False)
    except Exception:
        logger.exception('Error on set_new_user_inactive')


@receiver(pre_save, sender=ExtendDeployRequest)
def extend_deploy(sender, instance, **kwargs):
    # When editing Extend Deploy Requests check if request is closed and
    # update Deployment if applicable and send email notification to user
    # with response
    try:
        if instance._state.adding is False:
            # Check if request was closed
            old_extend = ExtendDeployRequest.objects.get(pk=instance.pk)
            if not old_extend.closed and instance.closed:
                # Update reply_date to today
                instance.reply_date = timezone.now()

                # If accepted update Deployment's end_date automatically
                deploy = Deployment.objects.get(pk=instance.deployment.pk)
                if instance.accepted and instance.to_date != deploy.end_date:
                    deploy.end_date = instance.to_date
                    deploy.save()

                # Request was closed - Send email to user
                notify_extend_deploy_response(instance)
    except Exception:
        logger.exception('Error on extend_deploy')
