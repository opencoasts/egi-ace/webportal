# -*- coding: utf-8 -*-
import requests

from django.shortcuts import render, redirect, HttpResponseRedirect, HttpResponse
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _

from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.urls import reverse
from django.contrib.auth.hashers import make_password
from django.contrib.auth.tokens import default_token_generator

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash

from django.http import JsonResponse

from .utils import *
from .forms import *
from .models import UserProfile, UserProfileRating, SessionFile
from datamodel.models import DeploymentFile

app_name = 'front'


# View for Errors
def handler400(request, exception, template_name="front/error.html"):
    response = render(request, 'front/error.html', {'error': _('Pedido inválido.')})
    response.status_code = 400
    return response


def handler403(request, exception, template_name="front/error.html"):
    response = render(request, 'front/error.html', {'error': _('Permissões insuficientes.')})
    response.status_code = 403
    return response


def handler404(request, exception, template_name="front/error.html"):
    response = render(request, 'front/error.html', {'error': _('Página não encontrada.')})
    response.status_code = 404
    return response


def handler500(request):
    response = render(request, 'front/error.html', {'error': _('Erro de servidor.')})
    response.status_code = 500
    return response


# View for index
def home(request):
    return render(request, 'front/home.html', {'user': request.user, 'default_email': settings.DEFAULT_FROM_EMAIL})


# Class for login
class Login(View):
    form_class = AuthenticationForm
    template_name = 'front/login.html'

    def get(self, request):
        if request.user.is_authenticated:
            # If first session ever open in step1
            if not request.user.profile.read_disclaimer:
                return redirect('/wizard/step1')
            else:
                # If neither open deployments management page
                return redirect('/deployments')
        else:
            form = self.form_class(None)
            return render(request, self.template_name, {'form': form})

    def post(self, request):
        username = escape(request.POST['username'])
        password = escape(request.POST['password'])

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)

                if 'next' in request.GET:
                    gonext = request.GET['next']
                    return HttpResponseRedirect(gonext)
                else:
                    # If first session ever open in step1
                    if not request.user.profile.read_disclaimer:
                        return redirect('/wizard/step1')
                    else:
                        # If neither open deployments management page
                        return redirect('/deployments')

            else:
                response = _('A sua conta encontra-se desativada!')
                return render(request, self.template_name, {'response': response})
        else:
            response = _('Entrada em sessão inválida!')
            return render(request, self.template_name, {'response': response})


# View for register
def register(request):
    response = None

    if request.method == 'POST':
        user_form = RegistrationForm(request.POST)
        profile_form = ProfileForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            # Save new User -> username = email
            new_user = user_form.save(commit=False)
            new_user.email = new_user.username
            new_user.save()

            if new_user:
                # Save profile info to new User
                new_profile = profile_form.save(commit=False)
                new_profile.user = new_user
                new_profile.save()
                profile_form.save_m2m()

                # Send email notifying user's register
                token = default_token_generator.make_token(new_user)
                uidb64 = urlsafe_base64_encode(force_bytes(new_user.pk))
                confirmation_link = request.build_absolute_uri(reverse('front:register_confirm', kwargs={'uidb64': uidb64, 'token': token}))

                notify_registration(new_user, new_profile, confirmation_link)

                response = _('Por favor verifique a sua conta de email (incluindo SPAM). Receberá um email para confirmar o seu registo. '
                             'Assim que o seu registo for confirmado e validado com sucesso receberá um email da ativação da conta.')

                profile_form = None
                user_form = None
            else:
                response = _('Ocorreu um erro! Por favor tente mais tarde.')
    else:
        if request.user.is_authenticated:
            return redirect('/home', {'user': request.user})
        else:
            user_form = RegistrationForm(None)
            profile_form = ProfileForm(None)

    return render(request, 'front/register.html', {'profile_form': profile_form, 'user_form': user_form, 'response': response})


# View for user register confirmation
def register_confirm(request, uidb64, token):
    try:
        # User confirming registration via OPENCoastS - if valid send notification to LNEC
        new_user = User.objects.get(pk=urlsafe_base64_decode(uidb64))
        token_valid = default_token_generator.check_token(new_user, token)

        if token_valid:
            # Already active? no need for confirmation, go to login
            if new_user.is_active:
                return redirect('/login')
            else:
                new_profile = UserProfile.objects.get(user=new_user)

                if new_profile.confirm_registration:
                    response = _('Ocorreu um erro! Este URL não é válido ou já expirou.')
                else:
                    # Mark registration confirmation in DB
                    new_profile.confirm_registration = True
                    new_profile.save()
                    response = _('Registo confirmado! Assim que o seu registo for validado com sucesso receberá um email da ativação da conta.')

                    # Send user activation link to DEFAULT_FROM_EMAIL
                    activation_link = request.build_absolute_uri(reverse('front:userprofile_activate', kwargs={'uidb64': uidb64, 'token': token}))
                    notify_confirm_registration(new_user, new_profile, activation_link)
        else:
            response = _('Ocorreu um erro! Este URL não é válido ou já expirou.')

    except Exception:
        logger.exception('Error on register confirm')
        response = _('Ocorreu um erro! Por favor tente mais tarde.')

    return render(request, 'front/register.html', {'response': response})


# View for Profile
@login_required(login_url='/login/')
def profile(request):
    response = None

    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = FullProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            try:
                user_form.save()
                profile_form.save()
                response = _('O seu perfil foi atualizado com sucesso!')
            except:
                response = _('Ocorreu um erro! Por favor tente mais tarde.')
        else:
            response = _('Ocorreu um erro! Por favor tente mais tarde.')
    else:
        if not request.user.is_authenticated:
            return redirect('/home', {'user': request.user})
        else:
            user_form = UserForm(instance=request.user)
            profile_form = FullProfileForm(instance=request.user.profile)

    # Distinguish between normal users from EGI users (these cannot change passwords)
    disable_password = False
    if request.user.password == '***':
        disable_password = True

    return render(request, 'front/profile.html', {'user_form': user_form, 'profile_form': profile_form, 'response': response, 'disable_password': disable_password})


# View for Password update
@login_required(login_url='/login/')
def password_change(request):
    response = None

    if request.user.has_usable_password():
        pwd_form = PasswordChangeForm
    else:
        pwd_form = AdminPasswordChangeForm

    if request.method == 'POST':
        form = pwd_form(request.user, request.POST)
        if form.is_valid():
            try:
                form.save()
                update_session_auth_hash(request, form.user)
                response = _('A sua palavra-passe foi alterada com sucesso!')
            except:
                response = _('Ocorreu um erro! Por favor tente mais tarde.')
        else:
            response = _('Ocorreu um erro! Por favor tente mais tarde.')
    else:
        form = pwd_form(request.user)

    return render(request, 'front/password.html', {'pwd_form': form, 'response': response})


# View for Password recover - open reset pwd form
def password_recover_confirm(request, uidb64, token):
    token_valid = False
    response = None
    form = None

    try:
        user = User.objects.get(pk=urlsafe_base64_decode(uidb64))
        token_valid = default_token_generator.check_token(user, token)

        if token_valid:
            pwd_form = AdminPasswordChangeForm

            if request.method == 'POST':
                form = pwd_form(request.user, request.POST)
                if form.is_valid():
                    try:
                        user.password = make_password(form.cleaned_data['password1'])
                        user.save()

                        response = _('A sua palavra-passe foi alterada com sucesso!')
                        form = None
                    except Exception:
                        logger.exception('Error saving new password')
                        response = _('Ocorreu um erro! Por favor tente mais tarde.')
            else:
                form = pwd_form(user)
        else:
            response = _('Ocorreu um erro! Este URL não é válido ou já expirou.')
    except Exception:
        logger.exception('Error on password recover confirmation')
        response = _('Ocorreu um erro! Por favor tente mais tarde.')

    return render(request, 'front/password.html', {'pwd_form': form, 'response': response, 'token_valid': token_valid})


# View for Password recover - send email with recover link
def password_recover(request):
    response = _('Ocorreu um erro! Por favor tente mais tarde.')
    formdata = request.POST

    if request.method == 'POST':
        username = formdata['username']
        try:
            # Generate user token
            user = User.objects.get(username=username)
            token = default_token_generator.make_token(user)
            uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
            password_recover_link = request.build_absolute_uri(reverse('front:password_recover_confirm', kwargs={'uidb64': uidb64, 'token': token}))

            notify_user_password_recover(user, password_recover_link)
            response = _('Por favor verifique a sua conta de email. Receberá um email com indicações de recuperação da sua palavra-passe.')

        except Exception:
            logger.exception('Error on password recover')
            response = _('Não existe nenhuma conta associada ao email indicado.')

    return JsonResponse({'response': response})


# View for disclaimer
@login_required(login_url='/login/')
def disclaimer(request):
    return render(request, 'front/disclaimer.html', {'default_email': settings.DEFAULT_FROM_EMAIL})


# View for logout
def logout_user(request):
    logout(request)
    return redirect('/home')


# View for login/register via EGI Check-in service - Login redirect
def login_egi(request):
    msg = _('Ocorreu um erro durante a autenticação! Tente mais tarde.')
    try:
        KEY_TOKEN = 'access_token' #'openid_token'
        if KEY_TOKEN in request.session: #if 'openid_token' in request.session:
       	    user = request.user

            if user.is_active:
                # Redirecting
                if 'next' in request.GET:
                    gonext = request.GET['next']
                    return HttpResponseRedirect(gonext)
                else:
                    # If first session ever open in step1
                    if not request.user.profile.read_disclaimer:
                        return redirect('/wizard/step1')
                    else:
                        # If neither open deployments management page
                        return redirect('/deployments')
            else:
                # Get user information
                access_token = request.session[KEY_TOKEN]
                headers = {
                    'Authorization': "Bearer " + access_token #request.session['openid_token']
                }

                get_user_info = requests.get(settings.AUTH_SERVER + '/protocol/openid-connect/userinfo', headers=headers, allow_redirects=False) #settings.AUTH_SERVER + 'userinfo'
                if get_user_info.status_code == 200:
                    user_info = get_user_info.json()
                    new_user, created = User.objects.get_or_create(username=user_info['voperson_id']) #User.objects.get_or_create(username=user_info['sub'])

                    # If new user or no user info exists
                    if created or not new_user.email:
                        new_user.password = '***'
                        new_user.email = user_info['email']
                        new_user.first_name = user_info['given_name']
                        new_user.last_name = user_info['family_name']
                        new_user.save()

                    # Send fillout form
                    user_form = UserForm(instance=new_user)
                    profile_form = ProfileForm()
                    msg = _('Preencha os campos adicionais e conclua o registo.')

                    # If UserProfile already exists but no affiliation info exists: send out fill form again
                    try:
                        new_userprofile = UserProfile.objects.get(user=new_user)
                        if new_userprofile.affiliation and new_userprofile.affiliation_country:
                            # Not a new user, all is filled out and activation is still pending
                            user_form = None
                            profile_form = None
                            msg = _('Registo pendente! Assim que o seu registo for validado com sucesso receberá um email da ativação da conta.')
                    except Exception:
                        pass

                    # Send to registered page
                    return render(request, 'front/login_egi.html', {'response': msg, 'user_form': user_form, 'profile_form': profile_form})

                else:
                    logger.debug('>> EGI userinfo request status not 200')
                    # Error logging through egi
                    logout(request)
                    return render(request, 'front/login_egi.html', {'response': msg})
        else:
            logger.debug('>> EGI no token')
            # Error logging through egi
            logout(request)
            return render(request, 'front/login_egi.html', {'response': msg})
    except Exception:
        logger.exception('Error on EGI login')
        # Error logging through egi
        logout(request)
        return render(request, 'front/login_egi.html', {'response': msg})


# Complete EGI Registration
def login_egi_complete(request):
    response = _('Ocorreu um erro! Por favor tente mais tarde.')

    if request.POST:
        formdata = request.POST
        # Get user to update fields
        try:
            user = User.objects.get(username=formdata['username'])
            user.first_name = formdata['first_name']
            user.last_name = formdata['last_name']
            user.save()

            try:
                # Try to update info if already exists
                userprofile = UserProfile.objects.get(user=user)
                userprofile.affiliation = formdata['affiliation']
                userprofile.affiliation_country = formdata['affiliation_country']
                userprofile.confirm_registration = True
                userprofile.save()
            except Exception:
                # UserProfile doesn't exist yet let's create it
                userprofile = UserProfile.objects.create(user=user, affiliation=formdata['affiliation'],
                                                         affiliation_country=formdata['affiliation_country'],
                                                         confirm_registration=True)

            response = _('Registo efetuado com sucesso! Assim que o seu registo for validado com sucesso receberá um email da ativação da conta.')

            # Send email notifying user's register on create user
            token = default_token_generator.make_token(user)
            uidb64 = urlsafe_base64_encode(force_bytes(user.pk))

            # Send user activation link to DEFAULT_FROM_EMAIL
            activation_link = request.build_absolute_uri(reverse('front:userprofile_activate', kwargs={'uidb64': uidb64, 'token': token}))
            notify_registration_egi(user, userprofile, activation_link)

        except Exception:
            logger.exception('Error on EGI login complete')
            response = _('Ocorreu um erro! Por favor tente mais tarde.')

    return JsonResponse({'msg': response})


# View for logout of EGI Check-in service
def logout_egi(request):
    # Logout OPENCoastS
    logout(request)
    return redirect('/home')


@login_required(login_url='/login/')
def userprofile_activate(request, uidb64, token):
    try:
        # Only power users can activate users
        if request.user.is_staff:
            try:
                new_user = User.objects.get(pk=urlsafe_base64_decode(uidb64))
                # Comment check of token validity - link can be used more than once
                #token_valid = default_token_generator.check_token(new_user, token)
                #if token_valid:
                if new_user:
                    request.session['activate_new_user'] = new_user.username
                    if new_user.is_active:
                        request.session['activate_msg'] = str(_('Este utilizador já foi ativado!'))

                    return redirect('/admin/front/userprofile/')
                else:
                    msg = _('Ocorreu um erro! Por favor tente mais tarde.')

            except Exception:
                logger.exception('Error rendering admin/deployments with userprofile activate - getting user')
                msg = _('Ocorreu um erro! Por favor tente mais tarde.')
        else:
            msg = _('Atenção: Não tem permissões para ativar utilizadores.')
    except Exception:
        logger.exception('Error rendering admin/deployments with userprofile activate')

    request.session['msg_str'] = str(msg)
    return redirect('/deployments')


@login_required(login_url='/login/')
def activate_user(request, username):
    try:
        # Only power users can activate users
        if request.user.is_staff:
            try:
                # Save user as active (signal on Users pre-save sends email notification)
                new_user = User.objects.get(username=username)
                new_user.is_active = True
                new_user.save()

                request.session['activate_new_user'] = username
                request.session['activate_msg'] = str(_('Utilizador ativado!'))

                return redirect('/admin/front/userprofile/')

            except Exception:
                logger.exception('Error activating userprofile - getting user')
                msg = _('Ocorreu um erro! Por favor tente mais tarde.')
        else:
            msg = _('Atenção: Não tem permissões para ativar utilizadores.')
    except Exception:
        logger.exception('Error activating userprofile')

    request.session['msg_str'] = str(msg)
    return redirect('/deployments')


@login_required(login_url='/login/')
def rate_service(request):
    # Get or Post satisfaction survey if user hasn't rated experience yet and has
    # submited at least one deploy
    msg, table = None, None

    if request.POST:
        ratingform = ProfileRatingForm(request.POST)

        if not ratingform.is_valid():
            msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        else:
            msg = _('Obrigado, a sua opinião é importante para nós.')
            userprofile = request.user.profile
            now = timezone.now()
            rate = ratingform.cleaned_data['value']
            text = ratingform.cleaned_data['text']
            UserProfileRating.objects.create(userprofile=userprofile, creation_datetime=now, value=rate, text=text)

        if 'ratings' in request.POST:
            # Render form
            ratingform = ProfileRatingForm()
            # Get previous ratings
            ratings = get_user_ratings(request.user.profile)

            return render(request, 'front/ratings.html', {
                'forms': [None, ratingform],
                'ratings': ratings,
                'msg': msg
            })
        else:
            return JsonResponse({'msg': msg})
    else:
        # Render form
        ratingform = ProfileRatingForm()
        # Get previous ratings
        ratings = get_user_ratings(request.user.profile)

        return render(request, 'front/ratings.html', {
            'forms': [None, ratingform],
            'ratings': ratings,
            'msg': msg
        })


@login_required(login_url='/login/')
def download_file(request, file_cat, file_id):
    content = None
    try:
        if file_cat == "deploy":
            file = DeploymentFile.objects.get(id=file_id)
            file_id = 'deploy_' + str(file.deployment.id)
        else:
            file = SessionFile.objects.get(id=file_id)

        content = bytes(file.file)
        file_type = file.file_type.name
        file_extension = file.file_type.format
    except Exception:
        logger.exception('Error downloading file')

    response = HttpResponse(content, content_type='text/plain')
    response['Content-Disposition'] = "attachment; filename=%s_%s.%s" % (file_type, file_id, file_extension)

    return response

import calendar
from datetime import date, timedelta
from django.db.models import Count, Sum, F
from django.http import Http404

from .models import Deployment

run_stats_group = 'run_stats'

@login_required(login_url='/login/')
def monthly_run_time(request, year, month):

    if not request.user.groups.filter(name=run_stats_group).exists():
        raise Http404("What are you talking about?")

    begin = date(int(year), int(month), day=1)
    _, period_days = calendar.monthrange(begin.year, begin.month)
    end = begin + timedelta(days=period_days)

    deployment_set = ( 
        Deployment.objects
            .filter( deploymentrun__run_datetime__range=(begin, end) )
            .only('id', 'user__userprofile__affiliation_country')
            .prefetch_related('user__userprofile')
            .order_by('id')
            .annotate(
                runs = Count('deploymentrun'),
                run_time = 
                       Sum( F('deploymentrun__end')-F('deploymentrun__begin') ),
            )
    )

    run_times = dict(
        # query = str(deployment_set.query),
        deployments = tuple( {
            'id': deployment.id,
            'country': deployment.user.userprofile.affiliation_country.code,
            'run_time': deployment.run_time,
            'run_count': deployment.runs
        } for deployment in deployment_set )
    )

    return JsonResponse(run_times, json_dumps_params=dict(indent=2) )
