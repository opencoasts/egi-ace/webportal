from django.db.models import (
    Model as dbModel, ForeignKey, OneToOneField, CASCADE, ManyToManyField,
    SlugField, CharField, TextField, BooleanField, IntegerField, FloatField,
    DateField, DateTimeField,
)
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.contrib.gis.db.models import PolygonField, LineStringField, PointField

from django_countries.fields import CountryField

from django.utils.translation import ugettext_lazy as _
from django.contrib.sessions.models import Session
from django.contrib.admin.templatetags.admin_list import _boolean_icon

import datamodel.models as dm_models

from datetime import datetime


# Global Constants (used in forms)
FIELDTYPE = (
    (6, 'Array'),
    (0, 'Boolean'),
    (5, 'Choice'),
    (1, 'Float'),
    (2, 'Integer'),
    (4, 'List'),
    (3, 'Text'),
)

FILETYPE = (
    ('param', "param.nml"),
    ('wwminput', "wwminput.nml"),
)


class UserProfile(dbModel):
    """
    Table - extend Django admin user to store more information about users
    """
    user = OneToOneField(User, on_delete=CASCADE)
    affiliation = CharField(max_length=100)
    affiliation_country = CountryField(blank_label=_('selecione país da afiliação'))
    confirm_registration = BooleanField(default=False, verbose_name="Registo confirmado")
    read_disclaimer = BooleanField(default=False, verbose_name="Termos e condições aceites")

    deploy_period = IntegerField(verbose_name="Duração Sistema", default=30, help_text="Em dias")
    deploy_maxnr = IntegerField(verbose_name="Máximo Sistemas ativos", default=1)

    station_maxnr = IntegerField(verbose_name="Máximo Estações", default=5)
    # TODO: nodes_maxnr is specific to SCHISM Model, consider extending UsrProfile to that model
    node_maxnr = IntegerField(verbose_name="Máximo Nós", default=120000)

    def __str__(self):
        return "%s %s %s" % (str(self.user.id), self.affiliation, self.user.username)

    def username(self):
        return self.user.username

    def email(self):
        return self.user.email

    def first_name(self):
        return self.user.first_name

    def last_name(self):
        return self.user.last_name

    def is_active(self):
        return _boolean_icon(self.user.is_active)

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class UserProfileRating(dbModel):
    """
    Table - store user's feedback about the service
    """
    creation_datetime = DateTimeField(default=datetime.now)
    value = IntegerField()
    text = TextField(blank=True, null=True)
    userprofile = ForeignKey(UserProfile, on_delete=CASCADE)

    def __str__(self):
        return "%s, %s, %s" % (self.userprofile, self.creation_datetime, self.value)


class CRS(dbModel):
    """
    Admin Table - references to Horizontal Coordinate Reference Systems    
    """
    code = IntegerField(unique=True, db_index=True, primary_key=True, auto_created=True)
    name = CharField(max_length=100)
    description = TextField(max_length=250, blank=True)
    active = BooleanField(default=True)

    def __str__(self):
        # Translating DB contents (need to add translations in templates)
        return str(_("EPSG:%s | %s" % (self.code, self.name)))

    class Meta:
        verbose_name = 'Detail CRS'
        verbose_name_plural = 'CRSs'


class CRSV(dbModel):
    """
    Admin Table - references to Vertical Coordinate Reference Systems    
    """
    name = CharField(max_length=100)
    description = TextField(max_length=250, blank=True)
    offset = FloatField(default=0.0)
    code = IntegerField(unique=True, null=True, blank=True, help_text=_('Código EPSG se aplicável'))
    active = BooleanField(default=True)

    def __str__(self):
        # Translating DB contents (need to add translations in templates)
        return str(_("%s | %.2fm" % (self.name, self.offset)))

    class Meta:
        verbose_name = 'Detail CRS Vertical'
        verbose_name_plural = 'CRSs Vertical'


class Station(dbModel):
    """
    Admin Table - references to Stations (for Data Model Comparison)
    """

    code = CharField(max_length=50)
    source = CharField(max_length=200)
    owner = CharField(max_length=100)
    name = CharField(max_length=100, blank=True)
    params = CharField(max_length=500, blank=True, help_text="Delimitador das Opções: ;")
    notes = CharField(max_length=100, blank=True)
    active = BooleanField(default=True)
    point = PointField(srid=4236, blank=True)

    def __str__(self):
        return "%s | %s" % (self.code, self.name)

    class Meta:
        verbose_name = 'Detail Station'
        verbose_name_plural = 'Stations'


class BoundaryKind(dbModel):
    """
    Admin Table - references to kinds of Boundaries
    """
    # Constants
    CATEGORY_ATMOSPHERE = 'atm'
    CATEGORY_WATER = 'wat'
    CATEGORY = (
        (CATEGORY_ATMOSPHERE, 'Atmospheric'),
        (CATEGORY_WATER, 'Water'),
    )

    name = CharField(max_length=100, blank=True)
    reference = SlugField(max_length=50, blank=True)
    description = TextField(blank=True)
    notes = TextField(blank=True)

    category = CharField(choices=CATEGORY, max_length=6)

    def __str__(self):
        return "%s" % self.name


class ModelVersion(dbModel):
    """
    Table - Based on ModelVersion in datamodel, limits specific Forecast Model Types use in the Wizard
    Also references the Boundary Kinds considered by the ModelVersion
    """
    public = BooleanField(default=True)

    parent = OneToOneField(dm_models.ModelVersion, related_name='child', on_delete=CASCADE)
    # datamodel = OneToOneField(dm_models.ModelVersion, related_name='front')

    boundarykind = ManyToManyField(BoundaryKind, blank=True)

    def parent_model(self):
        return self.parent.model

    def parent_version(self):
        return self.parent.version

    parent_model.short_description = 'Model'
    parent_version.short_description = 'Version'

    def __str__(self):
        return "%s, %s" % (self.parent.model, self.parent.version)

    class Meta:
        verbose_name = 'Detail Model Version'
        verbose_name_plural = 'Model Versions'


class ForcingSource(dbModel):
    """
    Admin Table - connects Forcing Sources with Boundary Kinds
    """

    parent = OneToOneField(dm_models.ForcingSource, on_delete=CASCADE)
    # datamodel = OneToOneField(dm_models.ForcingSource, related_name='front')

    forcing_kind = ForeignKey(BoundaryKind, on_delete=CASCADE)
    active = BooleanField(default=True)

    def parent_name(self):
        return self.parent.name

    def parent_reference(self):
        return self.parent.reference

    parent_name.short_description = 'Forcing Name'

    def __str__(self):
        return self.parent.name

    class Meta:
        verbose_name = 'Forcing Source'
        verbose_name_plural = 'Forcing Sources'


class AbstractForcingSource(dbModel):

    parent = OneToOneField(dm_models.ForcingSource, on_delete=CASCADE)
    active = BooleanField(default=True)

    def __str__(self):
        return self.parent.name

    class Meta:
        abstract = True


class ForcingSourceAtmosphere(AbstractForcingSource):

    has_air = BooleanField(default=False)
    has_radiation = BooleanField(default=False)


class ForcingSourceOcean(AbstractForcingSource):

    has_circulation = BooleanField(default=False)
    has_temp_salt = BooleanField(default=False)
    has_waves = BooleanField(default=False)


class AbstractParameter(dbModel):
    """
    Abstract Table - references all info needed to render parameters on the wizard app
    """
    ui_notes = CharField(max_length=120, blank=True)
    category = CharField(blank=True, max_length=250)
    order_by = CharField(max_length=10, blank=True, help_text="Campo que ordena ascendentemente parâmetros")

    # TODO: change to kind
    par_type = IntegerField(choices=FIELDTYPE)
    active = BooleanField(default=True, help_text="Campo ativo, é considerado na interface, mesmo que não esteja visível")
    predefined = BooleanField(default=True, help_text="Campo visível nos parâmetros predefinidos, se estiver ativo")
    readonly = BooleanField(default=True, help_text="Campo de leitura apenas")
    ruleset = JSONField(blank=True, null=True, help_text="Preencher com JSON")

    # TODO: in the future these attributes could be all set in the ruleset attribute
    choices = TextField(max_length=1000, blank=True, help_text="Preencher 'valor: Descrição' | Delimitador das Opções: quebra de linha | Exemplo: 1: Cartesian")
    min_value = FloatField(blank=True, null=True)
    max_value = FloatField(blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ['order_by']


class ModelParameter(AbstractParameter):
    parent = OneToOneField(dm_models.ModelParameter, on_delete=CASCADE)

    def model(self):
        return self.parent.model

    def name(self):
        return self.parent.parameter.name

    def description(self):
        return self.parent.description

    def notes(self):
        return self.parent.notes

    def default(self):
        return self.parent.default_value

    class Meta:
        verbose_name = 'Detail Model Parameter'
        verbose_name_plural = 'Model Parameters'


class ModelVersionParameter(AbstractParameter):
    parent = OneToOneField(dm_models.ModelVersionParameter, on_delete=CASCADE)

    def model_version(self):
        return self.parent.model_version

    def name(self):
        return self.parent.parameter.name

    def description(self):
        return self.parent.description

    def notes(self):
        return self.parent.notes

    def default(self):
        return self.parent.default_value

    class Meta:
        verbose_name = 'Detail ModelVersion Parameter'
        verbose_name_plural = 'ModelVersion Parameters'


class ModelVersionParameter3D(AbstractParameter):
    parent = OneToOneField(dm_models.ModelVersionParameter, on_delete=CASCADE)

    def model_version(self):
        return self.parent.model_version

    def name(self):
        return self.parent.parameter.name

    def description(self):
        return self.parent.description

    def notes(self):
        return self.parent.notes

    def default(self):
        return self.parent.default_value

    class Meta:
        verbose_name = 'Detail ModelVersion Parameter 3D'
        verbose_name_plural = 'ModelVersion Parameters 3D'


class ModelVersionParameterWave(AbstractParameter):
    parent = OneToOneField(dm_models.ModelVersionParameter, on_delete=CASCADE)
    file_type = CharField(choices=FILETYPE, max_length=20, default='param')

    def model_version(self):
        return self.parent.model_version

    def name(self):
        return self.parent.parameter.name

    def description(self):
        return self.parent.description

    def notes(self):
        return self.parent.notes

    def default(self):
        return self.parent.default_value

    class Meta:
        verbose_name = 'Detail ModelVersion Parameter Waves'
        verbose_name_plural = 'ModelVersion Parameters Waves'


class SessionFile(dm_models.FileBlobTrait):
    """
    Table - references to temporary files - connected to Session
    """
    session = ForeignKey(Session, on_delete=CASCADE)

    def __str__(self):
        return "%s %s %s (%sMB)" % (self.id, self.file_type, self.session, self.file_size_mb)


class Deployment(dm_models.Deployment):
    """
    Table - Extends Deployment in datamodel, information regarding Deployments specific to the Wizard App
    """

    # datamodel = OneToOneField(dm_models.Deployment, on_delete=CASCADE,
    #                                      parent_link=True, related_name='front')

    step = IntegerField(blank=True, null=True)
    step1 = JSONField(blank=True, null=True)
    step2 = JSONField(blank=True, null=True)
    step3 = JSONField(blank=True, null=True)
    step4 = JSONField(blank=True, null=True)
    step5 = JSONField(blank=True, null=True)
    step6 = JSONField(blank=True, null=True)
    step7 = JSONField(blank=True, null=True)
    step8 = JSONField(blank=True, null=True)

    config_json = JSONField(blank=True, null=True)

    # origin = ForeignKey('self', blank=True, null=True, related_name='clone')   # TODO do we want this?
    clone = IntegerField(blank=True, null=True)                                  # Or is this enough

    user = ForeignKey(User, on_delete=CASCADE)
    # author = ForeignKey(User)

    def __str__(self):
        return "ID:%s" % self.id

    class Meta:
        verbose_name = 'Detail Deployment'
        verbose_name_plural = 'Deployments'


class DeploymentBoundary(dm_models.DeploymentBoundary):
    """
    Table - connects Boundaries with Deployments
    """
    boundary_line = LineStringField(blank=True, null=True)

    boundary_kind = ForeignKey(BoundaryKind, on_delete=CASCADE)

    def __str__(self):
        return '%s, %s' % (self.name, self.boundary_kind)


class ExtendDeployRequest(dbModel):
    """
    Table - references and manages deploy extend run period requests from users
    """
    deployment = ForeignKey(Deployment, on_delete=CASCADE)
    creation_datetime = DateTimeField(default=datetime.now)
    motive = TextField()
    to_date = DateField()
    closed = BooleanField(default=False)
    accepted = BooleanField(default=False)
    reply = TextField(blank=True)
    reply_date = DateField(blank=True, null=True)

    def __str__(self):
        return '%s, %s' % (self.creation_datetime, self.motive)


class BBox(dbModel):
    """
    Admin Table - references to BoundingBox Types    
    """
    # Constants
    BOXTYPE_GLOBAL = 'global'
    BOXTYPE_OCEANIC = 'oceanic'
    BOXTYPE_ATMOSPHERE = 'atm'
    BOXTYPE_WAVES = 'waves'
    BOXTYPE = (
        (BOXTYPE_GLOBAL, _('Global')),
        (BOXTYPE_OCEANIC, _('Oceânica')),
        (BOXTYPE_ATMOSPHERE, _('Atmosférica')),
        (BOXTYPE_WAVES, _('Ondas')),
    )

    # TODO: change to kind
    bbox_type = CharField(choices=BOXTYPE, max_length=10)
    color = CharField(max_length=7, default="#333", help_text="Hex color")
    forcing_source = OneToOneField(ForcingSource, related_name='forcing', blank=True, null=True, on_delete=CASCADE)
    notes = CharField(max_length=100, blank=True)
    polygon = PolygonField()

    def __str__(self):
        return "%s | %s" % (self.bbox_type, self.forcing_source)

    class Meta:
        verbose_name = 'Detail Bounding Box'
        verbose_name_plural = 'Bounding Boxes'

'''
class Requirement(dbModel):
    name = CharField(max_length=50)
    description = TextField(max_length=250, blank=True)
    notes = CharField(max_length=120, blank=True)

    req_type = CharField(max_length=10)
    value = CharField(max_length=10)

    model = ForeignKey(Model, default=1, on_delete=CASCADE)

    def __str__(self):
        return "%s | %s | %s | %s" % (self.model, self.req_type, self.name, self.value)

    class Meta:
        verbose_name = 'Detail Requirement'
        verbose_name_plural = 'Requirements'
'''


'''
class ParameterCategory(dbModel):
    name = CharField(max_length=100)
    reference = SlugField(max_length=50)
    description = TextField(blank=True)
    notes = TextField(blank=True)

    def __str__(self):
        return self.name
'''
