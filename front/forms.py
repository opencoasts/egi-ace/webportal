from django import forms
from django.contrib.auth.models import User
from .models import UserProfile, BBox
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from front.models import Station
from django.contrib.gis.geos import Point, GEOSGeometry

RATE_TYPE = (
    (1, _('Mau')),
    (2, _('Fraco')),
    (3, _('Razoável')),
    (4, _('Bom')),
    (5, _('Excelente')),
)

from io import TextIOWrapper
from wizard.forecast_models.schism_v540 import read_hgrid, boundaries2polygon


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]

        labels = {
            "username": 'Email',
        }

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'password1',
            'password2',
        ]

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

        self.fields['username'].help_text = None
        self.fields['username'].label = 'Email (*)'
        self.fields['first_name'].label = _('Primeiro Nome (*)')
        self.fields['last_name'].label = _('Último Nome (*)')
        self.fields['password1'].help_text = None
        self.fields['password1'].label = _('Palavra-passe (*)')
        self.fields['password2'].label = _('Confirmação (*)')

    def clean_username(self):
        username = self.cleaned_data['username']

        if User.objects.exclude(pk=self.instance.pk).filter(username=username).exists():
            raise ValidationError(_('Este email já foi registado.'))

        try:
            validate_email(username)
        except:
            raise ValidationError(_('Endereço de email inválido.'))

        return username

    def clean_first_name(self):
        if self.cleaned_data["first_name"].strip() == '':
            raise ValidationError(_('Campo obrigatório.'))
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if self.cleaned_data["last_name"].strip() == '':
            raise ValidationError(_('Campo obrigatório.'))
        return self.cleaned_data["last_name"]


class UserForm(forms.ModelForm):
    class Meta:
        model = User

        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
        ]

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget = forms.HiddenInput()
        self.fields['email'].widget.attrs['readonly'] = True

    def clean_email(self):
        # Making sure field is unchanged on POST
        if self.instance:
            return self.instance.email
        else:
            return self.fields['email']


class ProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = [
            'affiliation',
            'affiliation_country'
        ]

        labels = {
            "affiliation": _("Afiliação (*)"),
            "affiliation_country": _("País (*)"),
        }

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)

        self.fields['affiliation'].help_text = _("Organização / Universidade")


class FullProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = [
            'affiliation',
            'affiliation_country',
            'deploy_period',
            'deploy_maxnr',
            'station_maxnr',
            'node_maxnr'
        ]

        labels = {
            "affiliation": _("Afiliação"),
            "affiliation_country": _("País"),
            "deploy_period": _("Duração de cada Sistema"),
            "deploy_maxnr": _("Limite de Sistemas Ativos"),
            "station_maxnr": _("Limite de Estações"),
            "node_maxnr": _("Limite de Nós na Malha"),
        }

    def __init__(self, *args, **kwargs):
        super(FullProfileForm, self).__init__(*args, **kwargs)
        self.fields['deploy_period'].widget.attrs['readonly'] = True
        self.fields['deploy_maxnr'].widget.attrs['readonly'] = True
        self.fields['station_maxnr'].widget.attrs['readonly'] = True
        self.fields['node_maxnr'].widget.attrs['readonly'] = True

    def clean_deploy_period(self):
        # Making sure field is unchanged on POST
        if self.instance:
            return self.instance.deploy_period
        else:
            return self.fields['deploy_period']

    def clean_deploy_maxnr(self):
        # Making sure field is unchanged on POST
        if self.instance:
            return self.instance.deploy_maxnr
        else:
            return self.fields['deploy_maxnr']

    def clean_station_maxnr(self):
        # Making sure field is unchanged on POST
        if self.instance:
            return self.instance.station_maxnr
        else:
            return self.fields['station_maxnr']

    def clean_node_maxnr(self):
        # Making sure field is unchanged on POST
        if self.instance:
            return self.instance.node_maxnr
        else:
            return self.fields['node_maxnr']


class StationForm(forms.ModelForm):
    code = forms.CharField(required=True, label=_('Código (*)'))
    source = forms.CharField(required=True, label=_('Fonte (*)'))
    owner = forms.CharField(required=True, label=_('Origem (*)'))
    name = forms.CharField(required=False, label=_('Nome'))
    latitude = forms.FloatField(min_value=-90, max_value=90, required=True, label=_('Latitude (*)'))
    longitude = forms.FloatField(min_value=-180, max_value=180, required=True, label=_('Longitude (*)'))
    notes = forms.CharField(required=False, label=_('Notas'))
    active = forms.BooleanField(widget=forms.CheckboxInput, initial=True, required=False)

    class Meta(object):
        model = Station
        exclude = []
        widgets = {'point': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        coordinates = self.initial.get('point', None)
        if isinstance(coordinates, Point):
            self.initial['longitude'], self.initial['latitude'] = coordinates.tuple

    def clean(self):
        data = super().clean()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        point = data.get('point')
        if latitude and longitude and not point:
            data['point'] = Point(longitude, latitude)

        return data


class ProfileRatingForm(forms.Form):
    value = forms.ChoiceField(choices=RATE_TYPE, label='', required=True, widget=forms.RadioSelect)
    text = forms.CharField(max_length=200, label=_('Deixe aqui o seu comentário'), required=False, widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        super(ProfileRatingForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget.attrs = {"class": 'long'}


class BBoxForm(forms.ModelForm):
    # Add custom file field Coordinates
    coords = forms.CharField(required=False, label=_('Ou introduza coordenadas manualmente'), help_text="xmin ymin, xmin ymax, xmax ymax, xmax ymin")

    # Add custom file field
    grid_file = forms.FileField(required=False, label=_('Ou selecione uma malha horizontal'))

    class Meta:
        model = BBox
        fields = ['bbox_type', 'color', 'forcing_source', 'notes', 'polygon', 'coords', 'grid_file']
        widgets = {
            'color': forms.TextInput(attrs={'type': 'color'},),
        }

    def clean(self):
        # If a grid file is uploaded we must parse it and use its boundary lines to form a polygon
        data = super().clean()
        grid_field = data.get('grid_file')
        if grid_field:
            grid_file = grid_field.file
            # Process grid file to Polygon
            file_lines = TextIOWrapper(grid_file.file, encoding='utf-8').readlines()
            file_ok, file_errors, xyz, ncon, bnd_segm, cpp_lat, dt = read_hgrid(file_lines)
            if file_ok:
                bnd_ok, bnd_pol = boundaries2polygon(bnd_segm)
                if bnd_ok:
                    data['polygon'] = bnd_pol
                else:
                    raise forms.ValidationError(_('Não foi possível gerar um polígono das fronteiras.'), code="grid_file")
            else:
                raise forms.ValidationError(_('Não foi possível carregar a malha horizontal.'), code="grid_file")
        else:
            coord_field = data.get('coords')
            if coord_field:
                try:
                    first_coord = coord_field.split(",")[0]
                    coord_str = "POLYGON(({}, {}))".format(coord_field, first_coord)
                    data['polygon'] = GEOSGeometry(coord_str, srid=4326)
                except Exception as e:
                    raise forms.ValidationError(_('Coordenadas inválidas.'), code="coords")

        return data
