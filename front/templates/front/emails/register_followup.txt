{% extends "__common__/emails/base.txt" %}
{% load i18n %}

{% block content %}

{% trans "Afiliação" %}
{{ affiliation }}

{% trans "País da Afiliação" %}
{{ affiliation_country }}

{% trans "Nome" %}
{{ first_name }} {{ last_name }}

Email
{{ email }}

{% trans "Ativar utilizador" %}
{{ link }}

{% endblock content %}
