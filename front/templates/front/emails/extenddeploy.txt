{% extends "__common__/emails/base.txt" %}
{% load i18n %}

{% block content %}

{% trans "PEDIDO DE EXTENSÃO EFETUADO COM SUCESSO! Assim que o seu pedido for validado receberá um email de resposta." %}

{% if user %}
{% trans "Utilizador" %}
{{ user }}
{% endif %}

{% trans "Sistema de Previsão" %}
{{ deploy_id }}

{% trans "Motivo" %}
{{ motive }}

{% trans "Período de Extensão" %}
{% trans "até" %} {{ to_date }}

{% if link %}
{% trans "Editar Registo" %}
{{ link }}
{% endif %}

{% endblock content %}
