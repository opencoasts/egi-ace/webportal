FROM python:3.6-buster

RUN set -ux \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        gettext \
        libgdal-dev \
        less nano \
        postgresql-client \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /_app
COPY requirements.txt .

RUN set -ux \
    && python -m venv /_venv \
    && . /_venv/bin/activate \
    && python -m pip install --upgrade pip \
    && python -m pip install --requirement requirements.txt \
    && python -m pip install GDAL==2.4.2
ENV PATH="/_venv/bin/:$PATH"

ARG user_id=0
ARG postgis_host
ARG postgis_port='5432'
ARG postgis_user='oc_user'
ARG postgis_password='password'
ARG postgis_database='opencoasts'
ARG wms_host=''
ARG wms_port='8080'
ARG wms_path='ncWMS'
ARG wms_did_template='deployment_{id}'
ARG deployments_path='/_deployments'

COPY . .

RUN set -ux \
    && test "$user_id" -gt 0 \
        && useradd --uid "$user_id" user; \
    mkdir -vp _logs _confs "$deployments_path" \
    \
    && ln -snvf ../_confs/django.py ProjOpenCoastS/local_settings.py \
    && ln -snvf ../_confs/django.json ProjOpenCoastS/local_settings.json \
    \
    && python -m ProjOpenCoastS.setup \
        -POSTGIS_HOST="$postgis_host" \
        -POSTGIS_PORT="$postgis_port" \
        -POSTGIS_USER="$postgis_user" \
        -POSTGIS_PASSWORD="$postgis_password" \
        -POSTGIS_DATABASE="$postgis_database" \
        -WMS_HOST="$wms_host" \
        -WMS_PORT="$wms_port" \
        -WMS_PATH="$wms_path" \
        -DEPLOYMENTS_PATH="$deployments_path" \
    \
    && python -m manage compilemessages \
    && python -m manage collectstatic --clear --link \
    \
    && test "$user_id" -gt 0 \
        && chown --from root --recursive "$user_id" _logs \
        || true

USER "$user_id"

VOLUME [ "/_app/_logs", "/_app/_confs", "$deployments_path" ]

ENTRYPOINT [ "gunicorn", \
    "--config=python:gunicorn_config", \
    "--bind=0.0.0.0:8000", \
    "ProjOpenCoastS.wsgi" ]

EXPOSE 8000
