from django.utils.translation import ugettext_lazy as _
from django.utils.text import format_lazy
from django_tables2 import tables
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.safestring import mark_safe
from django.urls import reverse

from .forms import F_TYPE
from .utils import FORCING_TYPE, WATER_FORCING_TYPE, SOURCE_FORCING_TYPE, DECAY_TYPE
import datamodel.models as dm_models

import datetime


def parse_fmodel(value):
    if not value:
        return ''

    out_value, out_string = value, ""
    months = [_('Jan'), _('Fev'), _('Mar'), _('Abr'), _('Mai'), _('Jun'), _('Jul'), _('Ago'), _('Set'), _('Out'),
              _('Nov'), _('Dez')]

    if not isinstance(out_value, dict):
        # Before Run_type
        if isinstance(out_value, list):
            arr = []
            for i, item in enumerate(months):
                arr.append('%s:%s' % (item, out_value[i]))
            out_value = mark_safe('[{}]<br>'.format(', '.join(arr)))
        else:
            out_value = mark_safe('{}<br>'.format(out_value))
    else:
        for key, val in out_value.items():
            # TODO: improve this, overriding string codes to PT and then apply local translation
            # isn't working, so we hard coded it
            # Translate key
            name = str(dict(FORCING_TYPE)[key])

            fmtype = val["type"]
            fmvalue = val["value"]

            if fmtype == "monthly":
                arr = []
                for i, item in enumerate(months):
                    arr.append('%s:%s' % (item, fmvalue[i]))
                out = '[{}]'.format(', '.join(arr))
            elif fmtype == "annual":
                out = '{}={}'.format(_('anual'), fmvalue)
            elif fmtype == "url" or fmtype == "string":
                out = fmvalue
            elif fmtype == "percent":
                out = '{}% {}'.format(fmvalue["percent"], fmvalue["bid"])

            if len(out_value.items()) > 1:
                # More than one show variable label
                out = '{}: {}'.format(name, out)

            out_string += '{}<br>'.format(out)

        out_value = mark_safe(out_string.replace('"', '').replace('{', '').replace('}', ''))

    return out_value

def parse_wq_fmodel(value):
    if not value:
        return ''

    out_value, out_string = value, ""
    months = [_('Jan'), _('Fev'), _('Mar'), _('Abr'), _('Mai'), _('Jun'), _('Jul'), _('Ago'), _('Set'), _('Out'),
              _('Nov'), _('Dez')]

    if not isinstance(out_value, dict):
        # Before Run_type
        if isinstance(out_value, list):
            arr = []
            for i, item in enumerate(months):
                arr.append('%s:%s' % (item, out_value[i]))
            out_value = mark_safe('[{}]<br>'.format(', '.join(arr)))
        else:
            out_value = mark_safe('{}<br>'.format(out_value))
    else:
        for key, val in out_value.items():
            # TODO: improve this, overriding string codes to PT and then apply local translation
            # isn't working, so we hard coded it
            # Translate key
            key = str(dict(WATER_FORCING_TYPE)[key])

            fmtype = val["type"]
            fmvalue = val["value"]

            if fmtype == "monthly":
                arr = []
                for i, item in enumerate(months):
                    if key == 'E-coli' or key == 'Enterococcus':
                        arr.append('%s:%s' % (item, str(float(fmvalue[i]))))
                    else:
                        arr.append('%s:%s' % (item, fmvalue[i]))
                out = '[{}]'.format(', '.join(arr))
            elif fmtype == "annual":
                if key == 'E-coli' or key == 'Enterococcus':
                    out = '{}={}'.format(_('anual'), str(float(fmvalue)))
                else:
                    out = '{}={}'.format(_('anual'), fmvalue)
            elif fmtype == "url" or fmtype == "string":
                out = fmvalue
            elif fmtype == "percent":
                out = '{}% {}'.format(fmvalue["percent"], fmvalue["bid"])

            out = '{}: {}'.format(key, out)

            out_string += '{}<br>'.format(out)

        out_value = mark_safe(out_string.replace('"', '').replace('{', '').replace('}', ''))

    return out_value

def parse_sources_fmodel(value):
    if not value:
        return ''

    out_value, out_string = value, ""
    months = [_('Jan'), _('Fev'), _('Mar'), _('Abr'), _('Mai'), _('Jun'), _('Jul'), _('Ago'), _('Set'), _('Out'),
              _('Nov'), _('Dez')]

    if not isinstance(out_value, dict):
        # Before Run_type
        if isinstance(out_value, list):
            arr = []
            for i, item in enumerate(months):
                arr.append('%s:%s' % (item, out_value[i]))
            out_value = mark_safe('[{}]<br>'.format(', '.join(arr)))
        else:
            out_value = mark_safe('{}<br>'.format(out_value))
    else:
        print("---- OUT VALUE -----")
        print(out_value.items())
        for key, val in out_value.items():
            # TODO: improve this, overriding string codes to PT and then apply local translation
            # isn't working, so we hard coded it
            # Translate key
            key = str(dict(SOURCE_FORCING_TYPE)[key])

            fmtype = val["type"]
            fmvalue = val["value"]

            print(fmvalue)
            print("key: " + key)

            if fmtype == "monthly":
                arr = []
                for i, item in enumerate(months):
                    if key == 'E-coli' or key == 'Enterococcus':
                        arr.append('%s:%s' % (item, str(float(fmvalue[i]))))
                    else:
                        arr.append('%s:%s' % (item, fmvalue[i]))
                out = '[{}]'.format(', '.join(arr))
            elif fmtype == "annual":
                if key == 'E-coli' or key == 'Enterococcus':
                    out = '{}={}'.format(_('anual'), str(float(fmvalue)))
                else:
                    print("fmvalue: " + fmvalue)
                    out = '{}={}'.format(_('anual'), fmvalue)
            elif fmtype == "url" or fmtype == "string":
                out = fmvalue
            elif fmtype == "percent":
                out = '{}% {}'.format(fmvalue["percent"], fmvalue["bid"])

            out = '{}: {}'.format(key, out)

            out_string += '{}<br>'.format(out)

        out_value = mark_safe(out_string.replace('"', '').replace('{', '').replace('}', ''))

    return out_value

def parse_choice(value):
    if not value:
        return ''

    out_value = value

    if value in dict(DECAY_TYPE):
        out_value = str(dict(DECAY_TYPE)[value])

        return out_value
    else:
        return value

class GridTableSummary(tables.Table):
    filename = tables.columns.TemplateColumn(template_code="{{ record.filename }}", orderable=False, verbose_name=_('Ficheiro'))
    epsg = tables.columns.TemplateColumn(template_code="{{ record.epsg }}", orderable=False, verbose_name=_('EPSG'))
    rvert = tables.columns.TemplateColumn(template_code="{{ record.rvert }}", orderable=False, verbose_name=_('Ref. Vert.'))
    elems = tables.columns.TemplateColumn(template_code="{{ record.elems }}", orderable=False, verbose_name=_('Elementos'))
    nodes = tables.columns.TemplateColumn(template_code="{{ record.nodes }}", orderable=False, verbose_name=_('Nós'))
    boundaries = tables.columns.TemplateColumn(template_code="{{ record.boundaries }}""", orderable=False, verbose_name=_('Fronteiras'))

    class Meta:
        attrs = {'class': 'table simple-table'}
        empty_text = _('Sem registos')

    def render_rvert(self, value):
        out = '0.00m'
        if value:
            out = '%.2fm' % value
        return out


class ForcingsTable(tables.Table):
    html_str0 = """
            <input class='selBID' type='checkbox' name="ftypename" data-bid='{{ record.bid }}' onclick='event.stopPropagation();' />
        """

    chk = tables.columns.TemplateColumn(template_code=html_str0, orderable=False, verbose_name='')
    bid = tables.columns.TemplateColumn(template_code="{{ record.bid }}", orderable=False, verbose_name=_('ID'))
    ftype = tables.columns.TemplateColumn(template_code="{{ record.ftype }}", orderable=False, verbose_name=_('Tipo'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'id': 'forcings_table', 'class': 'table table-hover'}

    def render_ftype(self, value):
        if not value:
            return ''
        return str(dict(F_TYPE)[value])

    def render_fmodel(self, value):
        return parse_fmodel(value)

class WaterForcingsTable(tables.Table):
    html_str0 = """
            <input class='selBID' type='checkbox' name="ftypename" data-bid='{{ record.bid }}' onclick='event.stopPropagation();' />
        """

    chk = tables.columns.TemplateColumn(template_code=html_str0, orderable=False, verbose_name='')
    bid = tables.columns.TemplateColumn(template_code="{{ record.bid }}", orderable=False, verbose_name=_('ID'))
    ftype = tables.columns.TemplateColumn(template_code="{{ record.ftype }}", orderable=False, verbose_name=_('Tipo'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'id': 'forcings_table', 'class': 'table table-hover'}

    def render_ftype(self, value):
        if not value:
            return ''
        return str(dict(F_TYPE)[value])

    def render_fmodel(self, value):
        return parse_wq_fmodel(value)

class ForcingsTableSummary(tables.Table):
    bid = tables.columns.TemplateColumn(template_code="{{ record.bid }}", orderable=False, verbose_name=_('Fronteira'))
    ftype = tables.columns.TemplateColumn(template_code="{{ record.ftype }}", orderable=False, verbose_name=_('Tipo'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'class': 'table simple-table wrap-table'}
        empty_text = _('Sem registos')

    def render_ftype(self, value):
        if not value:
            return ''
        return str(dict(F_TYPE)[value])

    def render_fmodel(self, value):
        return parse_fmodel(value)

class WaterForcingsTableSummary(tables.Table):
    bid = tables.columns.TemplateColumn(template_code="{{ record.bid }}", orderable=False, verbose_name=_('Fronteira'))
    ftype = tables.columns.TemplateColumn(template_code="{{ record.ftype }}", orderable=False, verbose_name=_('Tipo'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'class': 'table simple-table wrap-table'}
        empty_text = _('Sem registos')

    def render_ftype(self, value):
        if not value:
            return ''
        return str(dict(F_TYPE)[value])

    def render_fmodel(self, value):
        return parse_wq_fmodel(value)

class StationsTable(tables.Table):
    html_str0 = """
                <input class='selSID' type='checkbox' data-code='{{ record.code }}' data-type='{{ record.type }}' {% if record.chk %}checked{% endif %} onclick='event.stopPropagation();' onchange='toggleMarkers(this);' />
            """

    chk = tables.columns.TemplateColumn(template_code=html_str0, orderable=False, verbose_name='')
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Nome'))
    lat = tables.columns.TemplateColumn(template_code="{{ record.lat }}", orderable=False, verbose_name=_('Latitude'))
    lon = tables.columns.TemplateColumn(template_code="{{ record.lon }}", orderable=False, verbose_name=_('Longitude'))
    cstation = tables.columns.TemplateColumn(template_code="{{ record.cstation }}", orderable=False, verbose_name=_('Comparação'))

    class Meta:
        attrs = {'id': 'stations_table', 'class': 'table table-hover'}

class SourcesTable(tables.Table):
    html_str0 = """
                <input class='selSID' type='checkbox' data-code='{{ record.code }}' {% if record.chk %}checked{% endif %} onclick='event.stopPropagation();' onchange='toggleMarkers(this);' />
            """

    chk = tables.columns.TemplateColumn(template_code=html_str0, orderable=False, verbose_name='')
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Nome'))
    lat = tables.columns.TemplateColumn(template_code="{{ record.lat }}", orderable=False, verbose_name=_('Latitude'))
    lon = tables.columns.TemplateColumn(template_code="{{ record.lon }}", orderable=False, verbose_name=_('Longitude'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'id': 'sources_table', 'class': 'table table-hover'}

    def render_ftype(self, value):
        if not value:
            return ''
        return str(dict(F_TYPE)[value])

    def render_fmodel(self, value):
        return parse_sources_fmodel(value)

class StationsTableSummary(tables.Table):
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Nome'))
    lat = tables.columns.TemplateColumn(template_code="{{ record.lat }}", orderable=False, verbose_name=_('Latitude'))
    lon = tables.columns.TemplateColumn(template_code="{{ record.lon }}", orderable=False, verbose_name=_('Longitude'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'class': 'table simple-table wrap-table'}
        empty_text = _('Sem registos')

class SourcesTableSummary(tables.Table):
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Nome'))
    lat = tables.columns.TemplateColumn(template_code="{{ record.lat }}", orderable=False, verbose_name=_('Latitude'))
    lon = tables.columns.TemplateColumn(template_code="{{ record.lon }}", orderable=False, verbose_name=_('Longitude'))
    fmodel = tables.columns.TemplateColumn(template_code="{{ record.fmodel }}", orderable=False, verbose_name=_('Forçamento'))

    class Meta:
        attrs = {'class': 'table simple-table wrap-table'}
        empty_text = _('Sem registos')

    def render_ftype(self, value):
        if not value:
            return ''
        return str(dict(F_TYPE)[value])

    def render_fmodel(self, value):
        return parse_sources_fmodel(value)

class ParametersTable(tables.Table):
    category = tables.columns.TemplateColumn(template_code="{{ record.category }}", orderable=False, verbose_name=_('Categoria'))
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Parâmetro'))
    description = tables.columns.TemplateColumn(template_code="{{ record.description }}", orderable=False, verbose_name=_('Descrição'))
    default = tables.columns.TemplateColumn(template_code="{{ record.default }}", orderable=False, verbose_name=_('Valor'))

    class Meta:
        attrs = {'id': 'pardefault_table', 'class': 'table', 'hidden': ''}
        empty_text = _('Sem registos')

    def __init__(self, *args, **kwargs):
        self.values = kwargs.pop('values', None)
        super(ParametersTable, self).__init__(*args, **kwargs)

    def render_description(self, value):
        if not value:
            return ''
        return value

    def render_category(self, value):
        if not value:
            return ''
        return value

    def render_default(self, value, record):
        if record.name in self.values:
            value = self.values[record.name]

        strchoices = record.choices
        if strchoices:
            choices = [(c.split(": ")[0], c.split(": ")[1].replace(";", "")) for c in strchoices.split("\r\n")]
            value = str(value) + " | " + str(dict(choices)[str(value)])

        return value


class ParametersTableSummary(tables.Table):
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Parâmetro customizado *'))
    value = tables.columns.TemplateColumn(template_code="{{ record.value }}", orderable=False, verbose_name=_('Valor'))

    class Meta:
        attrs = {'class': 'table simple-table'}
        empty_text = _('Sem registos')


class ParametersTable_file(tables.Table):
    comment = tables.columns.TemplateColumn(template_code="{{ record.comment }}", orderable=False, verbose_name=_(''))
    row = tables.columns.TemplateColumn(template_code="{{ record.row }}", orderable=False, verbose_name='')

    class Meta:
        attrs = {'id': 'pardefault_table', 'class': 'table', 'hidden': ''}
        empty_text = _('Sem registos')


class WWMParametersTable_file(tables.Table):
    comment = tables.columns.TemplateColumn(template_code="{{ record.comment }}", orderable=False, verbose_name=_(''))
    row = tables.columns.TemplateColumn(template_code="{{ record.row }}", orderable=False, verbose_name='')

    class Meta:
        attrs = {'id': 'wwmdefault_table', 'class': 'table', 'hidden': ''}
        empty_text = _('Sem registos')


class AuxFilesTableSummary(tables.Table):
    name = tables.columns.TemplateColumn(template_code="{{ record.name }}", orderable=False, verbose_name=_('Categoria'))
    choice = tables.columns.TemplateColumn(template_code="{{ record.choice }}", orderable=False, verbose_name=_('Input'))
    fileaction = tables.columns.TemplateColumn(template_code="{{ record.fileaction }}", orderable=False, verbose_name=_('Ficheiro'))

    class Meta:
        attrs = {'class': 'table simple-table'}
        empty_text = _('Sem registos')

    def render_fileaction(self, value, record):
        html_str = ""
        if value and "deploy_id" in record and not isinstance(record['choice'], dict):
            deploy_id = record["deploy_id"]
            href_view = self.request.build_absolute_uri(reverse('front:get_file', kwargs={'name': value, 'deploy_id': deploy_id, 'action': 'open', 'format': 'll'}))
            href_download = self.request.build_absolute_uri(reverse('front:get_file', kwargs={'name': value, 'deploy_id': deploy_id, 'action': 'download', 'format': 'll'}))

            html_str = '<a target="_blank" class="btn btn-default" href="{0}" title="{1}"><span class="glyphicon glyphicon-eye-open"></span></a>'.format(
                href_view, _("Abrir"))
            html_str += '<a class="btn btn-default" href="{0}" title="{1}"><span class="glyphicon glyphicon-download"></span></a>'.format(
                href_download, _("Descarregar"))

        return mark_safe(html_str)

    def render_choice(self, value, record):
        if isinstance(record['choice'], dict):
            html_str = "<p></p>"
            if(record['name'] == 'Tracer'):
                html_str += "<p>{0}={1}</p>".format('tracer', str(float(record['choice']['tracer'])))
            else:
                for key, value in record['choice'].items():
                    html_str += "<p>{0}={1}</p>".format(key, str(float(value)))

            return mark_safe(html_str)
        else:
            return parse_choice(value)


class DeploymentPrintTable(tables.Table):
    description = tables.columns.TemplateColumn(template_code="{{ record.description }}", orderable=False, verbose_name=_('Identificação'))
    deploy_dates = tables.columns.TemplateColumn(template_code="{{ record.creation_datetime }}", orderable=False, verbose_name=_('Datas'))
    step = tables.columns.TemplateColumn(template_code="{{ record.step }}", orderable=False, verbose_name=_('Estado'))

    class Meta:
        attrs = {'class': 'table table-responsive'}
        empty_text = _('Sem registos')

    def render_description(self, value, record):
        html_str = "<span class='label label-info'>ID:" + str(record.id) + "</span>"
        html_str += "<br/><strong>" + str(_('Nome')) + ":</strong> " + record.name
        if value:
            html_str += "<br/>" + value

        return mark_safe(html_str)

    def render_deploy_dates(self, value, record):
        html_str = "<strong>" + str(_('Criado a')) + ":</strong> " + date_format(record.creation_datetime, format='SHORT_DATETIME_FORMAT')
        if record.begin_date:
            html_str += "<br/><strong>" + str(_('Início')) + ":</strong> " + date_format(record.begin_date, format='SHORT_DATE_FORMAT')
        if record.end_date:
            html_str += "<br/><strong>" + str(_('Fim')) + ":</strong> " + date_format(record.end_date, format='SHORT_DATE_FORMAT')
        if record.last_run_date:
            html_str += "<br/><strong>" + str(_('Última corrida')) + ":</strong> " + date_format(record.last_run_date, format='SHORT_DATE_FORMAT')

        return mark_safe(html_str)

    def render_step(self, value, record):
        if not record.active:
            if value < 5:
                return str(format_lazy('{} {}', _('Passo'), value+1))
            elif value == 5 or (value == 6 and not record.begin_date):
                return str(_('Configurado'))
            else:
                return str(_('Desativado'))
        else:
            # Alerts and buttons access rendering
            # Get model version run period
            runperiodhours = dm_models.ModelVersionRunPeriod.objects.get(id=record.step1['run_period_pk']).run_period
            today = timezone.now().date()
            enddate = record.end_date

            # Class 'basic-btns' disables some row actions client-side
            if enddate < today:
                return str(_('Expirado'))
            elif enddate <= today + datetime.timedelta(hours=runperiodhours):
                return str(_('A expirar'))
            elif enddate <= today + datetime.timedelta(days=5):  # TODO - configurar dias para alertas do lado da base de dados para tornar isto dinâmico e escolha do utilizador
                return str(_('A expirar'))
            else:
                return str(_('Ativo'))

        return value


class ExtendDeploysTable(tables.Table):
    extenddeploy = tables.columns.TemplateColumn(template_code="{{ record.deployment }}", orderable=False, verbose_name=_('Sistema de Previsão'))

    class Meta:
        attrs = {'class': 'table simple-table'}
        empty_text = _('Sem registos')

    def render_extenddeploy(self, value, record):
        creation_datetime = date_format(record.creation_datetime, format='SHORT_DATE_FORMAT') if record.creation_datetime else ""
        to_date = date_format(record.to_date, format='SHORT_DATE_FORMAT') if record.to_date else ""
        reply_date = date_format(record.reply_date, format='SHORT_DATE_FORMAT') if record.reply_date else ""

        html_str = "<span class='label label-info'>" + str(record.deployment) + "</span> "
        if record.closed:
            if record.accepted:
                html_str += "<span class='label label-success'>" + str(_('Aceite')) + "</span>"
            else:
                html_str += "<span class='label label-default'>" + str(_('Rejeitado')) + "</span>"
        else:
            html_str += "<span class='label label-warning'>" + str(_('Pendente')) + "</span>"

        html_str += "<br/><strong>" + str(_('Pedido a')) + "</strong> " + creation_datetime
        html_str += "<br/><strong>" + str(_('Estender até')) + "</strong> " + to_date
        html_str += "<br/><strong>" + str(_('Motivo')) + "</strong> " + record.motive

        if record.reply:
            html_str += "<br/><strong>" + str(_('Resposta')) + "</strong> " + reply_date
            html_str += "<br/>" + record.reply

        return mark_safe(html_str)
