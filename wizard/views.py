import os
from django.forms.widgets import DateTimeBaseInput

from django.shortcuts import render, redirect, HttpResponse
from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.utils.text import format_lazy
from django.core.serializers import serialize

from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.forms import formset_factory

from .forms import *
from .tables import *
from .utils import *
from front.forms import ProfileRatingForm
from front.definitions import run_type_map as RUN_TYPE_MAP
from front.definitions import kind_type_map as KIND_TYPE_MAP

from .forecast_models import schism_v540 as schism

from datetime import datetime

from front.decorators import deploy_permission
from front.utils import notify_extend_deploy

from f90nml import Parser

import wiff.opencoasts.models as wiff_models

app_name = 'wizard'


@login_required(login_url='/login/')
def step_model(request):
    template, step, last_step, msg_str = templates[0]['path'], templates[0]['step'], 0, None
    step_forms, disclaimer = [], None
    step_name = 'step' + str(step)

    if 'last_step' in request.session:
        last_step = request.session['last_step']

    baroclinic_type = 'basic2d'
    waves_type = 'nowaves'
    wq_type = 'nowqtype'

    if request.method == 'POST':
        model = ModelForm(request.POST, user=request.user)

        if step_name not in request.session:
            if 'baroclinic_type' in model.data:
                baroclinic_type = model.data['baroclinic_type']

            if 'waves_type' in model.data:
                waves_type = model.data['waves_type']

            if 'wq_type' in model.data:
                wq_type = model.data['wq_type']

            # Save model selection in session key
            request.session[step_name] = {'model_pk': str(model.data['model']), 'run_period_pk': model.data['run_period'], 'baroclinic_type': baroclinic_type, 'waves_type': waves_type, 'wq_type': wq_type}

        # Create wizard key for new deployment
        # New wizard - save a unique key as session key
        if not 'wizard_key' in request.session:
            time_ref = datetime.strftime(timezone.now(), '%Y-%m-%d-%Hh%Mm%S_%fs')
            request.session['wizard_key'] = f'u{request.user.id}_{time_ref}'

        # All went well - update LAST STEP
        if not last_step:
            request.session['last_step'] = step

        # Goto step2
        return redirect(templates[step]['name'])
    else:
        # Pass request
        if step_name in request.session:
            if 'baroclinic_type' in request.session[step_name]:
                baroclinic_type = request.session[step_name]['baroclinic_type']

            if 'waves_type' in request.session[step_name]:
                waves_type = request.session[step_name]['waves_type']

            if 'wq_type' in request.session[step_name]:
                wq_type = request.session[step_name]['wq_type']

            model = ModelForm(initial={'model': request.session[step_name]['model_pk'],
                                       'run_period': request.session[step_name]['run_period_pk'],
                                       'baroclinic_type': baroclinic_type, 'waves_type': waves_type, 'wq_type': wq_type}, user=request.user)
        else:
            model = ModelForm(user=request.user)

        step_forms.append(model)

    # Add disclaimer info if user hasn't accepted terms
    if not request.user.profile.read_disclaimer:
        disclaimer = True

    # Just passing a message
    if 'msg_str' in request.session:
        msg_str = request.session['msg_str']
        request.session.pop('msg_str')

    return render(request, template, {
        'step': step,
        'msg_str': msg_str,
        'forms': step_forms,
        'disclaimer': disclaimer,
        'default_email': settings.DEFAULT_FROM_EMAIL,
        'baroclinic_type': baroclinic_type,
        'waves_type': waves_type,
        'wq_type': wq_type,
        'app': app_name,
    })


@login_required(login_url='/login/')
def step_domain(request):
    template, step, last_step, msg_str = templates[1]['path'], templates[1]['step'], 0, None
    step_forms, errors, msg = [], [], None
    step_name = 'step' + str(step)
    grid_info, grid_table, bbox, bbox_types, boundaries, boundary_types, bathymetry = None, None, [], [], [], [], []

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])

    # Return Global Bounding Box only
    # Commented on 26/09/2018 - Global BBox no longer used
    #bbox_queryset = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL)
    #if bbox_queryset:
    #    bbox = serialize('geojson', bbox_queryset)
    #    bbox_types = [{'type': item.bbox_type, 'label': str(dict(front_models.BBox.BOXTYPE)[item.bbox_type]), 'color': item.color}
    #                  for item in bbox_queryset]

    max_nodes = request.user.profile.node_maxnr
    max_bytes_size = calc_maxfilesize(max_nodes)

    baroclinic_type = 'basic2d'
    waves_type = 'nowaves'
    if 'baroclinic_type' in request.session['step1']:
        baroclinic_type = request.session['step1']['baroclinic_type']

    if 'waves_type' in request.session['step1']:
        waves_type = request.session['step1']['waves_type']

    if step_name in request.session:
        boundaries = request.session[step_name]['boundaries']['geojson']
        boundary_types = request.session[step_name]['boundaries']['legend']
        if 'bathymetry' in request.session[step_name]:
            bathymetry = request.session[step_name]['bathymetry']

        selcrs = request.session[step_name]['crs']
        try:
            selcrs = front_models.CRS.objects.get(pk=selcrs).name
        except Exception:
            pass

        # Validate if Boundaries Extent is inside Global BBox and Open boundaries is inside Oceanic BBox
        # Removed restriction on 26/09/2018
        #boundariesinbbox, msg = validate_bboxes(boundaries, selcrs)
        #if not boundariesinbbox:
        #    errors.append(msg)

        # Validate if Outer Boundaries are closed
        outerboundariesclosed, msg = validate_outerboundaries(boundaries)
        if not outerboundariesclosed:
            errors.append(msg)

        if 'grid_info' in request.session[step_name]:
            grid_info = request.session[step_name]['grid_info']

    else:
        data = DataInputForm(baroclinic_type=baroclinic_type, waves_type=waves_type)
        step_forms.append(data)

    if grid_info:
        grid_table = GridTableSummary([grid_info])

    # Just passing a message
    if 'msg_str' in request.session:
        msg_str = request.session['msg_str']
        request.session.pop('msg_str')

    return render(request, template, {
        'step': step,
        'msg_str': msg_str,
        'forms': step_forms,
        'errors': errors,
        'grid_info': grid_table,
        'bbox': bbox,
        'bbox_types': bbox_types,
        'boundaries': boundaries,
        'boundary_types': boundary_types,
        'bathymetry': bathymetry,
        'max_nodes': max_nodes,
        'max_bytes_size': round(max_bytes_size / (1024 * 1024)),
        'baroclinic_type': baroclinic_type,
        'waves_type': waves_type,
        'app': app_name,
    })


@login_required(login_url='/login/')
def step_front(request):
    template, step, last_step, msg_str = templates[2]['path'], templates[2]['step'], 0, None
    step_forms, errors, bbox, bbox_types, boundaries, boundary_types, forcings_table, forcings = [], [], [], [], [], [], None, []
    step_name = 'step' + str(step)

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])
    else:
        last_step = request.session['last_step']

        # Not quite at this step yet - return to step immediately next to last step
        if last_step < step - 1:
            return redirect(templates[last_step]['name'])

        runperiod_pk = request.session['step1']['run_period_pk']

        # Get simulation run type
        baroclinic_type = 'basic2d'
        waves_type = 'nowaves'
        if 'baroclinic_type' in request.session['step1']:
            baroclinic_type = request.session['step1']['baroclinic_type']

        if 'waves_type' in request.session['step1']:
            waves_type = request.session['step1']['waves_type']

        # Return Active Oceanic and Atmosphere Forcing's Bounding Boxes only
        if 'yes' in waves_type:
            bbox_queryset = front_models.BBox.objects.filter(bbox_type__in=[front_models.BBox.BOXTYPE_OCEANIC,
                                                                            front_models.BBox.BOXTYPE_ATMOSPHERE,
                                                                            front_models.BBox.BOXTYPE_WAVES]).filter(forcing_source__active=True)
        else:
            bbox_queryset = front_models.BBox.objects.filter(bbox_type__in=[front_models.BBox.BOXTYPE_OCEANIC,
                                                                        front_models.BBox.BOXTYPE_ATMOSPHERE]).filter(forcing_source__active=True)
        if bbox_queryset:
            # TODO: stabilize this workaround - condition models to selected runperiod
            try:
                runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=runperiod_pk)
                if runperiod.run_period == 72:
                    bbox_queryset = bbox_queryset.exclude(forcing_source__parent__reference='prism2017')
            except Exception:
                pass

            bbox = serialize('geojson', bbox_queryset)
            bbox_types = [
                {'forcing': item.forcing_source.pk, 'label': item.forcing_source.parent_reference(), 'color': item.color} for item in bbox_queryset]

        # Get only Open Boundaries
        bids = []
        boundaries_all = json.loads(request.session['step2']['boundaries']['geojson'])
        for obj in boundaries_all['features']:
            if obj['properties']['type'] == 'Open':
                bids.append(obj['properties']['code'])
                boundaries.append(obj)
        boundary_types = [{'type': 'Open', 'label': 'Open', 'color': 'blue'}]

        # Commented on 26/09/2018 - Team decided to remove this restriction
        #if len(boundaries) == 0:
        #    errors.append(_('Não existem fronteiras abertas!'))

        if request.method == 'POST':
            # Preparing Atm Model form
            atmmodelsChoice = ForcingAtmModelForm(request.POST, baroclinic_type=baroclinic_type)
            step_forms.append(atmmodelsChoice)

            if len(boundaries) > 0:
                # Preparing Ocean Model form
                oceanmodelsChoice = ForcingOceanModelForm(request.POST, runperiod_pk=runperiod_pk, baroclinic_type=baroclinic_type, waves_type=waves_type)
                step_forms.append(oceanmodelsChoice)

                # Preparing Forcing type choice form
                tableChoice = ForcingChoiceForm(request.POST, baroclinic_type=baroclinic_type, bids=bids)
                step_forms.append(tableChoice)

            # Validate form to move to next step
            valid, msg = form_validation(request, step)
            if valid:
                # All went well - update LAST STEP
                if last_step < step:
                    request.session['last_step'] = step
                # Goto next step
                return redirect(templates[step]['name'])
            else:
                if last_step == step:
                    # Conditions are not met anymore - update step
                    request.session['last_step'] = step - 1
                    last_step = step - 1

                errors.append(msg)

                # Fill default open boundaries
                for obj in boundaries:
                    forcings.append({'bid': obj['properties']['code']})
        else:
            if step_name in request.session:
                if 'forcings' in request.session[step_name]:
                    forcings = request.session[step_name]['forcings']

                else:
                    # Fill default open boundaries
                    for obj in boundaries:
                        forcings.append({'bid': obj['properties']['code']})

                # Preparing Atm Model form
                initial_atm = {}
                if 'atmmodel' in request.session[step_name]:
                    initial_atm["atmmodel"] = request.session[step_name]['atmmodel']
                atmmodelsChoice = ForcingAtmModelForm(initial=initial_atm)
                step_forms.append(atmmodelsChoice)

                if len(boundaries) > 0:
                    initial_ocean = {}
                    if 'oceanmodel' in request.session[step_name]:
                        initial_ocean["oceanmodel"] = request.session[step_name]['oceanmodel']

                    if 'oceanmodel_temp_salt' in request.session[step_name]:
                        initial_ocean["oceanmodel_temp_salt"] = request.session[step_name]['oceanmodel_temp_salt']

                    if 'oceanmodel_waves' in request.session[step_name]:
                        initial_ocean["oceanmodel_waves"] = request.session[step_name]['oceanmodel_waves']

                    oceanmodelsChoice = ForcingOceanModelForm(initial=initial_ocean,
                                                                  runperiod_pk=runperiod_pk,
                                                                  baroclinic_type=baroclinic_type, waves_type=waves_type)
                    step_forms.append(oceanmodelsChoice)

            else:
                # Preparing Atm Model form
                atmmodelsChoice = ForcingAtmModelForm(baroclinic_type=baroclinic_type)
                step_forms.append(atmmodelsChoice)

                if len(boundaries) > 0:
                    # Preparing Ocean Model form
                    oceanmodelsChoice = ForcingOceanModelForm(runperiod_pk=runperiod_pk, baroclinic_type=baroclinic_type, waves_type=waves_type)
                    step_forms.append(oceanmodelsChoice)

                # Fill default open boundaries
                for obj in boundaries:
                    forcings.append({'bid': obj['properties']['code']})

            if len(boundaries) > 0:
                # Preparing Forcing type choice form
                tableChoice = ForcingChoiceForm(baroclinic_type=baroclinic_type, bids=bids)
                step_forms.append(tableChoice)

        if len(boundaries) > 0:
            forcings_table = ForcingsTable(forcings)

        # Just passing a message
        if 'msg_str' in request.session:
            msg_str = request.session['msg_str']
            request.session.pop('msg_str')

        return render(request, template, {
            'step': step,
            'msg_str': msg_str,
            'forms': step_forms,
            'errors': errors,
            'bbox': bbox,
            'bbox_types': bbox_types,
            'boundaries': boundaries,
            'boundary_types': boundary_types,
            'forcings_table': forcings_table,
            'baroclinic_type': baroclinic_type,
            'waves_type': waves_type,
            'app': app_name,
        })


@login_required(login_url='/login/')
def step_stations(request):
    template, step, last_step, msg_str = templates[3]['path'], templates[3]['step'], 0, None
    step_forms, errors, bbox, bbox_types, boundaries, boundary_types = [], [], [], [], [], []
    step_name = 'step' + str(step)
    stations, station_types, stations_table, shell_coords, islands_coords, flagok = None, None, None, None, None, True

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])
    else:
        last_step = request.session['last_step']

        # Not quite at this step yet - return to step immediately next to last step
        if last_step < step - 1:
            return redirect(templates[last_step]['name'])

        # Return Global Bounding Box only
        # Commented on 26/09/2018 - Global BBox no longer used
        #bbox_queryset = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL)
        #if bbox_queryset:
        #    bbox = serialize('geojson', bbox_queryset)
        #    bbox_types = [{'type': item.bbox_type, 'label': str(dict(front_models.BBox.BOXTYPE)[item.bbox_type]),
        #                   'color': item.color}
        #                  for item in bbox_queryset]

        # Return Boundaries
        boundaries = request.session['step2']['boundaries']['geojson']
        boundary_types = request.session['step2']['boundaries']['legend']

        # Get simulation run type
        baroclinic_type = 'basic2d'
        waves_type = 'nowaves'
        if 'baroclinic_type' in request.session['step1']:
            baroclinic_type = request.session['step1']['baroclinic_type']

        if 'waves_type' in request.session['step1']:
            waves_type = request.session['step1']['waves_type']

        if not boundaries:
            errors.append(_('Não existem fronteiras!'))
        else:
            try:
                # Stations Legend
                station_types = [{'type': 'O', 'label': str(dict(STATION_TYPE)['O']), 'color': 'rgb(0,102,255)'},
                                 {'type': 'C', 'label': str(dict(STATION_TYPE)['C']), 'color': 'rgb(0,0,0,0)'},
                                 {'type': 'V', 'label': str(dict(STATION_TYPE)['V']), 'color': 'rgb(204,51,255)'}]

                step_stations_items = None
                if step_name in request.session:
                    # Build Stations Table from Session
                    step_stations_items = request.session[step_name]['stations']

                if request.method == 'POST':
                    valid, msg = form_validation(request, step)
                    if valid:
                        # All went well - update LAST STEP
                        if last_step < step:
                            request.session['last_step'] = step
                        # Goto next step
                        return redirect(templates[step]['name'])
                    else:
                        errors.append(msg)

                flagok, stations, cstations, shell_coords, islands_coords, stations_items = build_stations(boundaries, step_stations_items, baroclinic_type, waves_type)
                
                if not flagok:
                    errors.append(str(_('Ocorreu um erro! Por favor tente mais tarde.')))

            except Exception:
                logger.exception('Error rendering step stations')
                errors.append(str(_('Ocorreu um erro! Por favor tente mais tarde.')))

        # Build new station form
        step_forms.append(StationForm(stations=cstations))

        # Build stations table
        stations_table = StationsTable(stations_items)

        # Just passing a message
        if 'msg_str' in request.session:
            msg_str = request.session['msg_str']
            request.session.pop('msg_str')

        # Maximum number of stations user can add to model, recommended max distance between comparison stations in meters
        max_stations = request.user.profile.station_maxnr
        rec_distance = 100

        return render(request, template, {
            'step': step,
            'msg_str': msg_str,
            'forms': step_forms,
            'errors': errors,
            'bbox': bbox,
            'bbox_types': bbox_types,
            'boundaries': boundaries,
            'boundary_types': boundary_types,
            'stations': stations,
            'station_types': station_types,
            'stations_table': stations_table,
            'bbox_poly': shell_coords,
            'bbox_islands': islands_coords,
            'max_stations': max_stations,
            'rec_distance': rec_distance,
            'app': app_name,
        })


@login_required(login_url='/login/')
def step_param(request):
    template, step, last_step, msg_str = templates[4]['path'], templates[4]['step'], 0, None
    step_forms, step_forms_wwm, errors, pardefault_table, wwmdefault_table = [], [], [], None, None
    flag_check = True
    file_upload_allowed = False

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])
    else:
        last_step = request.session['last_step']

        # Not quite at this step yet - return to step immediately next to last step
        if last_step < step - 1:
            return redirect(templates[last_step]['name'])

        sel_model_version = request.session['step1']['model_pk']

        # Get simulation run type
        baroclinic_type = request.session['step1'].get('baroclinic_type') or 'basic2d'
        waves_type = request.session['step1'].get('waves_type') or 'nowaves'
        wq_type = request.session['step1'].get('wq_type') or 'nowqtype'
 
        if request.method == 'POST':
            valid, msg = form_validation(request, step)
            if valid:
                # All went well - update LAST STEP
                if last_step < step:
                    request.session['last_step'] = step
                # Goto next step
                return redirect(templates[step]['name'])
            else:
                errors.append(msg)

        params_wwm, formChoice_wwm, _calc_dt = None, None, None

        if 'step5' in request.session:
            params = request.session['step5']['params']
            formChoice = ParChoiceForm(initial={'parchoice': request.session['step5']['choice']}, baroclinic_type=baroclinic_type)

            if 'yes' in waves_type:
                params_wwm = request.session['step5']['wwm']['params']
                formChoice_wwm = WWMChoiceForm(initial={'wwmchoice': request.session['step5']['wwm']['choice']})
            if 'C' in request.session['step5']['choice']:
                flag_check = False
        else:
            params = request.session['step2']['params']

            formChoice = ParChoiceForm(baroclinic_type=baroclinic_type)

            if 'yes' in waves_type:
                formChoice_wwm = WWMChoiceForm()

            # If DT was calculated on step 2, open ParChoiceForm with 'Customized' as default
            #if 'dt' in request.session['step2']['params']:
                #print(" -------------- Step5 não existe e dt existe ------------")
                #formChoice = ParChoiceForm(initial={'parchoice': 'C'}, baroclinic_type=baroclinic_type)

                # edited by plopes
                #_calc_dt = True
                #if 'yes' in waves_type:
                    #formChoice_wwm = WWMChoiceForm(initial={'wwmchoice': 'C'})
            #else:
                #print(" -------------- Step5 não existe e dt não existe ------------")
                #formChoice = ParChoiceForm(baroclinic_type=baroclinic_type)

                #if 'yes' in waves_type:
                    #formChoice_wwm = WWMChoiceForm()

        run_kind = dict(
            baroclinic_type = baroclinic_type,
            waves_type = waves_type,
            wq_type = wq_type,
        )

        parFields = ParFormset(**run_kind, sel_model_version=sel_model_version, prefix="cst", values=params, _calc_dt=_calc_dt)

        step_forms.append(formChoice)
        step_forms.append(parFields)

        if 'yes' in waves_type:
            #gets dt calculated value
            _dt = parFields['dt'].value()
            wwmFields = ParFormset(**run_kind, sel_model_version=sel_model_version, prefix="cst_wwm", values=params_wwm, file_type="wwminput", dt_val=_dt)

            step_forms_wwm.append(formChoice_wwm)
            step_forms_wwm.append(wwmFields)

        # TODO: in a near future we allow param.in upload and parse
        if file_upload_allowed:
            if 'step5' in request.session:
                parchoiceU = ParUploadForm(initial={'file': request.session['step5']['file']}, prefix="cst")

                if 'yes' in waves_type:
                    wwmchoiceU = ParUploadForm(initial={'file': request.session['step5']['wwm']['file']}, prefix="cst_wwm")
            else:
                parchoiceU = ParUploadForm(prefix="cst")

                if 'yes' in waves_type:
                    wwmchoiceU = ParUploadForm(prefix="cst_wwm")
            step_forms.append(parchoiceU)

        # Just passing a message
        if 'msg_str' in request.session:
            msg_str = request.session['msg_str']
            request.session.pop('msg_str')

        return render(request, template, {
            'step': step,
            'msg_str': msg_str,
            'forms': step_forms,
            'forms_wwm': step_forms_wwm,
            'errors': errors,
            'table': pardefault_table,
            'table_wwm': wwmdefault_table,
            'file_upload_allowed': file_upload_allowed,
            'baroclinic_type': baroclinic_type,
            'waves_type': waves_type,
            'flag_check': flag_check,            
            'app': app_name,
        })


@login_required(login_url='/login/')
def step_auxfiles(request):
    template, step, last_step, msg_str = templates[5]['path'], templates[5]['step'], 0, None
    step_formset, errors, auxfiles_items = [], [], None

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])
    else:
        last_step = request.session['last_step']

        # Not quite at this step yet - return to step immediately next to last step
        if last_step < step - 1:
            return redirect(templates[last_step]['name'])

        # Get simulation run type
        baroclinic_type = 'basic2d'
        wq_type = 'nowqtype'
        if 'baroclinic_type' in request.session['step1']:
            baroclinic_type = request.session['step1']['baroclinic_type']

        if 'wq_type' in request.session['step1']:
            wq_type = request.session['step1']['wq_type']

        # TODO: In the future get Auxiliary file list from Requirements or FileType Tables
        # If readonly is added to options fields are not editable
        if "3d" in baroclinic_type:
            if "generic" in wq_type:
                auxfilesset = [
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.002, 'name': 'Drag', 'label': _('Coeficiente de atrito [-]'), 'options': {'constant_type': 'float'}},
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.06, 'name': 'Albedo', 'label': _('Albedo [-]'), 'options': {'constant_type': 'float', 'min_value': 0.00, 'max_value': 1.00}},
                    {'choice': 'C', 'hidden': 'N', 'constant': 7, 'name': 'Watertype', 'label': format_lazy('{} - {} 1 {} 7', _('Tipo de água'), _('inteiro entre'), _('e')), 'options': {'constant_type': 'integer', 'min_value': 1, 'max_value': 7}},
                    {'choice': 'C', 'hidden': 'N', 'name': 'Temperature', 'label': format_lazy('{} - {}', _('Temperatura [ºC]'), _('Condições iniciais')), 'options': {'constant_type': 'float', 'min_value': 0.0, 'max_value': 40.0}},
                    {'choice': 'C', 'hidden': 'N', 'name': 'Salinity', 'label': format_lazy('{} - {}', _('Salinidade [PSU]'), _('Condições iniciais')), 'options': {'constant_type': 'float', 'min_value': 0.0, 'max_value': 42.0}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 1e-6, 'name': 'Diffmin', 'label': _('Difusividade mínima [m2/s]'), 'options': {'readonly': True}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 1e-2, 'name': 'Diffmax', 'label': _('Difusividade máxima [m2/s]'), 'options': {'readonly': True}},
                    {'choice': 'D', 'hidden': 'N', 'constant': None, 'value': '', 'name': 'Tracer', 'label':_('Equação de decaimento para Traçador Genérico [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'InitTracer', 'label':_('Condições iniciais para Traçador Genérico [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 0, 'name': 'InitDummy_hidden', 'label':'', 'options': {'readonly': True}}
                ]
            elif "fecal" in wq_type:
                auxfilesset = [
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.002, 'name': 'Drag', 'label': _('Coeficiente de atrito [-]'), 'options': {'constant_type': 'float'}},
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.06, 'name': 'Albedo', 'label': _('Albedo [-]'), 'options': {'constant_type': 'float', 'min_value': 0.00, 'max_value': 1.00}},
                    {'choice': 'C', 'hidden': 'N', 'constant': 7, 'name': 'Watertype', 'label': format_lazy('{} - {} 1 {} 7', _('Tipo de água'), _('inteiro entre'), _('e')), 'options': {'constant_type': 'integer', 'min_value': 1, 'max_value': 7}},
                    {'choice': 'C', 'hidden': 'N', 'name': 'Temperature', 'label': format_lazy('{} - {}', _('Temperatura [ºC]'), _('Condições iniciais')), 'options': {'constant_type': 'float', 'min_value': 0.0, 'max_value': 40.0}},
                    {'choice': 'C', 'hidden': 'N', 'name': 'Salinity', 'label': format_lazy('{} - {}', _('Salinidade [PSU]'), _('Condições iniciais')), 'options': {'constant_type': 'float', 'min_value': 0.0, 'max_value': 42.0}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 1e-6, 'name': 'Diffmin', 'label': _('Difusividade mínima vertical [m2/s]'), 'options': {'readonly': True}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 1e-2, 'name': 'Diffmax', 'label': _('Difusividade máxima vertical [m2/s]'), 'options': {'readonly': True}},
                    {'choice': 'D', 'hidden': 'N', 'constant': None, 'value': '', 'name': 'E-coliEnterococcus', 'label': _('Equação de decaimento [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'Fib', 'label':_('Fração de agregação de FIB a sedimentos [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'SedimentationFib', 'label':_('Taxa de sedimentação da agregação de FIB a sedimentos [m/s]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'InitE-coli', 'label':_('Condições iniciais para E-coli [UFC/100ml ou MPN/100ml]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'InitEnterococcus', 'label':_('Condições iniciais para Enterococcus [UFC/100ml ou MPN/100ml]'), 'options': {'constant_type': 'float', 'min_value': 0}}
                ]
            else:
                auxfilesset = [
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.002, 'name': 'Drag', 'label': _('Coeficiente de atrito [-]'), 'options': {'constant_type': 'float'}},
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.06, 'name': 'Albedo', 'label': _('Albedo [-]'), 'options': {'constant_type': 'float', 'min_value': 0.00, 'max_value': 1.00}},
                    {'choice': 'C', 'hidden': 'N', 'constant': 7, 'name': 'Watertype', 'label': format_lazy('{} - {} 1 {} 7', _('Tipo de água'), _('inteiro entre'), _('e')), 'options': {'constant_type': 'integer', 'min_value': 1, 'max_value': 7}},
                    {'choice': 'C', 'hidden': 'N', 'name': 'Temperature', 'label': format_lazy('{} - {}', _('Temperatura [ºC]'), _('Condições iniciais')), 'options': {'constant_type': 'float', 'min_value': 0.0, 'max_value': 40.0}},
                    {'choice': 'C', 'hidden': 'N', 'name': 'Salinity', 'label': format_lazy('{} - {}', _('Salinidade [PSU]'), _('Condições iniciais')), 'options': {'constant_type': 'float', 'min_value': 0.0, 'max_value': 42.0}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 1e-6, 'name': 'Diffmin', 'label': _('Difusividade mínima vertical [m2/s]'), 'options': {'readonly': True}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 1e-2, 'name': 'Diffmax', 'label': _('Difusividade máxima vertical [m2/s]'), 'options': {'readonly': True}},
                ]
        else:
            if "fecal" in wq_type:
                auxfilesset = [
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.025, 'name': 'Manning', 'label': _('Coeficiente de Manning [m1/3/s]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'D', 'hidden': 'N', 'constant': None, 'value': '', 'name': 'E-coliEnterococcus', 'label': _('Equação de decaimento [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'Fib', 'label':_('Fração de agregação de FIB a sedimentos [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'SedimentationFib', 'label':_('Taxa de sedimentação da agregação de FIB a sedimentos [m/s]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'InitE-coli', 'label':_('Condições iniciais para E-coli [UFC/100ml ou MPN/100ml]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'InitEnterococcus', 'label':_('Condições iniciais para Enterococcus [UFC/100ml ou MPN/100ml]'), 'options': {'constant_type': 'float', 'min_value': 0}}
                ]
            elif "generic" in wq_type:
                auxfilesset = [
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.025, 'name': 'Manning', 'label': _('Coeficiente de Manning [m1/3/s]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'D', 'hidden': 'N', 'constant': None, 'value': '', 'name': 'Tracer', 'label':_('Equação de decaimento para Traçador Genérico [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'Fib', 'label':_('Fração de agregação de FIB a sedimentos [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'SedimentationFib', 'label':_('Taxa de sedimentação da agregação de FIB a sedimentos [m/s]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'N', 'constant': None, 'name': 'InitTracer', 'label':_('Condições iniciais para Traçador Genérico [-]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                    {'choice': 'C', 'hidden': 'Y', 'constant': 0, 'name': 'InitDummy_hidden', 'label':'', 'options': {'readonly': True}}
                ]
            else:
                auxfilesset = [
                    {'choice': 'C', 'hidden': 'N', 'constant': 0.025, 'name': 'Manning', 'label': _('Coeficiente de Manning [m1/3/s]'), 'options': {'constant_type': 'float', 'min_value': 0}},
                ]

        if request.method == 'POST':
            valid, msg = form_validation(request, step)
            if valid:
                # All went well - update LAST STEP
                if last_step < step:
                    request.session['last_step'] = step
                # Goto next step
                return redirect(templates[step]['name'])
            else:
                errors.append(msg)

        if 'step6' in request.session:
            # Update forms list with user's saved options
            auxfilesset_updated = []
            for item in auxfilesset:
                # Searching for item in request
                match = next((obj for obj in request.session['step6']['auxfilesset'] if obj['name'] == item['name']))
                if(match['name'] == 'InitE-coli' or match['name'] == 'InitEnterococcus'):
                    value = match['constant']
                    new_value = float(value)
                    new_value_string = str(new_value)
                    match['constant'] = new_value_string
                # Adding default options
                if 'options' in item:
                    match['options'] = item['options']
                auxfilesset_updated.append(match)
            auxfilesset = auxfilesset_updated

        auxfilesFormSet = formset_factory(AuxFilesFormset, max_num=len(auxfilesset))

        # Translate labels before creating form
        for item in auxfilesset:
            item['label'] = _(item['label'])

        step_formset = auxfilesFormSet(initial=auxfilesset)

        # Just passing a message
        if 'msg_str' in request.session:
            msg_str = request.session['msg_str']
            request.session.pop('msg_str')

        return render(request, template, {
            'step': step,
            'msg_str': msg_str,
            'forms': step_formset,
            'auxfilesset': auxfilesset,
            'errors': errors,
            'baroclinic_type': baroclinic_type,
            'wq_type': wq_type,
            'app': app_name,
        })

@login_required(login_url='/login/')
def step_wquality(request):
    template, step, last_step, msg_str = templates[6]['path'], templates[6]['step'], 0, None
    step_forms, form_source, errors, bbox, bbox_types, boundaries, boundary_types, bathymetry, forcings_table, forcings, ftypes = [], [], [], [], [], [], [], [], None, [], []
    step_name = 'step' + str(step)
    step_two, step_three, step_continue = False, False, False
    sources, sources_table, shell_coords, islands_coords, flagok = None, None, None, None, True
    max_sources, rec_distance, ws_model = None, None, None

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])
    else:
        last_step = request.session['last_step']

        # Not quite at this step yet - return to step immediately next to last step
        if last_step < step - 1:
            return redirect(templates[last_step]['name'])

        runperiod_pk = request.session['step1']['run_period_pk']

        # Get simulation run type
        baroclinic_type = 'basic2d'
        waves_type = 'nowaves'
        wq_type = 'nowqtype'
        if 'baroclinic_type' in request.session['step1']:
            baroclinic_type = request.session['step1']['baroclinic_type']

        if 'waves_type' in request.session['step1']:
            waves_type = request.session['step1']['waves_type']

        if 'wq_type' in request.session['step1']:
            wq_type = request.session['step1']['wq_type']

        if "no" in wq_type:
            return redirect(templates[7]['name'])

        else:
            if 'forcings' in request.session['step3']:
                for i in range(0, len(request.session['step3']['forcings'])):
                    ftypes += [[request.session['step3']['forcings'][i]['bid'], request.session['step3']['forcings'][i]['ftype']]]

            if step_name in request.session:
                if 'ws_model' in request.session[step_name]:
                    ws_model = request.session[step_name]['ws_model']

                if ws_model and "no" not in ws_model:
                    step_two = True

                    boundaries = request.session['step2']['boundaries']['geojson']
                    boundary_types = request.session['step2']['boundaries']['legend']

                    if not boundaries:
                        errors.append(_('Não existem fronteiras!'))
                    else:
                        try:

                            step_sources_items = None
                            if 'sources' in request.session[step_name]:
                                # Build Stations Table from Session
                                step_sources_items = request.session[step_name]['sources']

                            if request.method == 'POST':
                                valid, msg = form_validation(request, step)
                                if valid:
                                    # All went well - update LAST STEP
                                    if last_step < step:
                                        request.session['last_step'] = step
                                    # Goto next step
                                    return redirect(templates[step]['name'])
                                else:
                                    errors.append(msg)

                            flagok, sources, csources, shell_coords, islands_coords, sources_items = build_sources(boundaries, step_sources_items, baroclinic_type, waves_type)
                            
                            if not flagok:
                                errors.append(str(_('Ocorreu um erro! Por favor tente mais tarde.')))

                        except Exception:
                            logger.exception('Error rendering step stations')
                            errors.append(str(_('Ocorreu um erro! Por favor tente mais tarde.')))

                        # Build new station form
                        step_forms.append(SourcesForm(sources=csources, baroclinic_type=baroclinic_type, wq_type=wq_type))
                        form_source.append(SourcesForcingChoiceForm(baroclinic_type=baroclinic_type, wq_type=wq_type))

                        # Build stations table
                        sources_table = SourcesTable(sources_items)
                        if len(sources_items) > 0:
                            step_continue = True

                        # Maximum number of stations user can add to model, recommended max distance between comparison stations in meters
                        max_sources = request.user.profile.station_maxnr
                        rec_distance = 100
                else:
                    if last_step < step:
                        request.session['last_step'] = step               
                        # Goto next step
                        return redirect(templates[step]['name'])
                    else:
                        if 'yes' in waves_type:
                            bbox_queryset = front_models.BBox.objects.filter(bbox_type__in=[front_models.BBox.BOXTYPE_OCEANIC,
                                                                                front_models.BBox.BOXTYPE_ATMOSPHERE,
                                                                                front_models.BBox.BOXTYPE_WAVES]).filter(forcing_source__active=True)
                        else:
                            bbox_queryset = front_models.BBox.objects.filter(bbox_type__in=[front_models.BBox.BOXTYPE_OCEANIC,
                                                                            front_models.BBox.BOXTYPE_ATMOSPHERE]).filter(forcing_source__active=True)
                        if bbox_queryset:
                            # TODO: stabilize this workaround - condition models to selected runperiod
                            try:
                                runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=runperiod_pk)
                                if runperiod.run_period == 72:
                                    bbox_queryset = bbox_queryset.exclude(forcing_source__parent__reference='prism2017')
                            except Exception:
                                pass

                            bbox = serialize('geojson', bbox_queryset)
                            bbox_types = [
                                {'forcing': item.forcing_source.pk, 'label': item.forcing_source.parent_reference(), 'color': item.color} for item in bbox_queryset]

                        # Get only Open Boundaries
                        bids = []
                        boundaries_all = json.loads(request.session['step2']['boundaries']['geojson'])
                        for obj in boundaries_all['features']:
                            if obj['properties']['type'] == 'Open':
                                bids.append(obj['properties']['code'])
                                boundaries.append(obj)
                        boundary_types = [{'type': 'Open', 'label': 'Open', 'color': 'blue'}]

                        # Commented on 26/09/2018 - Team decided to remove this restriction
                        #if len(boundaries) == 0:
                        #    errors.append(_('Não existem fronteiras abertas!'))

                        if request.method == 'POST':
                            # Preparing Atm Model form
                            wsmodelsChoice = WaterSourcesModelForm(request.POST, baroclinic_type=baroclinic_type, wq_type=wq_type)
                            step_forms.append(wsmodelsChoice)

                            #if len(boundaries) > 0:
                            # Preparing Ocean Model form
                            #oceanmodelsChoice = ForcingOceanModelForm(request.POST, runperiod_pk=runperiod_pk, baroclinic_type=baroclinic_type, waves_type=waves_type)
                            #step_forms.append(oceanmodelsChoice)

                            # Preparing Forcing type choice form
                            #tableChoice = WaterForcingChoiceForm(request.POST, baroclinic_type=baroclinic_type, wq_type=wq_type, bids=bids)
                            #step_forms.append(tableChoice)

                            #   Validate form to move to next step
                    
                            valid, msg = form_validation(request, step)
                            if valid:
                                # All went well - update LAST STEP
                                if last_step < step:
                                    request.session['last_step'] = step               
                                # Goto next step
                                return redirect(templates[step]['name'])
                            else:
                                if last_step == step:
                                    # Conditions are not met anymore - update step
                                    request.session['last_step'] = step - 1
                                    last_step = step - 1

                                errors.append(msg)
                        else:
                            if 'wq_forcings' in request.session[step_name]:
                                forcings = request.session[step_name]['wq_forcings']
                            else:
                                # Fill default open boundaries
                                for obj in boundaries:
                                    forcings.append({'bid': obj['properties']['code']})

                            # Preparing Atm Model form
                            initial_wsm = {}
                            if 'ws_model' in request.session[step_name]:
                                ws_model = request.session[step_name]['ws_model']
                                initial_wsm["wsmodel"] = request.session[step_name]['ws_model']
                            wsmodelsChoice = WaterSourcesModelForm(initial=initial_wsm)
                            step_forms.append(wsmodelsChoice)    

                        if len(boundaries) > 0:
                            # Preparing Forcing type choice form
                            tableChoice = WaterForcingChoiceForm(request.POST, baroclinic_type=baroclinic_type, wq_type=wq_type, bids=bids)
                            step_forms.append(tableChoice)

                    if len(boundaries) > 0:
                        step_continue = True
                        forcings_table = WaterForcingsTable(forcings)
            else:
                if 'yes' in waves_type:
                    bbox_queryset = front_models.BBox.objects.filter(bbox_type__in=[front_models.BBox.BOXTYPE_OCEANIC,
                                                                                front_models.BBox.BOXTYPE_ATMOSPHERE,
                                                                                front_models.BBox.BOXTYPE_WAVES]).filter(forcing_source__active=True)
                else:
                    bbox_queryset = front_models.BBox.objects.filter(bbox_type__in=[front_models.BBox.BOXTYPE_OCEANIC,
                                                                            front_models.BBox.BOXTYPE_ATMOSPHERE]).filter(forcing_source__active=True)
                if bbox_queryset:
                    # TODO: stabilize this workaround - condition models to selected runperiod
                    try:
                        runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=runperiod_pk)
                        if runperiod.run_period == 72:
                            bbox_queryset = bbox_queryset.exclude(forcing_source__parent__reference='prism2017')
                    except Exception:
                        pass

                    bbox = serialize('geojson', bbox_queryset)
                    bbox_types = [
                        {'forcing': item.forcing_source.pk, 'label': item.forcing_source.parent_reference(), 'color': item.color} for item in bbox_queryset]

                # Get only Open Boundaries
                bids = []
                boundaries_all = json.loads(request.session['step2']['boundaries']['geojson'])
                for obj in boundaries_all['features']:
                    if obj['properties']['type'] == 'Open':
                        bids.append(obj['properties']['code'])
                        boundaries.append(obj)
                boundary_types = [{'type': 'Open', 'label': 'Open', 'color': 'blue'}]

                # Commented on 26/09/2018 - Team decided to remove this restriction
                #if len(boundaries) == 0:
                #    errors.append(_('Não existem fronteiras abertas!'))

                if request.method == 'POST':
                    # Preparing Atm Model form
                    wsmodelsChoice = WaterSourcesModelForm(request.POST, baroclinic_type=baroclinic_type, wq_type=wq_type)
                    step_forms.append(wsmodelsChoice)

                    #if len(boundaries) > 0:
                        # Preparing Ocean Model form
                        #oceanmodelsChoice = ForcingOceanModelForm(request.POST, runperiod_pk=runperiod_pk, baroclinic_type=baroclinic_type, waves_type=waves_type)
                        #step_forms.append(oceanmodelsChoice)

                        # Preparing Forcing type choice form
                        #tableChoice = WaterForcingChoiceForm(request.POST, baroclinic_type=baroclinic_type, wq_type=wq_type, bids=bids)
                        #step_forms.append(tableChoice)

                    # Validate form to move to next step
                    
                    valid, msg = form_validation(request, step)
                    if valid:
                        # All went well - update LAST STEP
                        if last_step < step:
                            request.session['last_step'] = step               
                        # Goto next step
                        return redirect(templates[step]['name'])
                    else:
                        if last_step == step:
                            # Conditions are not met anymore - update step
                            request.session['last_step'] = step - 1
                            last_step = step - 1

                        errors.append(msg)
                else:
                    if step_name in request.session:
                        if 'wq_forcings' in request.session[step_name]:
                            forcings = request.session[step_name]['wq_forcings']
                        else:
                            # Fill default open boundaries
                            for obj in boundaries:
                                forcings.append({'bid': obj['properties']['code']})


                        # Preparing Atm Model form
                        initial_wsm = {}
                        if 'ws_model' in request.session[step_name]:
                            ws_model = request.session[step_name]['ws_model']
                            initial_wsm["wsmodel"] = request.session[step_name]['ws_model']
                        wsmodelsChoice = WaterSourcesModelForm(initial=initial_wsm)
                        step_forms.append(wsmodelsChoice)    
                    else:
                        # Preparing Atm Model form
                        wsmodelsChoice = WaterSourcesModelForm(baroclinic_type=baroclinic_type, wq_type=wq_type)
                        step_forms.append(wsmodelsChoice)

                        # Fill default open boundaries
                        for obj in boundaries:
                            forcings.append({'bid': obj['properties']['code']})

                        for i in range(0, len(forcings)):
                            forcings[i]['ftype'] = ftypes[i][1]

                    if len(boundaries) > 0:
                        # Preparing Forcing type choice form
                        tableChoice = WaterForcingChoiceForm(request.POST, baroclinic_type=baroclinic_type, wq_type=wq_type, bids=bids)
                        step_forms.append(tableChoice)

                if len(boundaries) > 0:
                    forcings_table = WaterForcingsTable(forcings)

            # Just passing a message
            if 'msg_str' in request.session:
                msg_str = request.session['msg_str']
                request.session.pop('msg_str')

            return render(request, template, {
                'step': step,
                'msg_str': msg_str,
                'forms': step_forms,
                'form_source': form_source,
                'errors': errors,
                'bbox': bbox,
                'bbox_types': bbox_types,
                'boundaries': boundaries,
                'boundary_types': boundary_types,
                'bathymetry': bathymetry,
                'forcings_table': forcings_table,
                'baroclinic_type': baroclinic_type,
                'waves_type': waves_type,
                'wq_type': wq_type,
                "ftypes": ftypes,
                "flag": step_two,
                "flag_continue": step_continue,
                'sources': sources,
                'sources_table': sources_table,
                'bbox_poly': shell_coords,
                'bbox_islands': islands_coords,
                'ws_model': ws_model,
                'max_sources': max_sources,
                'rec_distance': rec_distance,
                'app': app_name,
            })

@login_required(login_url='/login/')
def step_deploy(request):
    template, step, last_step, msg_str = templates[7]['path'], templates[7]['step'], 0, None
    step_forms, errors, msg = [], [], None

    # If no steps - redirect
    if 'last_step' not in request.session:
        return redirect(templates[0]['name'])
    else:
        last_step = request.session['last_step']

        # Not quite at this step yet - return to step immediately next to last step
        if 'wq_type' in request.session['step1']:
            wq_type = request.session['step1'].get('wq_type')
        if last_step < step - 1:
            if "no" not in wq_type: 
                print("--- VERIFICAR ISTO ----")
                #return redirect(templates[last_step]['name'])

        if request.method == 'POST':
            deployform = DeployForm(request.POST)

            if deployform.is_valid():
                name = deployform.cleaned_data['name']
                description = deployform.cleaned_data['description']

                request.session['step8'] = {'name': name, 'description': description}

                # Request confirmation if deployment already active
                # Save all steps in deployments to this point and activate Deploy
                flagOk, message = activate_deploy(request)

                if not flagOk:
                    msg_str = message
                else:
                    # TODO: Fill other model objects (DeploymentBoundary, etc)
                    # ...

                    # Clear Wizard
                    clear_sessions(1, request)

                    return redirect('/deployments/')

        else:
            if 'step8' in request.session:
                deployform = DeployForm(initial=request.session['step8'])
            else:
                tempname = None
                try:
                    deploy = front_models.Deployment.objects.get(reference=str(request.session['wizard_key']))
                    parent_deploy = dm_models.Deployment.objects.get(id=deploy.id)
                    tempname = parent_deploy.name
                except Exception:
                    pass

                if tempname:
                    deployform = DeployForm(initial={'name': tempname})
                else:
                    deployform = DeployForm()

        step_forms.append(deployform)

    # Just passing a message
    if 'msg_str' in request.session:
        msg_str = request.session['msg_str']
        request.session.pop('msg_str')

    # run_type = 'basic2d'
    # if 'run_type' in request.session['step1']:
    #     run_type = request.session['step1']['run_type']
    baroclinic_type = request.session['step1'].get('baroclinic_type') or 'basic2d'
    waves_type = request.session['step1'].get('waves_type') or 'nowaves'
    wq_type = request.session['step1'].get('wq_type') or 'nowqtype'

    # Format step1 for summary
    step1_data = get_step_html(1, request.session['step1'], baroclinic_type=baroclinic_type, waves_type=waves_type, wq_type=wq_type)

    # Format step2 for summary
    grid_table, bbox, bbox_types, boundaries, boundary_types, bathymetry = None, [], [], [], [], []

    # Return Global Bounding Box only
    # Commented on 26/09/2018 - Global BBox no longer used
    #bbox_queryset = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL)
    #if bbox_queryset:
    #    bbox = serialize('geojson', bbox_queryset)
    #    bbox_types = [{'type': item.bbox_type, 'label': str(dict(front_models.BBox.BOXTYPE)[item.bbox_type]),
    #                   'color': item.color}
    #                  for item in bbox_queryset]

    boundaries = request.session['step2']['boundaries']['geojson']
    boundary_types = request.session['step2']['boundaries']['legend']
    bathymetry = request.session['step2']['bathymetry']

    xmin = 0
    xmax = 0
    ymin = 0
    ymax = 0

    grid_table = get_step_html(2, request.session['step2'])

    # Format step3 for summary
    step3_data = get_step_html(3, request.session['step3'])

    # Format step4 for summary
    step4_data = get_step_html(4, request.session['step4'])

    # Format step5 for summary
    step5_data = get_step_html(5, request.session['step5'], baroclinic_type=baroclinic_type, waves_type=waves_type)

    # Format step6 for summary
    deploy_id = 0
    if "deploy_id" in request.session:
        deploy_id = request.session["deploy_id"][0]
    step6_data = get_step_html(6, request.session['step6'], deploy_id=deploy_id)
    step6_data.request = request  # Pass request to table to get reverse URI

    step7_data = None
    if "no" not in wq_type:
        step7_data = get_step_html(7, request.session['step7'], wq_type=wq_type)

    return render(request, template, {
        'step': step,
        'msg_str': msg_str,
        'forms': step_forms,
        'errors': errors,
        'sum_step1': step1_data,
        'grid_info': grid_table,
        'bbox': bbox,
        'bbox_types': bbox_types,
        'boundaries': boundaries,
        'boundary_types': boundary_types,
        'bathymetry': bathymetry,
        'xmin': xmin,
        'xmax': xmax,
        'ymin': ymin,
        'ymax': ymax,
        'sum_step3': step3_data,
        'sum_step4': step4_data,
        'sum_step5': step5_data,
        'sum_step6': step6_data,
        'sum_step7': step7_data,
        'baroclinic_type': baroclinic_type,
        'waves_type': waves_type,
        'wq_type': wq_type,
        'default_email': settings.DEFAULT_FROM_EMAIL,
        'app': app_name,
    })


@login_required(login_url='/login/')
def validate_file(request):
    errors, err_type, file_ok, form = None, 0, True, DataInputForm(data=request.POST, files=request.FILES)
    boundaries, boundary_types, boundaries_extent, grid_info, sel_crs, bathymetry, cst_params = None, None, None, None, None, None, None
    originalbytes, originalbytes_3d = None, None

    # error_type: 0: return form; 1: return clear form; 2: return map

    # Get user inputs
    filename_orig = form.files['file'].name
    extension = os.path.splitext(filename_orig)[1][1:].strip()
    tmp_file = form.files['file']
    sel_crs = int(form.data['crs'])

    baroclinic_type = 'basic2d'
    if 'baroclinic_type' in request.session['step1']:
        baroclinic_type = request.session['step1']['baroclinic_type']

    if 'crsv' in form.data:
        sel_crsv = float(form.data['crsv'])
    else:
        sel_crsv = .0

    file_lines = []
    try:
        if sel_crs != 4326:
            # Save original bytes if not wgs84
            originalbytes = file2bytes(tmp_file)
            tmp_file.file.seek(0)   # Reposition file to start before reading lines

        max_nodes = request.user.profile.node_maxnr
        # Condition specific to Staff Users
        if request.user.is_staff and max_nodes < 300000:
            max_nodes = 300000

        # First level file validation
        file_lines = TextIOWrapper(tmp_file.file, encoding='utf-8').readlines()
        file_ok, file_errors, clear_form, filename, elems, nodes = schism.validate_file(extension, file_lines, max_nodes, sel_crs)

        # If file not valid
        if not file_ok:
            errors = str(file_errors)
            if clear_form:
                err_type = 1
        else:
            # Get user inputs
            calcdt = None
            if 'calcdt' in form.data:
                calcdt = form.data['calcdt']

            sat = None
            if 'sat' in form.data:
                sat = form.data['sat']

            # Get Mesh elements
            file_ok, file_errors, xyz, ncon, bnd_segm, cpp_lat, dt = schism.read_hgrid(file_lines, sel_crs, sel_crsv, calcdt)
            if file_ok:
                # Process Boundaries
                boundaries, boundary_count = schism.get_boundaries(bnd_segm)

                if boundary_count:
                    # Save grid info
                    grid_info = dict(
                        filename = filename_orig,
                        elems = elems,
                        nodes = nodes,
                        epsg = sel_crs,
                        rvert = sel_crsv,
                        boundaries = '; '.join( (
                            f"Open: {boundary_count['Open']}",
                            f"Land: {boundary_count['Land']}",
                            f"Island: {boundary_count['Island']}",
                        ) )
                    )

                    # Build legend
                    boundary_types = []
                    if boundary_count['Open'] > 0:
                        boundary_types.append({'type': 'Open', 'label': 'Open', 'color': 'blue'})
                    if boundary_count['Land'] > 0:
                        boundary_types.append({'type': 'Land', 'label': 'Land', 'color': 'green'})
                    if boundary_count['Island'] > 0:
                        boundary_types.append({'type': 'Island', 'label': 'Island', 'color': 'red'})

                    # Save PARAM.IN variables
                    cst_params = {}
                    runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=request.session['step1']['run_period_pk'])
                    cst_params['rnday'] = round(runperiod.run_period / 24)  # in days

                    # Comentado porque se decidiu fixar o ics=2
                    # if sel_crs != 4326:
                    #     ics = 1
                    #     cst_params['cpp_lat'] = cpp_lat
                    # else:
                    #     ics = 2
                    # cst_params['ics'] = ics

                    if calcdt and dt:
                        cst_params['dt'] = dt
                        #  Changes in dt value reflect on the following params
                        cst_params['nspool'] = round(3600 / dt)
                        cst_params['ihfskip'] = round(24 * 3600 / dt)
                        cst_params['hotout_write'] = round(24 * 3600 / dt)

                    # Compute Boundaries spatial extent
                    extent_ok, boundaries_extent, msg = compute_extent(boundaries)

                    # Second level file validation
                    # Validate if Boundaries Extent is inside Global BBox and Open boundaries is inside Oceanic BBox
                    # Removed this restriction on 26/09/2018
                    #boundariesinbbox, msg = validate_bboxes(boundaries, sel_crs)

                    # Validate if Outer Boundaries are closed
                    outerboundariesclosed, msg = validate_outerboundaries(boundaries)

                    # Comentado porque se decidiu remover a área global onde devem estar inseridas as malhas
                    #if boundariesinbbox and outerboundariesclosed:
                    if outerboundariesclosed:
                        # All went well - update LAST STEP
                        if request.session['last_step'] < templates[1]['step']:
                            request.session['last_step'] = templates[1]['step']

                        # Finally process Bathymetry
                        bathymetry, max_depth = schism.get_contours(xyz, ncon)

                        if '3d' in baroclinic_type:
                            # Getting vertical grid file
                            tmp_file_3d = form.files['file_3d']

                            originalbytes_3d = file2bytes(tmp_file_3d)
                            tmp_file_3d.file.seek(0)  # Reposition file to start before reading line

                            file_lines_3d = TextIOWrapper(tmp_file_3d.file, encoding='utf-8').readlines()
                            file_ok, file_errors, clear_form, vgrid_info = schism.validate_file_3d(file_lines_3d, max_depth)

                            # If file not valid
                            if not file_ok:
                                errors = str(file_errors)
                                if clear_form:
                                    err_type = 1
                    else:
                        errors = str(msg)
                        err_type = 2

                    if not errors:
                        # Save file content to SessionFile Table only if no errors were found
                        session = Session.objects.get(pk=request.session.session_key)
                        if sel_crs != 4326 and originalbytes:
                            bytesdata = originalbytes
                            file_type = dm_models.FileType.objects.get(name='hgrid', format='gr3')
                        else:
                            bytesdata = schism.write_ll(file_lines, xyz)
                            file_type = dm_models.FileType.objects.get(name='hgrid', format='ll')

                        # Get or Create - no duplicates this way
                        session_file, created = front_models.SessionFile.objects.get_or_create(file_type=file_type,
                                                                                               session=session)
                        session_file.file = bytesdata
                        session_file.name = filename_orig
                        session_file.save()

                        # If hgrid not in .ll format convert hgrid to .ll and save new deployment file
                        if sel_crs != 4326:
                            try:
                                ll_bytesdata = schism.write_ll(file_lines, xyz)
                                if ll_bytesdata:
                                    # Create LL Deployment File
                                    ll_file_type = dm_models.FileType.objects.get(name='hgrid', format='ll')
                                    ll_session_file, ll_created = front_models.SessionFile.objects.get_or_create(
                                        file_type=ll_file_type, session=session)
                                    ll_session_file.file = ll_bytesdata
                                    ll_session_file.save()

                            except Exception:
                                # If hgrid gr3 not found then there is nothing to copy from
                                logger.exception('Error converting .gr3 to .ll format (hgrid.gr3 not found)')

                        # Finally save vertical grid
                        if '3d' in baroclinic_type:
                            in_file_type = dm_models.FileType.objects.get(name='vgrid', format='in')
                            # Get or Create - no duplicates this way
                            in_session_file, created = front_models.SessionFile.objects.get_or_create(file_type=in_file_type,
                                                                                                   session=session)
                            in_session_file.file = originalbytes_3d
                            in_session_file.save()

                            # Update table displayed - for now all we show is the file name
                            # TODO: show full vgrid_info?
                            filename_orig_3d = form.files['file_3d'].name
                            grid_info['filename'] = filename_orig + ', ' + filename_orig_3d

            else:
                errors = str(file_errors)
                err_type = 1

    except Exception:
        logger.exception('Error uploading grid file')
        errors = str(_('Ocorreu um erro ao carregar a malha.'))
        err_type = 1

        # TODO: Save grid that failed to temp table on Database
        # For now we save in media folder
        time_stamp = datetime.strftime(timezone.now(), "%Y-%m-%d-%Hh%Mm%Ss")
        tmp_file_name = os.path.join(settings.BASE_DIR, 'media/wizard/temps/user%s_%s.%s' %
                                     (request.user.id, time_stamp, 'gr3'))
        with open(tmp_file_name, 'w') as destination:
            for line in file_lines:
                destination.write(line)
        logger.debug('Saved grid generating error: file %s' % tmp_file_name)

    # Delete temp file if open
    if tmp_file:
        tmp_file.file.close()

    # Finally
    if not errors or err_type >= 2:
        request.session['step2'] = {'boundaries': {'geojson': boundaries, 'legend': boundary_types},
                                    'extent': boundaries_extent,
                                    'grid_info': grid_info,
                                    'crs': sel_crs,
                                    'bathymetry': bathymetry,
                                    'params': cst_params}

        if sat:
            boundaries_json = json.loads(boundaries)

            features = boundaries_json['features']

            xmin = 2000000000000
            xmax = -200000000000
            ymin = 2000000000000
            ymax = -2000000000000

            for i in range(0, len(features)):
                feature = features[i]
                coordinates = feature['geometry']['coordinates']
                for coord in coordinates:
                    if(coord[0] < xmin):
                        xmin = coord[0]
                    if(coord[0] > xmax):
                        xmax = coord[0]
                    if(coord[1] < ymin):
                        ymin =  coord[1]
                    if(coord[1] > ymax):
                        ymax = coord[1]


            request.session['step2'].update(
                xmin = xmin,
                xmax = xmax,
                ymin = ymin,
                ymax = ymax,
                sat = sat,
            )

        if '3d' in baroclinic_type:
            request.session['step2']['vgrid_info'] = vgrid_info

        if errors and err_type >= 2:
            request.session['msg_str'] = errors

        return redirect(templates[1]['name'])
    else:
        if err_type < 2:
            err_msg = {'msg': errors}
            if err_type == 1:
                err_msg['clear'] = ''

            return JsonResponse(err_msg)


@login_required(login_url='/login/')
def deployments(request):
    template, deploy_table, errors, msg_str = 'front/deployments.html', None, [], None

    # Just passing a message
    if 'msg_str' in request.session:
        msg_str = request.session['msg_str']
        request.session.pop('msg_str')

    # Pass Extend Deploy period form
    forms = []
    forms.append(ExtendDeployForm())

    # Check if its time to ask user for a rate of the service
    rate = time_to_rate(request.user)
    if rate:
        forms.append(ProfileRatingForm())

    return render(request, template, {
        'msg_str': msg_str,
        'errors': errors,
        'forms': forms,
        'app': app_name,
        'rate': rate
    })


@login_required(login_url='/login/')
def open_wizard(request):
    # Load Wizard to last step in session
    if 'last_step' in request.session:
        last_step = request.session['last_step']
        if last_step == len(templates):
            return redirect(templates[last_step-1]['name'])
        else:
            return redirect(templates[last_step]['name'])
    else:
        return redirect(templates[0]['name'])


@login_required(login_url='/login/')
def accept_disclaimer(request):
    # Mark Disclaimer as read
    done = True
    try:
        userprofile = request.user.profile
        userprofile.read_disclaimer = True
        userprofile.save()
    except:
        done = False

    return JsonResponse({'done': done})


@login_required(login_url='/login/')
def step_model_restart(request):
    clear_sessions(1, request)
    exclude = None
    delete_files(request, exclude)
    return redirect(templates[0]['name'])


@login_required(login_url='/login/')
def step_domain_restart(request):
    clear_sessions(2, request)
    exclude = None
    delete_files(request, exclude)
    return redirect(templates[1]['name'])


@login_required(login_url='/login/')
def step_front_restart(request):
    clear_sessions(3, request)
    exclude = ['hgrid', 'vgrid']
    delete_files(request, exclude)
    return redirect(templates[2]['name'])


@login_required(login_url='/login/')
def step_stations_restart(request):
    clear_sessions(4, request)
    exclude = ['hgrid', 'vgrid']
    delete_files(request, exclude)
    return redirect(templates[3]['name'])


@login_required(login_url='/login/')
def step_param_restart(request):
    print(" -------------- Step5 clear starting ------------")
    clear_sessions(5, request)
    print(" -------------- Step5 cleared ------------")
    exclude = ['hgrid', 'vgrid']
    print(" -------------- Step5 exclude ------------")
    delete_files(request, exclude)
    print(" -------------- Step5 delete file ------------")
    return redirect(templates[4]['name'])


@login_required(login_url='/login/')
def step_auxfiles_restart(request):
    clear_sessions(6, request)
    exclude = ['hgrid', 'vgrid', 'param']
    delete_files(request, exclude)
    return redirect(templates[5]['name'])

@login_required(login_url='/login/')
def step_wquality_restart(request):
    clear_sessions(7, request)
    return redirect(templates[6]['name'])

@login_required(login_url='/login/')
def get_run_periods(request, mv_pk):
    # Returns Model version run periods for a specific Model version
    query = dm_models.ModelVersionRunPeriod.objects.filter(model_version__pk=mv_pk).values_list('id', 'run_period')
    values_dict = []
    for item in query:
        values_dict.append({'value': item[0], 'text': '{}h'.format(item[1])})

    return JsonResponse({'data': values_dict})

@login_required(login_url='/login/')
def get_definition(request, name, action=None, deploy_id=None):
    #Get a definition from definition.py based on the simulation model chosen by the user

    content = None

    if 'baroclinic_type' in request.session['step1']:
        baroclinic_type = request.session['step1']['baroclinic_type']

    if 'waves_type' in request.session['step1']:
        waves_type = request.session['step1']['waves_type']

    if 'wq_type' in request.session['step1']:
        wq_type = request.session['step1']['wq_type']

    definition_key = (baroclinic_type, waves_type, wq_type)
    definition = RUN_TYPE_MAP[definition_key]

    name_key = (name)
    name_kind = KIND_TYPE_MAP[name_key]

    query = wiff_models.Configuration.objects.filter(target=definition, kind=name_kind)

    content = query.first().text

    if (deploy_id == None):
        response = HttpResponse(content, content_type='text/plain')
    else:
        deploy_id = int(deploy_id)

        data = Parser().reads(content)

        deploy = front_models.Deployment.objects.get(id=deploy_id)

        step1_obj = deploy.step1 or request.session['step1']
        step5_obj = deploy.step5 or request.session['step5']

        if name == 'param':

            run_period_hours = dm_models.ModelVersionRunPeriod.objects.get(id=step1_obj['run_period_pk']).run_period
            run_period_days = run_period_hours/24

            dt = step5_obj["params"]['dt']

            data['core']['dt'] = int(float(dt))
            data['core']['ihfskip'] = round(24 * 3600 / int(float(dt)))
            data['core']['nspool'] = round(3600 / int(float(dt)))
            data['core']['ihfskip'] = round(24 * 3600 / int(float(dt)))
            data['schout']['nhot_write'] = round(24 * 3600 / int(float(dt)))
            data['opt']['nstep_wwm'] = int(float(step5_obj["params"]['nstep_wwm']))
            data['opt']['dramp'] = int(float(step5_obj["params"]['dramp']))
            data['core']['rnday'] = int(float(run_period_days))

        elif name == 'wwminput':
            data['engs']['brhd'] = int(float(step5_obj["wwm"]["params"]['ENGS_BRHD']))
            data['proc']['deltc'] = int(float(step5_obj["wwm"]["params"]['PROC_DELTC']))

        #patched = patch(content, nml_patch, data)

        if action == 'download':
            response = HttpResponse(str(data), content_type='text/plain')
            if deploy_id == 0:
                response['Content-Disposition'] = "attachment; filename=%s.%s" % (name, format)
            else:
                response['Content-Disposition'] = "attachment; filename=%s_%s.%s" % (deploy_id, name, format)
        else:
            response = HttpResponse(str(data), content_type='text/plain')

    return response


@deploy_permission
@login_required(login_url='/login/')
def get_file(request, deploy_id=None, name=None, action=None, format=None):
    # Get a deployment's file
    # Changed this method, no longer saving integral auxiliary files
    # Only saving constant or array values
    # For auxiliary files we must build them
    deploy_id = int(deploy_id)
    content = None

    try:
        if name == 'param' or name == 'wwminput':
            if name == 'param':
                if deploy_id == 0:
                    choice = request.session['step5']['choice']
                    data = request.session['step5']['params']
                else:
                    # Get Deployment
                    deploy = front_models.Deployment.objects.get(id=deploy_id)

                    if deploy.step5:
                        choice = deploy.step5['choice']
                        data = deploy.step5['params']
                    else:
                        choice = request.session['step5']['choice']
                        data = request.session['step5']['params']
            elif name == 'wwminput':
                if deploy_id == 0:
                    choice = request.session['step5']['wwm']['choice']
                    data = request.session['step5']['wwm']['params']
                else:
                    # Get Deployment
                    deploy = front_models.Deployment.objects.get(id=deploy_id)

                    if deploy.step5:
                        choice = deploy.step5['wwm']['choice']
                        data = deploy.step5['wwm']['params']
                    else:
                        choice = request.session['step5']['wwm']['choice']
                        data = request.session['step5']['wwm']['params']

            if choice != 'U':
                baroclinic_type = 'basic2d'
                if deploy_id == 0:
                    sel_model_version = request.session['step1']['model_pk']
                    if 'baroclinic_type' in request.session['step1']:
                        baroclinic_type = request.session['step1']['baroclinic_type']
                else:
                    sel_model_version = deploy.step1['model_pk']
                    if 'baroclinic_type' in deploy.step1:
                        baroclinic_type = deploy.step1['baroclinic_type']

                # Fill and return param.in file from step5
                version = dm_models.ModelVersion.objects.get(pk=sel_model_version).version.replace('.', '')
                template_name = "wizard/files/schism_{0}_{1}.{2}.html".format(version, baroclinic_type, name)
                template = get_template(template_name)
                content = template.render(data)
            else:
                # Get uploaded param.in
                if deploy_id == 0:
                    # Get file from SessionFile model
                    session = Session.objects.get(pk=request.session.session_key)
                    file_type = dm_models.FileType.objects.get(name=name, format=format)
                    session_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)
                    content = bytes(session_file.file)
                else:
                    try:
                        file_type = dm_models.FileType.objects.get(name=name, format=format)
                        deploy_file = dm_models.DeploymentFile.objects.get(file_type=file_type, deployment=deploy)
                        content = bytes(deploy_file.file)
                    except Exception:
                        # if there is a deployment id but not a deployment file an error will occur
                        # this happens when user has restarted step Domain of the wizard and hasn't saved changes
                        session = Session.objects.get(pk=request.session.session_key)
                        file_type = dm_models.FileType.objects.get(name=name, format=format)
                        session_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)
                        content = bytes(session_file.file)
        else:
            if deploy_id == 0:
                # Get file from SessionFile model
                session = Session.objects.get(pk=request.session.session_key)
                if 'grid' in name:
                    # hgrid or vgrid
                    file_type = dm_models.FileType.objects.get(name=name, format=format)
                    session_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)
                    content = bytes(session_file.file)
                else:
                    # auxiliary file
                    # Get hgrid.ll
                    file_type = dm_models.FileType.objects.get(name='hgrid', format='ll')
                    hgrid_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)
                    hgrid_content = bytes(hgrid_file.file)
                    # Get constant or array
                    for item in request.session["step6"]["auxfilesset"]:
                        if item["name"].lower() == name:
                            if item["choice"] == "C":
                                content = schism.write_auxfile_fromhgrid(hgrid_content, item["constant"], name)
                            elif item["choice"] == "U":
                                content = schism.write_auxfile_fromhgrid(hgrid_content, item["array"], name)
            else:
                # Get file from DeploymentFile model
                deploy = front_models.Deployment.objects.get(id=deploy_id)
                if 'grid' in name:
                    # hgrid or vgrid
                    try:
                        file_type = dm_models.FileType.objects.get(name=name, format=format)
                        d_file = dm_models.DeploymentFile.objects.get(file_type=file_type, deployment=deploy)
                    except Exception:
                        # if there is a deployment id but not a deployment file an error will occur
                        # this happens when user has restarted step Domain of the wizard and hasn't saved changes
                        session = Session.objects.get(pk=request.session.session_key)
                        d_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)

                    content = bytes(d_file.file)
                else:
                    # auxiliary file
                    # Get hgrid.ll
                    try:
                        file_type = dm_models.FileType.objects.get(name='hgrid', format='ll')
                        hgrid_file = dm_models.DeploymentFile.objects.get(file_type=file_type, deployment=deploy)
                    except Exception:
                        session = Session.objects.get(pk=request.session.session_key)
                        hgrid_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)

                    hgrid_content = bytes(hgrid_file.file)

                    if deploy.step6 and "auxfilesset" in deploy.step6:
                        if 'wizard_key' in request.session and deploy.reference == str(request.session['wizard_key']):
                            # Sessions can be more updated
                            items = request.session["step6"]["auxfilesset"]
                        else:
                            items = deploy.step6["auxfilesset"]
                    else:
                        items = request.session["step6"]["auxfilesset"]

                    # Get constant or array
                    for item in items:
                        if item["name"].lower() == name:
                            if item["choice"] == "C":
                                content = schism.write_auxfile_fromhgrid(hgrid_content, item["constant"], name)
                            elif item["choice"] == "U":
                                content = schism.write_auxfile_fromhgrid(hgrid_content, item["array"], name)
    except Exception:
        pass

    if action == 'download':
        response = HttpResponse(content, content_type='text/plain')
        if deploy_id == 0:
            response['Content-Disposition'] = "attachment; filename=%s.%s" % (name, format)
        else:
            response['Content-Disposition'] = "attachment; filename=%s_%s.%s" % (deploy_id, name, format)
    else:
        response = HttpResponse(content, content_type='text/plain')

    return response


@login_required(login_url='/login/')
def save_steps(request, step):
    valid, msg, deploy_id = True, None, None

    # Execute only if step 1 is done, at least
    if 'last_step' in request.session and 'step1' in request.session:
        reference = request.session['wizard_key']

        if int(step) > 0:
            # Updating Step, validating form
            valid, msg = form_validation(request, step)

            # Check if deploy already exists
            try:
                deploy_test = front_models.Deployment.objects.get(reference=reference)
            except:
                # If it doesn't we exit - user hasn't execute save deploy yet
                return JsonResponse({'msg': msg, 'deploy_id': deploy_id})

        # If 'global save' we save deploy or create it if it doesn't exist
        # If 'step update' we save existing deploy to the DB with the changes
        if valid:
            try:
                now = timezone.now()
                user = request.user
                mv_runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=request.session['step1']['run_period_pk'])

                # If Deploy does not exist we ask for a name for it
                if 'inputname' in request.POST:
                    name = str(request.POST['inputname'])
                else:
                    nextnr = front_models.Deployment.objects.filter(user=request.user).count() + 1
                    name = 'Deploy {0:02d}'.format(nextnr)

                # Create or update a Deploy with all the steps session variables
                deploy, created = front_models.Deployment.objects.get_or_create(reference=reference,
                                                                                defaults={'creation_datetime': now,
                                                                                          'user': user,
                                                                                          'name': name, 'active': False,
                                                                                          'model_version_period': mv_runperiod})

                # Update model version run period
                deploy.model_version_period = mv_runperiod

                deploy.step = int(request.session['last_step'])
                deploy.step1 = request.session['step1']

                if 'step2' in request.session:
                    deploy.step2 = request.session['step2']
                else:
                    deploy.step2 = None

                if 'step3' in request.session:
                    deploy.step3 = request.session['step3']
                else:
                    deploy.step3 = None

                if 'step4' in request.session:
                    deploy.step4 = request.session['step4']
                else:
                    deploy.step4 = None

                if 'step5' in request.session:
                    deploy.step5 = request.session['step5']
                else:
                    deploy.step5 = None

                if 'step6' in request.session:
                    deploy.step6 = request.session['step6']
                else:
                    deploy.step6 = None

                if 'step7' in request.session:
                    deploy.step7 = request.session['step7']
                else:
                    deploy.step7 = None

                deploy.save()

                # Save all Sessionfile(s) to DeploymentFile(s)
                session = Session.objects.get(pk=request.session.session_key)
                session_files = front_models.SessionFile.objects.filter(session=session)
                if session_files:
                    for session_file in session_files:
                        # Create or replace
                        deploy_file, created2 = dm_models.DeploymentFile.objects.get_or_create(file_type=session_file.file_type,
                                                                                              deployment=deploy)
                        deploy_file.file = session_file.file
                        deploy_file.name = session_file.name
                        deploy_file.save()

                        # Delete already saved file from SessionFile
                        session_file.delete()

                # Finally prepare message to user
                deploy_id = 'ID:' + str(deploy.id)
                if created:
                    msg = str(format_lazy('{} {} {}', _('Sistema de Previsão'), deploy_id, _('criado com sucesso.')))

                    # Create session deploy_id
                    dep_id = [deploy.id] if not deploy.clone else [deploy.id, deploy.clone]
                    request.session['deploy_id'] = dep_id
                else:
                    if int(step) == 0:
                        msg = str(format_lazy('{} {} {}', _('Sistema de Previsão'), deploy_id, _('guardado com sucesso.')))
                    else:
                        msg = str(format_lazy('{} {} {}', _('Sistema de Previsão'), deploy_id, _('alterado e guardado com sucesso.')))

            except Exception:
                logger.exception('Error saving steps')
                msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    else:
        msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse({'msg': msg, 'deploy_id': deploy_id})


# Not in use - Wizard is always allowed now, only submitting deploys are limited
@login_required(login_url='/login/')
def new_deploy(request):
    # Restart and open the Wizard if allowed
    msg = None

    try:
        if not new_deploy_possible(request.user):
            msg = str(_('Lamentamos mas atingiu o seu limite de Sistemas de Previsão ativos.'))

    except Exception:
        logger.exception('Error on new deploy')
        msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse({'msg': msg})


@deploy_permission
@login_required(login_url='/login/')
def view_deploy(request, deploy_id):
    # Return Deploy's details table
    template = get_template("wizard/steps_summary.html")

    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        bbox, bbox_types, choice = [], [], None

        baroclinic_type = 'basic2d'
        waves_type = 'nowaves'
        wq_type = 'nowqtype'
        if 'baroclinic_type' in deploy.step1:
            baroclinic_type = deploy.step1['baroclinic_type']

        if  'waves_type' in deploy.step1:
            waves_type = deploy.step1['waves_type']

        if 'wq_type' in deploy.step1:
            wq_type = deploy.step1['wq_type']

        # Format step1 for summary
        step1_data = get_step_html(1, deploy.step1, baroclinic_type=baroclinic_type, waves_type=waves_type, wq_type=wq_type)

        # Format step2 for summary
        # Return Global Bounding Box only
        # Commented on 26/09/2018 - Global BBox no longer used
        #bbox_queryset = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL)
        #if bbox_queryset:
        #    bbox = serialize('geojson', bbox_queryset)
        #    bbox_types = [{'type': item.bbox_type, 'label': str(dict(front_models.BBox.BOXTYPE)[item.bbox_type]),
        #                   'color': item.color}
        #                  for item in bbox_queryset]

        boundaries = deploy.step2['boundaries']['geojson']
        boundary_types = deploy.step2['boundaries']['legend']
        bathymetry = deploy.step2['bathymetry']

        grid_table = get_step_html(2, deploy.step2)

        # Format step3 for summary
        step3_data = get_step_html(3, deploy.step3)

        # Format step4 for summary
        step4_data = get_step_html(4, deploy.step4)

        # Format step5 for summary
        step5_data = get_step_html(5, deploy.step5, baroclinic_type=baroclinic_type, waves_type=waves_type)

        # Format step6 for summary
        step6_data = get_step_html(6, deploy.step6, deploy_id=deploy_id)
        step6_data.request = request  # Pass request to table to get reverse URI

        step7_data = None
        if "no" not in 'wq_type':
            step7_data = get_step_html(7, deploy.step7, wq_type=wq_type)

        content = template.render({
            'deploy_id': deploy_id,
            'sum_step1': step1_data,
            'grid_info': grid_table,
            'bbox': bbox,
            'bbox_types': bbox_types,
            'boundaries': boundaries,
            'boundary_types': boundary_types,
            'bathymetry': bathymetry,
            'sum_step3': step3_data,
            'sum_step4': step4_data,
            'sum_step5': step5_data,
            'sum_step6': step6_data,
            'sum_step7': step7_data,
            'baroclinic_type': baroclinic_type,
            'wq_type': wq_type
        })

    except Exception:
        logger.exception('Error viewing deploy')
        content = "error"

    return HttpResponse(content, content_type='text/html')


@deploy_permission
@login_required(login_url='/login/')
def open_deploy(request, deploy_id):
    # Open the Deploy in the Wizard (only non submitted deploys)
    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        if not deploy.step:
            request.session['msg_str'] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
            return redirect('/deployments')
        else:
            # Delete all related instances in SessionFile if exist
            session = Session.objects.get(pk=request.session.session_key)
            session_files = front_models.SessionFile.objects.filter(session=session)
            if session_files:
                for session_file in session_files:
                    session_file.delete()

            # Fill request session values
            fill_sessions(request, deploy)

            if deploy.step == 8:
                return redirect(templates[deploy.step - 1]['name'])
            else:
                return redirect(templates[deploy.step]['name'])

    except Exception:
        logger.exception('Error opening deploy')
        request.session['msg_str'] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        return redirect('/deployments')


@deploy_permission
@login_required(login_url='/login/')
def clone_deploy(request, deploy_id):
    # Duplicate the Deploy to a non-submitted Deploy with the same attributes
    response = {}
    try:
        # Get selected deployment
        sel_deploy = front_models.Deployment.objects.get(id=deploy_id)

        if not sel_deploy.step:
            response["success"] = False
            response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        else:
            now = timezone.now()
            mv_runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=sel_deploy.step1['run_period_pk'])

            time_ref = datetime.strftime(timezone.now(), '%Y-%m-%d-%Hh%Mm%S_%fs')
            reference = f'u{request.user.id}_{time_ref}'
            name = sel_deploy.step8['name']
            description = sel_deploy.step8['description']

            # Create new inactive deployment from selected one
            deploy = front_models.Deployment(creation_datetime=now, model_version_period=mv_runperiod, active=False,
                                                 user=request.user, reference=reference, step=6, name=name, description=description,
                                                 step1=sel_deploy.step1, step2=sel_deploy.step2, step3=sel_deploy.step3,
                                                 step4=sel_deploy.step4, step5=sel_deploy.step5, step6=sel_deploy.step6,
                                                 step7=sel_deploy.step7, clone=sel_deploy.id)

            deploy.save()

            # Clone sel_deploy's DeploymentFile(s) to new deploy
            sel_deploy_files = dm_models.DeploymentFile.objects.filter(deployment__pk=sel_deploy.id)
            if sel_deploy_files:
                for sel_deploy_file in sel_deploy_files:
                    deploy_file = dm_models.DeploymentFile(file_type=sel_deploy_file.file_type,
                                                               file=sel_deploy_file.file, name=sel_deploy_file.name,
                                                               deployment=deploy)
                    deploy_file.save()

            response["success"] = True
            response["msg"] = str(_('Clone criado com sucesso! Pode abrir o novo sistema no Assistente de Configuração e usar o menu de passos para efetuar as alterações desejadas.'))

    except Exception:
        logger.exception('Error cloning deploy')
        response["success"] = False
        response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse(response)


@deploy_permission
@login_required(login_url='/login/')
def deactivate_deploy(request, deploy_id):
    # Change the status of the Deploy to non active
    # TODO: not stable action
    response = {}
    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        if not deploy.step:
            response["success"] = False
            response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        else:
            if deploy.step:
                deploy.active = False
                deploy.save()

                response["success"] = True
                response["msg"] = str(format_lazy('{} {} {}', _('Sistema de Previsão'), deploy_id, _('desativado com sucesso.')))

    except Exception:
        logger.exception('Error deactivating deploy')
        response["success"] = False
        response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse(response)


@deploy_permission
@login_required(login_url='/login/')
def reactivate_deploy(request, deploy_id):
    # Change the status of the Deploy to active
    # TODO: not stable action
    response = {}
    try:
        # Validate if user can activate more systems
        if not new_deploy_possible(request.user):
            response["success"] = False
            response["msg"] = str(_('Lamentamos mas atingiu o seu limite de Sistemas de Previsão ativos.'))
        else:
            # Get Deployment
            deploy = front_models.Deployment.objects.get(id=deploy_id)

            if not deploy.step:
                response["success"] = False
                response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
            else:
                deploy.active = True
                deploy.save()

                response["success"] = True
                response["msg"] = str(format_lazy('{} {} {}', _('Sistema de Previsão'), deploy_id, _('reativado com sucesso.')))

    except Exception:
        logger.exception('Error reactivating deploy')
        response["success"] = False
        response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse(response)


@deploy_permission
@login_required(login_url='/login/')
def delete_deploy(request, deploy_id):
    # Change the status of the Deploy to deleted (and if never active deletes all associated files??)
    # TODO: not stable action
    response = {}
    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        if not deploy.step:
            response["success"] = False
            response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        else:
            deploy.deleted = True
            deploy.save()

            # If deploy loaded on Wizard delete it
            if 'wizard_key' in request.session:
                if request.session['wizard_key'] == deploy.reference:
                    step_model_restart(request)

            response["success"] = True
            response["msg"] = str(format_lazy('{} {} {}', _('Sistema de Previsão'), deploy_id, _('apagado com sucesso.')))

    except Exception:
        logger.exception('Error deleting deploy')
        response["success"] = False
        response["msg"] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse(response)


@deploy_permission
@login_required(login_url='/login/')
def print_deploy(request, deploy_id):
    # Print Deploy Summary
    deploy_table, grid_table, bbox, bbox_types, boundaries, boundary_types, bathymetry = None, None, [], [], [], [], []
    step1_data, step3_data, step4_data, step5_data, step6_data, step7_data = None, None, None, None, None, None

    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        if not deploy.step:
            request.session['msg_str'] = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
            return redirect('/deployments')
        else:
            # Format step1 for summary
            baroclinic_type = 'basic2d'
            waves_type = 'nowaves'
            wq_type = 'nowqtype'
            if 'baroclinic_type' in deploy.step1:
                baroclinic_type = deploy.step1['baroclinic_type']

            if  'waves_type' in deploy.step1:
                waves_type = deploy.step1['waves_type']

            if 'wq_type' in deploy.step1:
                wq_type = deploy.step1['wq_type']

            step1_data = get_step_html(1, deploy.step1, baroclinic_type=baroclinic_type, waves_type=waves_type, wq_type=wq_type)

            # Format step2 for summary
            # Return Global Bounding Box only
            # Commented on 26/09/2018 - Global BBox no longer used
            #bbox_queryset = front_models.BBox.objects.filter(bbox_type=front_models.BBox.BOXTYPE_GLOBAL)
            #if bbox_queryset:
            #    bbox = serialize('geojson', bbox_queryset)
            #    bbox_types = [
            #        {'type': item.bbox_type, 'label': str(dict(front_models.BBox.BOXTYPE)[item.bbox_type]), 'color': item.color}
            #        for item in bbox_queryset]

            boundaries = deploy.step2['boundaries']['geojson']
            boundary_types = deploy.step2['boundaries']['legend']
            bathymetry = deploy.step2['bathymetry']

            grid_table = get_step_html(2, deploy.step2)

            # Format step3 for summary
            step3_data = get_step_html(3, deploy.step3)

            # Format step4 for summary
            step4_data = get_step_html(4, deploy.step4)

            # Format step5 for summary
            step5_data = get_step_html(5, deploy.step5, baroclinic_type=baroclinic_type, waves_type=waves_type)

            # Format step6 for summary
            step6_data = get_step_html(6, deploy.step6, deploy_id=deploy_id)
            step6_data.request = request  # Pass request to table to get reverse URI

            if 'wq_type' in request.session['step1']:
                wq_type = request.session['step1'].get('wq_type')
                step7_data = get_step_html(7, request.session['step7'], wq_type=wq_type)

            deploys = front_models.Deployment.objects.filter(id=deploy_id)
            deploy_table = DeploymentPrintTable(deploys)

    except Exception:
        logger.exception('Error printing deploy')

    return render(request, 'wizard/steps_summary_print.html', {
        'deploy_id': deploy_id,
        'deploy_table': deploy_table,
        'sum_step1': step1_data,
        'grid_info': grid_table,
        'bbox': bbox,
        'bbox_types': bbox_types,
        'boundaries': boundaries,
        'boundary_types': boundary_types,
        'bathymetry': bathymetry,
        'sum_step3': step3_data,
        'sum_step4': step4_data,
        'sum_step5': step5_data,
        'sum_step6': step6_data,
        'sum_step7': step7_data,
        'baroclinic_type': baroclinic_type,
        'waves_type': waves_type,
        'wq_type': wq_type,
        'print': True
    })


@deploy_permission
@login_required(login_url='/login/')
def get_boundaries(request, deploy_id):
    # Get deployment's boundaries
    boundaries = None
    try:
        # Get Deployment
        deploy = front_models.Deployment.objects.get(id=deploy_id)

        if deploy.step2 and 'boundaries' in deploy.step2 and 'geojson' in deploy.step2["boundaries"]:
            boundaries = deploy.step2["boundaries"]["geojson"]

    except Exception:
        logger.exception('Error getting deployments boundaries')
        boundaries = None
        pass

    return JsonResponse({'boundaries': boundaries})


@login_required(login_url='/login/')
def extend_deploy(request):
    # User's request to extend the Deploy, saves instance in models and sends email to Admin
    # TODO: not stable - can a user make more than one extend requests of a deploy? or only one?
    msg, form_data = None, request.POST

    try:
        if not form_data['deploy_id']:
            msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))
        else:
            deploy_id = form_data['deploy_id']
            motive = str(form_data['motive'])
            to_date = datetime.strptime(form_data['to_date'], str(_('%d/%m/%Y'))).date()

            # Check if an opened extend request for this deployment already exists
            count = front_models.ExtendDeployRequest.objects.filter(deployment__pk=deploy_id, closed=False).count()
            if count > 0:
                msg = str(format_lazy('{} {}', _('Já existe um Pedido pendente para o Sistema de Previsão ID:'), deploy_id))
            else:
                now = timezone.now()
                deploy = front_models.Deployment.objects.get(id=deploy_id)

                # Save request in database
                extend = front_models.ExtendDeployRequest(creation_datetime=now, to_date=to_date, motive=motive, closed=False, deployment=deploy)
                extend.save()

                msg = str(format_lazy('{} {}', _('Pedido de Extensão do Sistema de Previsão ID:'), deploy_id, _(' enviado com sucesso.')))

                # Prepare email to notify admin and user
                notify_extend_deploy(extend, get_current_site(request))

    except Exception:
        logger.exception('Error extending deploy')
        msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse({'msg': msg})


@login_required(login_url='/login/')
def view_extend_deploys(request):
    # Return Extend Deploys Requests table
    extenddeploy_table = None
    try:
        # Get User's Deployments
        deploys = front_models.Deployment.objects.filter(user=request.user).filter(deleted=False)
        extenddeploys = front_models.ExtendDeployRequest.objects.filter(deployment__in=deploys).order_by('-creation_datetime')
        extenddeploy_table = ExtendDeploysTable(extenddeploys).as_html(request)
    except Exception:
        logger.exception('Error rendering deploy extend requests')

    return HttpResponse(extenddeploy_table, content_type='text/html')


@login_required(login_url='/login/')
def get_deploys(request):
    try:
        # Return processed Deploy records in json
        if not request.user.is_staff:
            # Get User's Deployments
            deploys = dm_models.Deployment.objects.filter(deployment__user=request.user).filter(deleted=False)\
                .values('pk', 'name', 'description', 'active', 'creation_datetime', 'begin_date', 'end_date',
                        'last_run_date', 'model_version_period', 'deployment__step', 'deployment__step1', 'deployment__clone')
        else:
            # Get All Deployments (not deleted)
            deploys = dm_models.Deployment.objects.filter(deleted=False) \
                .values('pk', 'name', 'description', 'active', 'creation_datetime', 'begin_date', 'end_date',
                        'last_run_date', 'model_version_period', 'deployment__step', 'deployment__step1', 'deployment__clone', 'deployment__user__username')

        deploy_data = []
        for item in deploys:
            pk = str(item["pk"])

            baroclinic_type = str(BAROCLINIC_TYPE_map['basic2d'])
            print("---------- ITEM ------------")
            print(item["deployment__step1"])
            if 'baroclinic_type' in item["deployment__step1"]:
                baroclinic_type = str(BAROCLINIC_TYPE_map[item["deployment__step1"]["baroclinic_type"]])

            model = "{0} ({1})".format(dm_models.ModelVersionRunPeriod.objects.get(id=item["model_version_period"]), baroclinic_type)
            name = item["name"]
            description = item["description"]
            active = item["active"]
            creation = item["creation_datetime"]
            begin_date = item["begin_date"]
            last_run_date = item["last_run_date"]
            end_date = item["end_date"]
            step = item["deployment__step"]

            if request.user.is_staff:
                creator = item["deployment__user__username"]

            # Render deployment's state
            runperiodhours = dm_models.ModelVersionRunPeriod.objects.get(id=item['deployment__step1']['run_period_pk']).run_period
            state_code = get_state(step, active, begin_date, end_date, runperiodhours, last_run_date)
            state, actions_html = render_state(state_code, step, pk)

            # If deploy was cloned change label to add that information
            pk_label = pk if not item["deployment__clone"] else '%s:%s' % (pk, str(item["deployment__clone"]))
            pk_html = '<span class="label label-info">{0}</span>'.format(pk_label)

            # TODO: remove this check when users can see more than their deployments
            if request.user.is_staff:
                # Add information about creator
                description = '<b>{0}</b> {1}<br/><span class="text-muted">{2}</span>'.format(_('Criado por'), creator, description)
            else:
                description = '<span class="text-muted">{0}</span>'.format(description)

            deploy_data.append({
                "pk_code": int(item["pk"]),
                "pk": mark_safe(pk_html),
                "name": name,
                "description": mark_safe(description),
                "model": model,
                "creation_datetime": date_format(creation, format='SHORT_DATETIME_FORMAT'),
                "begin_date": date_format(begin_date, format='SHORT_DATE_FORMAT') if item["begin_date"] else "",
                "end_date": date_format(end_date, format='SHORT_DATE_FORMAT') if item["end_date"] else "",
                "last_run_date": date_format(last_run_date, format='SHORT_DATE_FORMAT') if item["last_run_date"] else "",
                "state_code": state_code,
                "state": mark_safe(state),
                "actions": mark_safe(actions_html)
            })

        deploy_table = {}
        deploy_table["data"] = deploy_data

    except Exception:
        logger.exception('Error getting deploys')
        deploy_table = None

    return JsonResponse(deploy_table, safe=False)


def clear_sessions(index, request):
    # Clear session group of current step as well as all created on following steps
    stepindex = index
    for i in range(stepindex, len(templates) + 1):
        step = 'step' + str(i)
        if step in request.session:
            request.session.pop(step)

    if index == 1:
        # Clear all sessions
        if 'last_step' in request.session:
            request.session.pop('last_step')
        if 'wizard_key' in request.session:
            request.session.pop('wizard_key')
        if 'deploy_id' in request.session:
            request.session.pop('deploy_id')
    else:
        request.session['last_step'] = stepindex - 1


def get_step_html(step, origin, deploy_id=None, baroclinic_type=None, waves_type=None, wq_type=None):
    # Not a view - Auxiliary function only
    # Helps to fill Summary templates (on step_deploy or on deployments page view deploy
    # summary or on print deploy summary)
    step_html = None

    try:
        if step == 1:
            step_html = {
                'model': str(dm_models.ModelVersionRunPeriod.objects.get(id=origin['run_period_pk'])),
                'baroclinic_type': str(BAROCLINIC_TYPE_map[baroclinic_type]),
                'waves_type': str(WAVES_TYPE_map[waves_type]),
                'wq_type': str(WATER_QUALITY_TYPE_map[wq_type]),
            }

        elif step == 2:
            grid_info = origin['grid_info']
            step_html = GridTableSummary([grid_info])

        elif step == 3:
            forcings, atmmodel = None, None
            if 'forcings' in origin:
                forcings = ForcingsTableSummary(origin['forcings'])
            if 'atmmodel' in origin:
                fmodels = ((x[0], x[1]) for x in
                           front_models.ForcingSource.objects.values_list('parent__reference', 'parent__name'))
                try:
                    atmmodel = str(dict(fmodels)[origin['atmmodel']])
                except Exception:
                    atmmodel = 'No forcing'
                    pass
            step_html = [forcings, atmmodel]

        elif step == 4:
            step4 = origin['stations']
            step4_items = []
            for item in step4:
                if item['chk']:
                    step4_items.append(item)
            step_html = StationsTableSummary(step4_items)

        elif step == 5:
            step_html, step5_data, step5_items = [], [], []
            step5 = origin['params']
            for item in step5:
                # Translate labels
                step5_items.append({'name': _(item), 'value': step5[item]})
            choice = origin['choice']
            params = ParametersTableSummary(step5_items)

            step_html.append(choice)
            step_html.append(params)

            if waves_type:
                if 'yes' in waves_type:
                    step5_items = []
                    step5 = origin['wwm']['params']
                    for item in step5:
                        # Translate labels
                        step5_items.append({'name': _(item), 'value': step5[item]})

                    wwm_choice = origin['wwm']['choice']
                    wwm_params = ParametersTableSummary(step5_items)

                    step_html.append(wwm_choice)
                    step_html.append(wwm_params)

        elif step == 6:
            aux_step6 = origin['auxfilesset']
            step6_items, choice = [], None
            for item in aux_step6:
                if 'hidden' not in item:
                    item['hidden'] = 'N'
                if item['hidden'] == 'N':
                    if item['choice'] == 'C':
                        choice = item['constant']

                    elif item['choice'] == 'D':
                        if 'constant' in item:
                            choice = item['constant']
                        else:
                            choice = item['value']
                    elif item['choice'] == 'U':
                        choice = item['file']
                    step6_items.append({'name': item['name'], 'choice': choice, 'fileaction': str(item['name']).lower(),
                                        'deploy_id': deploy_id})
            step_html = AuxFilesTableSummary(step6_items)

        elif step == 7:
            if wq_type:
                wq_forcings = None
                if 'wq_forcings' in origin:
                    wq_forcings = WaterForcingsTableSummary(origin['wq_forcings'])

                if 'sources' in origin:
                    sources = origin['sources']
                    sources_items = []
                    for item in sources:
                        if item['chk']:
                            sources_items.append(item)
                    sources_table = SourcesTableSummary(sources_items)
                    
                    step_html = [wq_forcings, sources_table]
                else:
                    step_html = [wq_forcings]

    except Exception:
        logger.exception('Error returning step html for summary')
        pass

    return step_html


templates = [
    {'step': 1, 'name': '/wizard/step1', 'path': 'wizard/step_model.html', 'function': step_model, 'restart': step_model_restart},
    {'step': 2, 'name': '/wizard/step2', 'path': 'wizard/step_domain.html', 'function': step_domain, 'restart': step_domain_restart},
    {'step': 3, 'name': '/wizard/step3', 'path': 'wizard/step_front.html', 'function': step_front, 'restart': step_front_restart},
    {'step': 4, 'name': '/wizard/step4', 'path': 'wizard/step_stations.html', 'function': step_stations, 'restart': step_stations_restart},
    {'step': 5, 'name': '/wizard/step5', 'path': 'wizard/step_param.html', 'function': step_param, 'restart': step_param_restart},
    {'step': 6, 'name': '/wizard/step6', 'path': 'wizard/step_auxfiles.html', 'function': step_auxfiles, 'restart': step_auxfiles_restart},
    {'step': 7, 'name': '/wizard/step7', 'path': 'wizard/step_wquality.html', 'function': step_wquality, 'restart': step_wquality_restart},
    {'step': 8, 'name': '/wizard/step8', 'path': 'wizard/step_deploy.html', 'function': step_deploy},
]


'''
@login_required(login_url='/login/')
def save_steps(request):
    # Create or update a Deploy with all the steps session variables
    msg, deploy_id = None, None

    # Save only if step 1 is done, at least
    if 'last_step' in request.session and 'step1' in request.session:
        try:
            user = request.user
            mv_runperiod = dm_models.ModelVersionRunPeriod.objects.get(id=request.session['step1']['run_period_pk'])

            reference = request.session['wizard_key']
            now = timezone.now()

            # If Deploy does not exist we ask for a name for it
            if request.POST:
                name = str(request.POST['inputname'])
            else:
                nextnr = front_models.Deployment.objects.filter(user=request.user).count() + 1
                name = 'Deploy {0:02d}'.format(nextnr)

            deploy, created = front_models.Deployment.objects.get_or_create(reference=reference,
                                                                            defaults={'creation_datetime': now,
                                                                                      'user': user,
                                                                                      'name': name, 'active': False,
                                                                                      'model_version_period': mv_runperiod})

            # Update model version run period
            deploy.model_version_period = mv_runperiod

            deploy.step = int(request.session['last_step'])
            deploy.step1 = request.session['step1']

            if 'step2' in request.session:
                deploy.step2 = request.session['step2']
            else:
                deploy.step2 = None

            if 'step3' in request.session:
                deploy.step3 = request.session['step3']
            else:
                deploy.step3 = None

            if 'step4' in request.session:
                deploy.step4 = request.session['step4']
            else:
                deploy.step4 = None

            if 'step5' in request.session:
                deploy.step5 = request.session['step5']
            else:
                deploy.step5 = None

            if 'step6' in request.session:
                deploy.step6 = request.session['step6']
            else:
                deploy.step6 = None

            deploy.save()

            # Save all Sessionfile(s) to DeploymentFile(s)
            session = Session.objects.get(pk=request.session.session_key)
            session_files = front_models.SessionFile.objects.filter(session=session)
            if session_files:
                for session_file in session_files:
                    deploy_file, created = dm_models.DeploymentFile.objects.get_or_create(file_type=session_file.file_type, deployment=deploy)
                    deploy_file.file = session_file.file
                    deploy_file.name = session_file.name
                    deploy_file.save()
                    # Delete already saved file from SessionFile
                    session_file.delete()

            # Finally prepare message to user
            deploy_id = 'ID:' + str(deploy.id)
            if created:
                msg = str(format_lazy(_('Sistema de Previsão '), deploy_id, _(' criado com sucesso.')))

                # Create session deploy_id
                dep_id = [deploy.id] if not deploy.clone else [deploy.id, deploy.clone]
                request.session['deploy_id'] = dep_id
            else:
                msg = str(format_lazy(_('Sistema de Previsão '), deploy_id, _(' guardado com sucesso.')))

        except Exception:
            logger.exception('Error saving steps')
            msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    else:
        msg = str(_('Ocorreu um erro! Por favor tente mais tarde.'))

    return JsonResponse({'msg': msg, 'deploy_id': deploy_id})
'''


'''
@login_required(login_url='/login/')
def save_step(request, step):
    valid, msg = form_validation(request, step)
    return JsonResponse({'valid': valid, 'msg': msg})
'''


'''
@deploy_permission
@login_required(login_url='/login/')
def get_file(request, deploy_id=None, name=None, action=None, format=None):
    # Get a deployment's file
    deploy_id = int(deploy_id)

    if deploy_id == 0:
        # Get file from SessionFile model
        session = Session.objects.get(pk=request.session.session_key)
        file_type = dm_models.FileType.objects.get(name=name, format=format)
        session_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)
        content = bytes(session_file.file)
    else:
        # Get file from DeploymentFile model
        deploy = front_models.Deployment.objects.get(id=deploy_id)
        try:
            file_type = dm_models.FileType.objects.get(name=name, format=format)
            deploy_file = dm_models.DeploymentFile.objects.get(file_type=file_type, deployment=deploy)
            content = bytes(deploy_file.file)
        except Exception:
            # if there is a deployment id but not a deployment file an error will occur
            # this happens when user has restarted step Domain of the wizard and hasn't saved changes
            session = Session.objects.get(pk=request.session.session_key)
            file_type = dm_models.FileType.objects.get(name=name, format=format)
            session_file = front_models.SessionFile.objects.get(file_type=file_type, session=session)
            content = bytes(session_file.file)

    if action == 'download':
        response = HttpResponse(content, content_type='text/plain')
        if deploy_id == 0:
            response['Content-Disposition'] = "attachment; filename=%s.%s" % (name, format)
        else:
            response['Content-Disposition'] = "attachment; filename=%s_%s.%s" % (deploy_id, name, format)
    else:
        response = HttpResponse(content, content_type='text/plain')

    return response
'''
