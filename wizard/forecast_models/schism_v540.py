import sys

import numpy as np

from matplotlib import use as mpl_use
mpl_use('Agg')

from matplotlib import cm as mpl_cm
from matplotlib import pyplot as mpl_pyplot
from matplotlib import colors as mpl_colors
#from matplotlib.path import Path as mpl_path
#from matplotlib.colors import SymLogNorm as mpl_SymLogNorm

from django.contrib.gis.gdal import SpatialReference, CoordTransform, OGRGeometry
from geojson import Feature, FeatureCollection, LineString as geojson_LineString, MultiPolygon as geojson_MultiPolygon
from geojson import dumps as geojson_dumps

from django.utils.translation import ugettext_lazy as _

from cmath import sqrt as math_sqrt
from math import acos as math_acos
from math import cos as math_cos

from front.utils import isWGS84, logger

from shapely.ops import linemerge
from django.contrib.gis.geos import Polygon, LinearRing


def transform_geog(sel_crs):
    # Convert specified projection to WGS84
    in_proj = SpatialReference(sel_crs)
    out_proj = SpatialReference(4326)
    return CoordTransform(in_proj, out_proj)


def transform_proj():
    # Convert WGS84 to World Equidistant Cylindrical Projection
    in_proj = SpatialReference(4326)
    out_proj = SpatialReference(32663)
    return CoordTransform(in_proj, out_proj)


def round_up(num):
#    div_3600 = np.array([1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 16, 18, 20, 24, 25, 30, 36, 40, 45, 48, 50, 60, 72, 75, 80, 90, 100,
#           120, 144, 150, 180, 200, 225, 240, 300, 360, 400, 450, 600, 720, 900, 1200, 1800, 3600])
    div_3600 = np.array([1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18, 20, 24, 30, 36,
                                             40, 45, 60, 72, 90, 120, 180, 360])
    return div_3600[np.argmin(np.abs((div_3600 - num)))].item()


def validate_file(extension, lines, max_nodes, sel_crs=None):
    logger.debug('>> validate_file')
    file_ok, errors, trans, start, clear_form, filename, nrelems, nrnodes = True, None, None, True, True, None, None, None

    try:
        # Test if extension is coherent with the selected coordinate system
        if extension == 'll' and sel_crs != 4326:
            msg = _('Ficheiros com a extensão .ll deverão ter coordenadas geográficas! Deverá selecionar o EPSG:4236 | WGS84.')
            return False, msg, clear_form, filename, nrelems, nrnodes

        nrelems, nrnodes, firstelem, first_node, nropenboundaries = 0, 0, 0, None, 0

        # Get number of elements and nodes
        filename = lines[0]
        nrelems = int(lines[1].split()[0])
        nrnodes = int(lines[1].split()[1])

        # Test if max number of nodes was passed
        if nrnodes > max_nodes:
            msg = _('Malha horizontal - Limite de número de nós ultrapassado: ') + str(max_nodes)
            return False, msg, clear_form, filename, nrelems, nrnodes

        # Test if grid is triangular
        firstelemtype = int(lines[2 + nrnodes].split()[1])
        if firstelemtype != 3:
            msg = _('Malha horizontal - Apenas são válidas malhas triangulares.')
            return False, msg, clear_form, filename, nrelems, nrnodes

        # Test choice of CRS
        first_node = lines[2].split()
        try:
            msg = _('Malha horizontal - Não foi possível a conversão de coordenadas! Verifique se selecionou o Sistema de Referência de Coordenadas correto.')

            pnt = OGRGeometry('POINT(' + str(float(first_node[1])) + ' ' + str(float(first_node[2])) + ')')
            if sel_crs != 4326:
                trans = transform_geog(sel_crs)
                pnt.transform(trans)

            # check if WGS84
            if not isWGS84(pnt.x, pnt.y):
                logger.debug('>> Error: coordinate outside wgs84 ' + str(pnt))
                return False, msg, False, filename, nrelems, nrnodes
        except:
            return False, msg, False, filename, nrelems, nrnodes

        # Test if boundaries exist
        try:
            nropenboundaries = int(lines[2 + nrnodes + nrelems + 1].split()[0])

            # Test if open boundaries exist
            # Commented on 26/09/2018 - Team decided to remove this restriction
            # if nropenboundaries < 1:
            #    msg = _('A malha aparenta não ter fronteiras abertas.')
            #    return False, msg, clear_form, filename, nrelems, nrnodes
        except:
            msg = _('A malha horizontal aparenta não ter fronteiras.')
            return False, msg, clear_form, filename, nrelems, nrnodes

        # All validations passed
        return True, None, clear_form, filename, nrelems, nrnodes

    except Exception:
        logger.exception('Error validating file on schism')
        msg = _('Erro ao processar malha horizontal! Confirme se o formato é o correto.')
        return False, msg, clear_form, filename, nrelems, nrnodes


def get_dt(dtmin, x1, x2, x3, y1, y2, y3, z1, z2, z3):
    cumax, g = 100, 9.81
    try:
        area = float(((x1 - x3) * (y2 - y3) + (x3 - x2) * (y1 - y3)) / 2.)
        hel = float((z1 + z2 + z3) / 3.)

        if hel > 1. and area > 0:
            d = g * hel
            dt = cumax * math_sqrt(area) / math_sqrt(d)
            dtmin = dt.real if dt.real < dtmin else dtmin

        if dtmin < 1:
            return False, None
        else:
            return True, dtmin

    except Exception:
        logger.exception('Error getting dt on schism')
        return False, None


def read_hgrid(lines, sel_crs=4326, sel_crsv=0.0, calcdt=False):
    logger.debug('>> read_hgrid')
    dtmin, dtmin_aux, r, deg2rad = 100, 1e10, 6378206.4, (math_acos(-1.) / 180.)

    if sel_crs != 4326:
        trans = transform_geog(sel_crs)

    try:
        index = 1  # no need to read first line (filename)
        el_no = lines[index]
        el_aux = int(el_no.split()[0])
        el = np.array(el_aux)
        nodes_aux = int(el_no.split()[1])
        nodes = np.array(nodes_aux)

        # Read and process all Coordinates
        xyz, xyz_proj = np.zeros((nodes, 3), dtype=np.float64), np.zeros((nodes, 3), dtype=np.float64)
        for i in range(nodes):
            index += 1
            line = lines[index]
            line = line.split()
            x = float(line[1])
            y = float(line[2])
            z = float(line[3]) + sel_crsv        # Adding vertical reference to depth

            if sel_crs != 4326:
                # Saving projected coordinates (used if calcdt)
                xyz_proj[i] = [x, y, z]
                # Convert projected to geographic coordinates
                pnt = OGRGeometry('POINT(' + str(x) + ' ' + str(y) + ')')
                pnt.transform(trans)
                x = round(pnt.x, 9)
                y = round(pnt.y, 9)

            xyz[i] = [x, y, z]

        # Calculate Point of Origin and mean cpp_lat
        pnt_origin = None
        try:
            mean_lat = np.mean(xyz[:, 1])
            mean_lon = np.mean(xyz[:, 0])
            pnt_origin = OGRGeometry('POINT(' + str(mean_lon) + ' ' + str(mean_lat) + ')')
            cpp_lat = round(pnt_origin.y)
        except Exception:
            pass

        # Read Triangular Facets and calculate dt
        ncon = []

        # Dealing with geometries that cross the 180 meridian
        if np.min(xyz) >= -178 and np.max(xyz) <= 178:
            # Append all Triangular Facets
            for j in range(el):
                index += 1
                line2 = lines[index]
                line2 = line2.split()
                no1, no2, no3 = int(line2[2]), int(line2[3]), int(line2[4])
                ncon.append([no1, no2, no3])

                # Calculate dt
                if calcdt:
                    if sel_crs == 4326:
                        x1, x2, x3 = xyz[:, 0][no1 - 1], xyz[:, 0][no2 - 1], xyz[:, 0][no3 - 1]
                        y1, y2, y3 = xyz[:, 1][no1 - 1], xyz[:, 1][no2 - 1], xyz[:, 1][no3 - 1]
                        z1, z2, z3 = xyz[:, 2][no1 - 1], xyz[:, 2][no2 - 1], xyz[:, 2][no3 - 1]

                        # Must convert from WGS84 to CPP projection (World Equidistant Cylindrical Projection)
                        # This method is extremely faster than using CoordTransform
                        x1 = r * (x1 - pnt_origin.x) * math_cos(pnt_origin.y * deg2rad) * deg2rad
                        y1 = y1 * deg2rad * r
                        x2 = r * (x2 - pnt_origin.x) * math_cos(pnt_origin.y * deg2rad) * deg2rad
                        y2 = y2 * deg2rad * r
                        x3 = r * (x3 - pnt_origin.x) * math_cos(pnt_origin.y * deg2rad) * deg2rad
                        y3 = y3 * deg2rad * r
                    else:
                        # Use original projected coordinates
                        x1, x2, x3 = xyz_proj[:, 0][no1 - 1], xyz_proj[:, 0][no2 - 1], xyz_proj[:, 0][no3 - 1]
                        y1, y2, y3 = xyz_proj[:, 1][no1 - 1], xyz_proj[:, 1][no2 - 1], xyz_proj[:, 1][no3 - 1]
                        z1, z2, z3 = xyz_proj[:, 2][no1 - 1], xyz_proj[:, 2][no2 - 1], xyz_proj[:, 2][no3 - 1]

                    calcdt, dtmin_aux = get_dt(dtmin_aux, x1, x2, x3, y1, y2, y3, z1, z2, z3)
        else:
            # Only append Triangular Facets that do not cross the antimeridian
            for j in range(el):
                index += 1
                line2 = lines[index]
                line2 = line2.split()
                no1, no2, no3 = int(line2[2]), int(line2[3]), int(line2[4])

                x1, x2, x3 = xyz[:, 0][no1 - 1], xyz[:, 0][no2 - 1], xyz[:, 0][no3 - 1]
                xs = np.array([x1, x2, x3])
                minx, maxx = np.min(xs), np.max(xs)
                if minx >= -178 and maxx <= 178:
                    ncon.append([no1, no2, no3])

                    # Calculate dt
                    if calcdt:
                        if sel_crs == 4326:
                            # Get geographical coordinates
                            x1, x2, x3 = xyz[:, 0][no1 - 1], xyz[:, 0][no2 - 1], xyz[:, 0][no3 - 1]
                            y1, y2, y3 = xyz[:, 1][no1 - 1], xyz[:, 1][no2 - 1], xyz[:, 1][no3 - 1]
                            z1, z2, z3 = xyz[:, 2][no1 - 1], xyz[:, 2][no2 - 1], xyz[:, 2][no3 - 1]

                            # Must convert from WGS84 to CPP projection (World Equidistant Cylindrical Projection)
                            # This method is extremely faster than using CoordTransform
                            x1 = r * (x1 - pnt_origin.x) * math_cos(pnt_origin.y * deg2rad) * deg2rad
                            y1 = y1 * deg2rad * r
                            x2 = r * (x2 - pnt_origin.x) * math_cos(pnt_origin.y * deg2rad) * deg2rad
                            y2 = y2 * deg2rad * r
                            x3 = r * (x3 - pnt_origin.x) * math_cos(pnt_origin.y * deg2rad) * deg2rad
                            y3 = y3 * deg2rad * r
                        else:
                            # Use original projected coordinates
                            x1, x2, x3 = xyz_proj[:, 0][no1 - 1], xyz_proj[:, 0][no2 - 1], xyz_proj[:, 0][no3 - 1]
                            y1, y2, y3 = xyz_proj[:, 1][no1 - 1], xyz_proj[:, 1][no2 - 1], xyz_proj[:, 1][no3 - 1]
                            z1, z2, z3 = xyz_proj[:, 2][no1 - 1], xyz_proj[:, 2][no2 - 1], xyz_proj[:, 2][no3 - 1]

                        calcdt, dtmin_aux = get_dt(dtmin_aux, x1, x2, x3, y1, y2, y3, z1, z2, z3)

        # Convert to numpy array
        ncon = np.array(ncon)

        # Consolidate dtmin value
        if calcdt and dtmin_aux:
            if dtmin_aux < 1:
                dtmin = 1
            else:
                dtmin = round_up(int(dtmin_aux))

        # Read Boundaries
        index += 1
        line = lines[index]
        if len(line) == 0:
            msg = _('A malha horizontal aparenta não ter fronteiras.')
            return False, msg, None, None, None, None, None
        else:
            line = line.split()
            nopen = int(line[0])

            index += 1
            line = lines[index]
            line = line.split()
            nd_open = int(line[0])
            bnd_segm = []

            for i in range(nopen):
                index += 1
                line = lines[index]
                line = line.split()
                ibnd = int(line[0])

                c = 0
                bnd_open = np.empty((ibnd, 2))
                for j in range(ibnd):
                    index += 1
                    line = lines[index]
                    line = line.split()
                    ibnd = int(line[0])
                    bnd_open[c] = np.round([float(xyz[ibnd - 1][0]), float(xyz[ibnd - 1][1])], decimals=6)
                    c += 1

                bnd_segm.append((bnd_open, 0))

            # Land Boundaries
            index += 1
            line = lines[index]
            line = line.split()
            nland = int(line[0])
            index += 1
            line = lines[index]
            line = line.split()

            for i in range(nland):
                index += 1
                line = lines[index]
                line = line.split()
                ibnd = int(line[0])
                itype = int(line[1])

                if itype == 0:
                    bnd_land = np.empty((ibnd, 2))
                else:
                    bnd_land = np.empty((ibnd + 1, 2))

                c = 0
                bnd_land = np.zeros((ibnd, 2))
                for j in range(ibnd):
                    index += 1
                    line = lines[index]
                    line = line.split()
                    ibnd = int(line[0])
                    bnd_land[c] = np.round([float(xyz[ibnd - 1][0]), float(xyz[ibnd - 1][1])], decimals=6)
                    c += 1

                if itype == 1:
                    bnd_land[-1] = bnd_land[0]  # Island add last point = first point

                bnd_segm.append((bnd_land, itype + 1))

            return True, None, xyz, ncon, bnd_segm, cpp_lat, dtmin

    except Exception as e:
        logger.exception('Error parsing hgrid on schism')
        strtxt = _("Erro a ler a malha na linha")
        msg = '{0} {1} - {2}'.format(strtxt, index, e)
        return False, msg, None, None, None, None, None


def get_boundaries(bnd_segm):
    logger.debug('>> get_boundaries')
    boundary_types, boundary_count, geojson_all, trans = ['Open', 'Land', 'Island', 'Land'], {'Open': 0, 'Land': 0, 'Island': 0}, [], None

    try:
        all_features = []
        for bid, boundary in enumerate(bnd_segm):
            btype = boundary_types[boundary[1]]
            code = btype.lower() + '-' + str(bid + 1)
            boundary_count[boundary_types[boundary[1]]] += 1

            vertices = boundary[0].tolist()

            # Dealing with geometries that cross the antimeridian
            for i, coord in enumerate(vertices):
                if i > 0:
                    if vertices[i - 1][0] < -160 and coord[0] > 160:
                        coord[0] = coord[0] - 360
                    elif vertices[i - 1][0] > 160 and coord[0] < -160:
                        coord[0] = 360 + coord[0]

            # prepare geojson
            line = geojson_LineString(vertices)
            properties = {
                "code": code,
                "type": btype,
            }
            all_features.append(Feature(geometry=line, properties=properties))

        if len(all_features) > 0:
            # Create geojson with all boundaries
            feature_collection = FeatureCollection(all_features)
            geojson_all = geojson_dumps(feature_collection)

    except Exception:
        logger.exception('Error getting boundaries on schism')
        geojson_all = None
        boundary_count = None

    return geojson_all, boundary_count


def get_contours(xyz, ncongr):
    logger.debug('>> get_contours')
    geojson_obj, trans, max_depth = None, None, None

    try:
        x = xyz[:, 0]
        y = xyz[:, 1]
        z = xyz[:, 2]
        triangles = ncongr[:] - 1

        # Gt reversed colormap scheme
        palette = mpl_cm.get_cmap('viridis_r')

        # Build levels including land/sea separation (<0)
        if z.min() < 0:
            n_contours = 19
            levels = np.linspace(start=0, stop=z.max(), num=n_contours)
            levels = np.concatenate(([z.min()], levels))
        else:
            n_contours = 20
            levels = np.linspace(start=z.min(), stop=z.max(), num=n_contours)

        max_depth = z.max()     # used to validate models with run_type=3D

        #cs = mpl_pyplot.tricontourf(x, y, triangles, z, levels, cmap=palette, antialiased=False, norm=SymLogNorm(linthresh=(z.max()-z.min())/2, vmin=z.min(), vmax=z.max()))
        cs = mpl_pyplot.tricontourf(x, y, triangles, z, levels, cmap=palette, antialiased=False)

        features, contour_index = [], 0
        for collection in cs.collections:
            color = mpl_colors.rgb2hex(collection.get_facecolor().tolist()[0])
            if contour_index == 0 and z.min() < 0:
                color = "#666"  # Land

            mpolygons, properties = [], {
                "color": color,
                "depth": "%i - %i" % (round(levels[contour_index]), round(levels[contour_index+1])),
            }

            for path in collection.get_paths():
                path.should_simplify = False
                mpoly = path.to_polygons(closed_only=True)

                polygon = []
                for poly in mpoly:
                    coordinates = []
                    # get polygon's coordinates
                    for k, p in enumerate(poly):
                        pnt = OGRGeometry('POINT(' + str(p[0]) + ' ' + str(p[1]) + ')')
                        coordinates.append((round(pnt.x, 6), round(pnt.y, 6)))

                    # append coordinates to polygon
                    polygon.append(coordinates)

                # append polygon to group of polygons with the same Z
                mpolygons.append(polygon)

            # build multipolygon object
            mpolys = geojson_MultiPolygon(tuple(mpolygons))
            # append the multipolygon object as a feature with properties
            features.append(Feature(geometry=mpolys, properties=properties))

            contour_index += 1

        # build feature collection
        feature_collection = FeatureCollection(features)

        # json from feature collection
        geojson_obj = geojson_dumps(feature_collection)

    except Exception:
        logger.exception('Error getting contours on schism')

    return geojson_obj, max_depth


def write_ll(lines, xyz):
    logger.debug('>> write_ll')
    # Save hgrid to LL File (projected to geographic coordinates and/or with bathymetry adjustment)
    try:
        line_no = 1
        # Get number of nodes in header
        header = lines[1]
        nodes_no = int(header.split()[1])

        for (i, item) in enumerate(lines):
            if 1 < i < (nodes_no + 2):
                coord = xyz[i-2]
                lines[i] = str(line_no) + ' ' + ('%.9f' % coord[0]) + ' ' + ('%.9f' % coord[1]) + ' ' + ('%.6f' % coord[2]) + '\n'
                line_no += 1

        strdata = ''.join(lines)
        bytesdata = strdata.encode()

    except Exception:
        logger.exception('Error writing .ll file')
        bytesdata = None

    return bytesdata


'''
def write_auxfile_fromhgrid(lines, constant, arrayvals, name):
    logger.debug('>> write_auxfile_fromhgrid')
    # Save aux file like manning file from the hgrid file content and a constant value or array
    try:
        # Rewrite content at line 0
        lines[0] = name + '.ll'
        # Get number of nodes in header
        header = lines[1]
        elems_no, nodes_no = int(header.split()[0]), int(header.split()[1])

        if constant:
            for (i, item) in enumerate(lines):
                if 1 < i < (nodes_no + 2):
                    line_arr = lines[i].split()
                    # Replace Z coord with constant
                    lines[i] = line_arr[0] + ' ' + line_arr[1] + ' ' + line_arr[2] + ' ' + constant
        else:
            for (i, item) in enumerate(lines):
                if 1 < i < (nodes_no + 2):
                    line_arr = lines[i].split()
                    newline_arr = arrayvals[i].split()
                    # Replace Z coord with constant
                    lines[i] = line_arr[0] + ' ' + line_arr[1] + ' ' + line_arr[2] + ' ' + newline_arr[3]

        linenr = elems_no + nodes_no + 2
        # Remove lines after elements nr + nodes nr + 2 (Boundaries information)
        strdata = '\n'.join(lines[0:linenr])
        bytesdata = strdata.encode()

    except Exception:
        logger.exception('Error writing auxiliary file from hgrid')
        bytesdata = None

    return bytesdata
'''


def write_auxfile_fromhgrid(file, fillval, name, bk=False):
    logger.debug('>> write_auxfile_fromhgrid(name={}, bk={})'.format(name, bk))
    # returns auxiliary file content from the hgrid file content and replaces depth by a constant value or array
    try:
        lines = file.decode().split("\n")
        # Rewrite content at line 0
        lines[0] = name + '.ll'
        # Get number of nodes in header
        header = lines[1]
        elems_no, nodes_no = int(header.split()[0]), int(header.split()[1])

        if not isinstance(fillval, list):
            for (i, item) in enumerate(lines):
                if 1 < i < (nodes_no + 2):
                    line_arr = lines[i].split()
                    # Replace Z coord with constant
                    lines[i] = line_arr[0] + ' ' + line_arr[1] + ' ' + line_arr[2] + ' ' + str(fillval)
        else:
            for (i, item) in enumerate(lines):
                if 1 < i < (nodes_no + 2):
                    line_arr = lines[i].split()
                    # Replace Z coord with constant
                    lines[i] = line_arr[0] + ' ' + line_arr[1] + ' ' + line_arr[2] + ' ' + str(fillval[i-2])

        # Get lines up to nodes
        # if not backoffice (for users) add the elements part
        linenr = nodes_no + 2
        if not bk:
            linenr = linenr + elems_no

        strdata = '\n'.join(lines[0:linenr])

    except Exception:
        logger.exception('Error writing auxiliary file from hgrid')
        strdata = None

    return strdata


def get_auxfile_array(lines):
    logger.debug('>> get_auxfile_array')
    # Return auxfile array
    out_array = []

    try:
        # Get number of nodes in header
        header = lines[1]
        elems_no, nodes_no = int(header.split()[0]), int(header.split()[1])

        for (i, item) in enumerate(lines):
            if 1 < i < (nodes_no + 2):
                line_arr = lines[i].split()
                out_array.append(line_arr[3])

    except Exception:
        logger.exception('Error returning auxiliary file array')

    return out_array


def validate_file_3d(lines, max_depth):
    logger.debug('>> validate_file_3d')
    file_ok, clear_form = True, False

    try:
        # Get from the second line: number of rows and levels and value at last level
        nlines = len(lines)-1
        check = int(lines[0].split()[0])
        if check == 1:
            nvrt = int(lines[1].split()[0])
        elif check == 2:
            nvrt = int(lines[1].split()[0])
            kz = int(lines[1].split()[1])
            h_s = float(lines[1].split()[2])
        else:
            msg = _('Malha vertical - Linha não coincide com o cabeçalho (nvrt).')
            return False, msg, clear_form, None

        if check == 2:
            last_row = lines[nlines].split()
            first_level_row = lines[3].split()
            last_level_row = lines[kz+2].split()

        # Test if last row = nrrows
        
        #if int(last_row[0]) != nvrt:
            #msg = _('Malha vertical - Última linha não coincide com o cabeçalho (nvrt).')
            #return False, msg, clear_form, None

        # Test if max Z depth level is highest than horizontal grid's max_depth
        
        #if float(first_level_row[1])*-1 < max_depth:
            #msg = _('Malha vertical - Profundidade máxima tem de ser inferior à verificada na malha horizontal.')
            #return False, msg, clear_form, None

        # Test if last Z level's depth = h_s
        
        #if float(last_level_row[1])*-1 != h_s:
            #msg = _('Malha vertical - Profundidade definida no nível máximo não coincide com o cabeçalho (h_s).')
            #return False, msg, clear_form, None

        # Get z_levels
        z_levels = []
        if check == 2:
            for n in range(3, kz+3):
                z_levels.append(lines[n].split()[1])

        # Get s_levels
        s_levels = []
        if check == 2:
            for n in range(kz+5, nlines+1):
                s_levels.append(lines[n].split()[1])

        # Get other values
        h_c = 0
        theta_b = 0
        thetha_h = 0
        if check == 2:
            aux_row = lines[kz+4].split()
            h_c, theta_b, theta_h = aux_row[0], aux_row[1], aux_row[2]

        out_dic = {'z_levels': z_levels, 's_levels': s_levels, 'h_c': h_c, 'theta_b': theta_b, 'theta_h': theta_h}

        # All validations passed
        return True, None, clear_form, out_dic

    except Exception:
        logger.exception('Error validating vgrid.in file')
        msg = _('Erro ao processar malha vertical! Confirme se o formato é o correto.')
        return False, msg, clear_form, None


def boundaries2polygon(bnd_segm):
    boundary_types = ['Open', 'Land', 'Island', 'Land']
    try:
        lines = []
        for boundary in bnd_segm:
            btype = boundary_types[boundary[1]]
            # Get open and land boundary lines
            if btype == 'Open' or btype == 'Land':
                lines.append(boundary[0].tolist())

        # Merge lines coordinates
        mergedlines = linemerge(lines)
        geos_poly = Polygon()
        geos_poly.exterior_ring = LinearRing(tuple(mergedlines.coords))

        return True, geos_poly

    except Exception:
        return False, None
